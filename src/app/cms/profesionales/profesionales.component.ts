import { Component, OnInit } from '@angular/core';
import { Store, Actions, ofActionSuccessful, ofActionErrored } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { DbObject } from 'src/app/interfaces/Parametros.interface';
import { switchMap } from 'rxjs/operators';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AgregarProfesional } from 'src/app/states/parametros.state';

@Component({
  selector: 'app-profesionales',
  templateUrl: './profesionales.component.html',
  styleUrls: ['./profesionales.component.css']
})
export class ProfesionalesComponent implements OnInit {
  profesionales: Observable<DbObject>;
  nuevoProfesional: FormControl

  constructor(
    private store: Store,
    private fb: FormBuilder,
    private actions: Actions,
    private snack: MatSnackBar
  ) { }

  ngOnInit() {
    this.nuevoProfesional = this.fb.control(null, [Validators.required, Validators.minLength(3)]);
    this.profesionales = this.store.select(state =>
      state.parametros
    ).pipe(switchMap(params => of(params.profesionales)));
  }

  agregar = () => {
    const profesional = this.nuevoProfesional.value
    this.store.dispatch(new AgregarProfesional(profesional))
    this.actions.pipe(ofActionSuccessful(AgregarProfesional))
      .subscribe(() => {
        this.nuevoProfesional.reset();
        this.snack.open('Agregado con exito', 'Ok', { duration: 2500 });
      });
    this.actions.pipe(ofActionErrored(AgregarProfesional))
      .subscribe(() => {
        this.snack.open('Algo salio mal :(', 'Ok');
      });
  }

}
