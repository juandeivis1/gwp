import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CmsRoutingModule } from "./cms-routing.module";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatMenuModule } from "@angular/material/menu";
import { MatSelectModule } from "@angular/material/select";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatTabsModule } from "@angular/material/tabs";
import { MatToolbarModule } from "@angular/material/toolbar";
import { NavComponent } from './nav/nav.component';
import { LugaresComponent } from './lugares/lugares.component';
import { ReactiveFormsModule } from "@angular/forms";
import { EditarLugarComponent } from './editar-lugar/editar-lugar.component';
import { PortafoliosComponent } from './portafolios/portafolios.component';
import { ProfesionalesComponent } from './profesionales/profesionales.component';
import { ProgramasComponent } from './programas/programas.component';
import { CumplirComponent } from './cumplir/cumplir.component';

@NgModule({
  imports: [
    CommonModule,
    CmsRoutingModule,
    MatIconModule,
    MatDialogModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatTabsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    ReactiveFormsModule,
    MatMenuModule
  ],
  declarations: [
    NavComponent,
    LugaresComponent,
    EditarLugarComponent,
    PortafoliosComponent,
    ProfesionalesComponent,
    ProgramasComponent,
    CumplirComponent
  ],
  entryComponents: [
    EditarLugarComponent
  ]
})
export class CmsModule { }