import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NavComponent } from "./nav/nav.component";
import { LugaresComponent } from "./lugares/lugares.component";
import { PortafoliosComponent } from "./portafolios/portafolios.component";
import { ProfesionalesComponent } from "./profesionales/profesionales.component";
import { ProgramasComponent } from "./programas/programas.component";
import { CumplirComponent } from "./cumplir/cumplir.component";


const routes: Routes = [
  { path: '', redirectTo: 'lugares', pathMatch: 'full' },
  {
    path: '', component: NavComponent, children: [
      { path: 'lugares', component: LugaresComponent },
      { path: 'portafolios', component: PortafoliosComponent },
      { path: 'profesionales', component: ProfesionalesComponent },
      { path: 'programas', component: ProgramasComponent },
      { path: 'cumplir', component: CumplirComponent },
    ]
  },
  { path: '**', redirectTo: 'lugares', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CmsRoutingModule { }
