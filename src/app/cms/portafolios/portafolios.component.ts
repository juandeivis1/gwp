import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Store, Actions, ofActionSuccessful, ofActionErrored } from '@ngxs/store';
import { DbObject } from 'src/app/interfaces/Parametros.interface';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AgregarPortafolio } from 'src/app/states/parametros.state';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-portafolios',
  templateUrl: './portafolios.component.html',
  styleUrls: ['./portafolios.component.css']
})
export class PortafoliosComponent implements OnInit {
  portafolios: Observable<DbObject>;
  nuevoPortafolio: FormControl;

  constructor(
    private store: Store,
    private fb: FormBuilder,
    private actions: Actions,
    private snack: MatSnackBar
  ) { }

  ngOnInit() {
    this.nuevoPortafolio = this.fb.control(null, [Validators.required, Validators.minLength(6)])
    this.portafolios = this.store.select(state =>
      state.parametros
    ).pipe(switchMap(params => of(params.portafolios)));
  }

  agregar = () => {
    const portafolio = this.nuevoPortafolio.value
    this.store.dispatch(new AgregarPortafolio(portafolio))
    this.actions.pipe(ofActionSuccessful(AgregarPortafolio))
      .subscribe(() => {
        this.nuevoPortafolio.reset();
        this.snack.open('Agregado con exito', 'Ok', { duration: 2500 });
      });
    this.actions.pipe(ofActionErrored(AgregarPortafolio))
      .subscribe(() => {
        this.snack.open('Algo salio mal :(', 'Ok');
      });
  }

}
