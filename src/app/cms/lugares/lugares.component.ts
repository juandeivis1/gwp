import { Component, OnInit } from '@angular/core';
import { Parametros, Ciudad, Comunidad, Barrio } from 'src/app/interfaces/Parametros.interface';
import { Store, Actions, ofActionSuccessful, ofActionErrored } from '@ngxs/store';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AgregarCiudad, AgregarComunidad, AgregarBarrio, ActualizarBarrio, ActualizarComunidad, ActualizarCiudad } from 'src/app/states/parametros.state';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-lugares',
  templateUrl: './lugares.component.html',
  styleUrls: ['./lugares.component.css']
})
export class LugaresComponent implements OnInit {
  parametros: Parametros;
  city: FormGroup;
  community: FormGroup;
  neighborhood: FormGroup;

  citiesA: Ciudad[];
  citiesB: Ciudad[];
  citiesC: Ciudad[];
  communitiesA: Comunidad[];
  communitiesB: Comunidad[];
  barrios: Barrio[];
  ciudadEditando: number;
  comunidadEditando: number;
  barrioEditando: number;

  constructor(
    private store: Store,
    private fb: FormBuilder,
    private actions: Actions,
    private snack: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.actions.pipe(ofActionSuccessful(AgregarCiudad, AgregarComunidad, AgregarBarrio, ActualizarBarrio, ActualizarCiudad, ActualizarComunidad))
      .subscribe(() => {
        this.parametros = this.store.selectSnapshot(state =>
          state.parametros
        );
      });
  }

  ngOnInit() {
    this.parametros = this.store.selectSnapshot(state =>
      state.parametros
    );
    this.city = this.fb.group({
      district: [null, [Validators.required]],
      name: [null, [Validators.required, Validators.minLength(4)]]
    });
    this.community = this.fb.group({
      city: [null, [Validators.required]],
      name: [null, [Validators.required, Validators.minLength(4)]]
    });

    this.neighborhood = this.fb.group({
      community: [null, [Validators.required]],
      name: [null, [Validators.required, Validators.minLength(4)]],
      population: [null]
    });

  }

  // Centro poblacional

  filtrarCiudadesA = (distrito) => {
    this.citiesA = this.parametros.ciudades.filter(ciudad => ciudad.district == distrito);
  }

  addCity = () => {
    this.store.dispatch(new AgregarCiudad(this.city.value))
    this.actions.pipe(ofActionSuccessful(AgregarCiudad))
      .subscribe(() => {
        this.snack.open('Ciudad agregada con exito!', 'Ok', { duration: 3000 });
        this.city.reset();
      });
    this.actions.pipe(ofActionErrored(AgregarCiudad))
      .subscribe(() => {
        this.snack.open('Error al agregar la ciudad!', 'Ok');
      });
  }

  // Comunidad

  filtrarCiudadesB = (distrito) => {
    this.citiesB = this.parametros.ciudades.filter(ciudad => ciudad.district == distrito);
    this.community.patchValue({
      city: null
    });
    this.communitiesA = null;
  }

  filtrarComunidadesA = (ciudad) => {
    this.communitiesA = this.parametros.comunidades.filter(comunidad => comunidad.city == ciudad);
  }

  addCommunity = () => {
    this.store.dispatch(new AgregarComunidad(this.community.value))
    this.actions.pipe(ofActionSuccessful(AgregarComunidad))
      .subscribe(() => {
        this.snack.open('Comunidad agregada con exito!', 'Ok', { duration: 3000 });
        this.community.reset();
      });
    this.actions.pipe(ofActionErrored(AgregarComunidad))
      .subscribe(() => {
        this.snack.open('Error al agregar la comunidad!', 'Ok');
      });
  }

  // Barrio

  filtrarCiudadesC = (distrito) => {
    this.citiesC = this.parametros.ciudades.filter(ciudad => ciudad.district == distrito);
    this.neighborhood.patchValue({
      community: null
    });
    this.communitiesB = null;
    this.barrios = null;
  }


  filtrarComunidadesB = (ciudad) => {
    this.communitiesB = this.parametros.comunidades.filter(comunidad => comunidad.city == ciudad);
    this.neighborhood.patchValue({
      community: null
    });
    this.barrios = null;
  }

  filtrarBarrios = (comunidad) => {
    this.barrios = this.parametros.barrios.filter(barrio => barrio.community == comunidad);
  }

  addNeighborhood = () => {
    this.store.dispatch(new AgregarBarrio(this.neighborhood.value))
    this.actions.pipe(ofActionSuccessful(AgregarBarrio))
      .subscribe(() => {
        this.snack.open('Barrio agregada con exito!', 'Ok', { duration: 3000 });
        this.neighborhood.reset();
      });
    this.actions.pipe(ofActionErrored(AgregarBarrio))
      .subscribe(() => {
        this.snack.open('Error al agregar el Barrio!', 'Ok');
      });
  }

  editarCiudad = (ciudad: Ciudad) => {
    this.city.patchValue({
      name: ciudad.name
    })
    this.ciudadEditando = ciudad.id;
  }

  actualizarCiudad = () => {
    const ciudad = this.city.value;
    this.store.dispatch(new ActualizarCiudad(ciudad, this.ciudadEditando));
    this.actions.pipe(ofActionSuccessful(ActualizarCiudad))
      .subscribe(() => {
        this.filtrarCiudadesA(ciudad.district);
        this.snack.open('Ciudad actualizada con exito!', 'Ok', { duration: 3000 });
        this.cancelarCiudad();
      });
    this.actions.pipe(ofActionErrored(ActualizarBarrio))
      .subscribe(() => {
        this.snack.open('Error al actualizar la ciudad!', 'Ok');
      });
  }


  cancelarCiudad = () => {
    this.city.patchValue({
      name: null
    })
    this.ciudadEditando = null;
  }

  editarComunidad = (comunidad: Comunidad) => {
    this.community.patchValue({
      name: comunidad.name
    })
    this.comunidadEditando = comunidad.id;
  }

  actualizarComunidad = () => {
    const comunidad = this.community.value;
    this.store.dispatch(new ActualizarComunidad(comunidad, this.comunidadEditando));
    this.actions.pipe(ofActionSuccessful(ActualizarComunidad))
      .subscribe(() => {
        this.filtrarComunidadesA(comunidad.city);
        this.snack.open('Comunidad actualizado con exito!', 'Ok', { duration: 3000 });
        this.cancelarComunidad();
      });
    this.actions.pipe(ofActionErrored(ActualizarBarrio))
      .subscribe(() => {
        this.snack.open('Error al actualizar la comunidad!', 'Ok');
      });
  }

  cancelarComunidad = () => {
    this.community.patchValue({
      name: null
    })
    this.comunidadEditando = null;
  }

  editarBarrio = (barrio: Barrio) => {
    this.neighborhood.patchValue({
      name: barrio.name,
      population: barrio.population
    });
    this.barrioEditando = barrio.id;
  }

  actualizarBarrio = () => {
    const barrio = this.neighborhood.value;
    this.store.dispatch(new ActualizarBarrio(barrio, this.barrioEditando));
    this.actions.pipe(ofActionSuccessful(ActualizarBarrio))
      .subscribe(() => {
        this.filtrarBarrios(barrio.community);
        this.snack.open('Barrio actualizado con exito!', 'Ok', { duration: 3000 });
        this.cancelarBarrio();
      });
    this.actions.pipe(ofActionErrored(ActualizarBarrio))
      .subscribe(() => {
        this.snack.open('Error al actualizar el barrio!', 'Ok');
      });
  }

  cancelarBarrio = () => {
    this.neighborhood.patchValue({
      name: null,
      population: null
    });
    this.barrioEditando = null;
  }


}
