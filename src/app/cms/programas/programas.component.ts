import { Component, OnInit } from '@angular/core';
import { Store, Actions, ofActionSuccessful, ofActionErrored } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { DbObject } from 'src/app/interfaces/Parametros.interface';
import { switchMap } from 'rxjs/operators';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AgregarPrograma } from 'src/app/states/parametros.state';

@Component({
  selector: 'app-programas',
  templateUrl: './programas.component.html',
  styleUrls: ['./programas.component.css']
})
export class ProgramasComponent implements OnInit {
  programas: Observable<DbObject>;
  nuevoPrograma: FormControl;
  constructor(
    private store: Store,
    private fb: FormBuilder,
    private actions: Actions,
    private snack: MatSnackBar
    ) { }

  ngOnInit() {
    this.nuevoPrograma = this.fb.control(null, [Validators.required, Validators.minLength(3)]);
    this.programas = this.store.select(state =>
      state.parametros
    ).pipe(switchMap(params => of(params.programas)));
  }

  agregar = () => {
    const programa = this.nuevoPrograma.value
    this.store.dispatch(new AgregarPrograma(programa))
    this.actions.pipe(ofActionSuccessful(AgregarPrograma))
      .subscribe(() => {
        this.nuevoPrograma.reset();
        this.snack.open('Agregado con exito', 'Ok', { duration: 2500 });
      });
    this.actions.pipe(ofActionErrored(AgregarPrograma))
      .subscribe(() => {
        this.snack.open('Algo salio mal :(', 'Ok');
      });
  }

}
