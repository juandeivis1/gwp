import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CumplirComponent } from './cumplir.component';

describe('CumplirComponent', () => {
  let component: CumplirComponent;
  let fixture: ComponentFixture<CumplirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CumplirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CumplirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
