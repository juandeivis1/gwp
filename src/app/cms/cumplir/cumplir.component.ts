import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { DbObject } from 'src/app/interfaces/Parametros.interface';
import { Store, Actions, ofActionSuccessful, ofActionErrored } from '@ngxs/store';
import { switchMap } from 'rxjs/operators';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { AgregarCumplir } from 'src/app/states/parametros.state';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-cumplir',
  templateUrl: './cumplir.component.html',
  styleUrls: ['./cumplir.component.css']
})

export class CumplirComponent implements OnInit {
  cumplir: Observable<DbObject>;
  nuevoCumplir: FormControl

  constructor(
    private store: Store,
    private fb: FormBuilder,
    private actions: Actions,
    private snack: MatSnackBar
    ) { }

  ngOnInit() {
    this.nuevoCumplir = this.fb.control(null, [Validators.required, Validators.minLength(6)])
    this.cumplir = this.store.select(state =>
      state.parametros
    ).pipe(switchMap(params => of(params.cumplir)));
  }

  agregar = () => {
    const cumplir = this.nuevoCumplir.value
    this.store.dispatch(new AgregarCumplir(cumplir))
    this.actions.pipe(ofActionSuccessful(AgregarCumplir))
    .subscribe(() => {
      this.nuevoCumplir.reset();
      this.snack.open('Agregado con exito', 'Ok', { duration: 2500});
    });
    this.actions.pipe(ofActionErrored(AgregarCumplir))
    .subscribe(() => {
      this.snack.open('Algo salio mal :(', 'Ok');
    });
  }
}
