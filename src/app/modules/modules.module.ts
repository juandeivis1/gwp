import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgxsModule } from '@ngxs/store';
import { AuthState } from '../states/auth.state';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { InternoState } from '../states/interno.state';
import { ParametrosState } from '../states/parametros.state';
import { AdminState } from '../states/admin.state';
import { AngularFireModule } from '@angular/fire';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { Proyectos } from '../states/proyectos';
@NgModule({
  imports: [
    CommonModule,
    AngularFireModule.initializeApp({ messagingSenderId: '231573923464' }),
    AngularFireMessagingModule,
    HttpClientModule,
    NgxsModule.forRoot([
      AuthState,
      InternoState,
      ParametrosState,
      AdminState,
      Proyectos
    ]),
    NgxsStoragePluginModule.forRoot({
      key: [
        'admin',
        'auth',
        'interno',
        'parametros',
        'proyectos'
      ],

    }),
    NgxsReduxDevtoolsPluginModule.forRoot({
      disabled: false
    }),
    NgxsLoggerPluginModule.forRoot({
      disabled: false
    }),
    NgxsRouterPluginModule.forRoot()
  ],
  declarations: [],
  providers: [],
  exports: []
})
export class ModulesModule { }
