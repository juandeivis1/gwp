import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InversionProgramaComponent } from './inversion-programa.component';

describe('InversionProgramaComponent', () => {
  let component: InversionProgramaComponent;
  let fixture: ComponentFixture<InversionProgramaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InversionProgramaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InversionProgramaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
