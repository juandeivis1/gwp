import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GetCostosPrograma, GetCostosPortafolio } from '../../states/admin.state';
import { Store } from '@ngxs/store';
import { DecimalPipe } from '@angular/common';
import { switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { AuthState } from 'src/app/states/auth.state';

@Component({
  selector: 'app-inversion-programa',
  templateUrl: './inversion-programa.component.html',
  styleUrls: ['./inversion-programa.component.css']
})
export class InversionProgramaComponent implements OnInit {

  public incomeOptions: any = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }],
      xAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    responsive: true,
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          var label = data.labels[tooltipItem.index] || "";
          if (label) {
            label += ": ₡";
          }
          label += this.dp.transform(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
          return label;
        }
      }
    }
  };

  public pieOptions: any = {
    pieceLabel: {
      render: 'value'
    },
    responsive: true,
    // legend: {
    //   display: false,
    // }
  };
  costosPrograma: Observable<any>;
  costosPortafolio: Observable<any>;
  imprimir: Array<boolean> = []

  constructor(
    private store: Store,
    private dp: DecimalPipe
  ) { }

  ngOnInit() {
    this.refrescar()
    this.costosPrograma = this.store.select(state => state.admin.programas).pipe(tap(portafolios => {
      portafolios.map(() => {
        this.imprimir.push(false)
      })
    }))
    this.costosPortafolio = this.store.select(state => state.admin.portafolios)
  }

  refrescar = () => {
    const token = this.store.selectSnapshot(AuthState.token);
    this.store.dispatch(new GetCostosPortafolio(token));
    this.store.dispatch(new GetCostosPrograma(token));
  }

  obtenerFases = (fases: any) => {
    let fasesKeys = Object.keys(fases)
    let pasa = false;
    fasesKeys.map(fase => {
      fases[fase] > 0 ? pasa = true : null;
    });
    return pasa ? fases : undefined;
  }

  obtenerPortafolio = (portafolio) => {
    return this.costosPortafolio.pipe(switchMap((portafolios: any[]) => {
      if (portafolios.length > 0) {
        return of(portafolios.find(porta => porta.portafolio == portafolio).resumen)
      } else {
        return of(null)
      }
    }))
  }

}
