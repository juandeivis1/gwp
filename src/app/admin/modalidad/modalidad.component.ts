import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { GetModalidad } from 'src/app/states/admin.state';
import { Observable, BehaviorSubject, combineLatest, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthState } from 'src/app/states/auth.state';

interface Modalidad {
  modalidad: string
  portafolios: {portafolio: string}[]
}

@Component({
  selector: 'app-modalidad',
  templateUrl: './modalidad.component.html',
  styleUrls: ['./modalidad.component.css']
})
export class ModalidadComponent implements OnInit {

  modalidade$: Observable<any[]>
  portafolios: Observable<string[]>
  portafolio: Observable<any>
  imprimir: Array<boolean> = []
  modalidad: BehaviorSubject<string> = new BehaviorSubject('Obra por Contrato')
  modalidadActual: Observable<Modalidad> = of(null)
  portafolioActual: BehaviorSubject<string> = new BehaviorSubject(null)

  constructor(private store: Store) {  }

  ngOnInit() {
    this.modalidade$ = this.store.select(state => state.admin.modalidades)
    this.refrescar()
    this.modalidadActual = combineLatest(this.modalidad, this.modalidade$).pipe(switchMap(([modalidad, modalidades]) => {
      if (modalidades.length > 0) {
        let nuevaModalidad = modalidades.find((_modalidad) => _modalidad.modalidad == modalidad)
        this.imprimir = nuevaModalidad.portafolios.map(_portafolio => false)
        let portafolios = nuevaModalidad.portafolios.map(_portafolio => _portafolio.portafolio)
        this.portafolios = of(portafolios)
        this.nuevoPortafolio(portafolios[0])
        return of(nuevaModalidad)
      } else {
        return of(null)
      }
    }))

    this.portafolio = combineLatest(this.modalidadActual, this.portafolioActual).pipe(switchMap(([modalidad,portafolio]) => {
      let portafolioFiltado = modalidad.portafolios.find(_portafolio => _portafolio.portafolio == portafolio);
      return of(portafolioFiltado)
    }))

  }

  refrescar = () => {
    const token = this.store.selectSnapshot(AuthState.token);
    this.store.dispatch(new GetModalidad(token))
  }

  nuevaModalidad = (modalidad: string) => {
    this.modalidad.next(modalidad)
  }

  nuevoPortafolio = (modalidad: string) => {
    this.portafolioActual.next(modalidad)
  }

}
