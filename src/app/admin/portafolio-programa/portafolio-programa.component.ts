import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Store, Select, Actions, ofActionSuccessful, ofActionErrored } from '@ngxs/store';
import { ApiService } from 'src/app/services/api.service';
import { tap, finalize, catchError } from 'rxjs/operators';
import { DecimalPipe } from '@angular/common';
import { GetPorPro, AdminState } from 'src/app/states/admin.state';
import { AuthState } from 'src/app/states/auth.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-portafolio-programa',
  templateUrl: './portafolio-programa.component.html',
  styleUrls: ['./portafolio-programa.component.css']
})
export class PortafolioProgramaComponent implements OnInit {

  result: Observable<any>;
  proyectos: any;
  sumatoria: any;
  filtros: any;
  parametros: any;
  consultando: boolean;
  complete: boolean;
  portafoliosPrint = [];
  portafolios = [];

  public chartType: any = 'line';
  public chartOptions = {
    responsive: true,
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          let label = data.labels[tooltipItem.index] || '';
          if (label) {
            label += ': ₡';
          }
          label += this.dp.transform(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
          return label;
        }
      }
    },
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public chartOptionsQuantity = {
    responsive: true,
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          let label = data.labels[tooltipItem.index] || '';
          label += ': ' + this.dp.transform(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
          return label;
        }
      }
    },
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public volumenColor = [
    {
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
  ];
  public poblacionColor = [
    {
      backgroundColor: '#0076ff57',
      borderColor: '#0076ff',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];
  public costoColor = [
    {
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
  ];
  public percapitaColor = [
    {
      backgroundColor: '#ff408152',
      borderColor: '#ff4081',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];

  constructor(
    private fb: FormBuilder,
    private store: Store,
    private dp: DecimalPipe,
    private actions: Actions
  ) {
    this.actions.pipe(
      ofActionSuccessful(
        GetPorPro
      )
    ).subscribe(() => {
      this.consultando = false;
      this.complete = true;
    });

    this.actions.pipe(
      ofActionErrored(
        GetPorPro
      )
    ).subscribe(() => {
      this.consultando = false;
      this.complete = true;
    });
  }

  ngOnInit() {
    this.filtros = this.fb.group({
      portafolio: [null],
      programa: [null]
    });
    this.result = this.store.select(AdminState.porpro).pipe(
      tap(
        porpro => {
          this.portafolios = [];
          this.portafoliosPrint = [];
          if (porpro) {
            porpro.portafolios.map(
              portafolio => {
                this.portafolios.push({ portafolio: portafolio.portafolio, print: true });
                this.portafoliosPrint.push({ portafolio: portafolio.portafolio, print: false });
              }
            );
          }
        }
      )
    );
    this.parametros = this.store.select(state => state.parametros);
  }

  generarReporte = () => {
    this.consultando = true;
    const token = this.store.selectSnapshot(AuthState.token);
    this.store.dispatch(new GetPorPro(token, this.filtros.value));
  }

  reset = () => {
    this.filtros.reset();
    this.complete = false;
  }

  setToPrint = (index: number) => {
    this.portafoliosPrint[index].print = !this.portafoliosPrint[index].print;
  }

}
