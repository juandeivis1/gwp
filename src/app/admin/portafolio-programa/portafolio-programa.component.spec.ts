import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortafolioProgramaComponent } from './portafolio-programa.component';

describe('PortafolioProgramaComponent', () => {
  let component: PortafolioProgramaComponent;
  let fixture: ComponentFixture<PortafolioProgramaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortafolioProgramaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortafolioProgramaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
