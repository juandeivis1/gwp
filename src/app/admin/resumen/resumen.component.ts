import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Store, Select } from '@ngxs/store';
import { InformacionStateModel } from '../../interfaces/InformacionStateModel.interface';
import { Costos } from '../../interfaces/Costos.interface';
import { Pendiente } from '../../interfaces/Pendiente.interface';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DecimalPipe } from '@angular/common';
import { Proyecto } from 'src/app/interfaces/Proyecto';
import { SincronizarProyecto } from 'src/app/states/proyectos';
import { AuthState } from 'src/app/states/auth.state';
import { BusquedaProyectoComponent } from 'src/app/core/busqueda-proyecto/busqueda-proyecto.component';
import { tap, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-resumen',
  templateUrl: './resumen.component.html',
  styleUrls: ['./resumen.component.css']
})
export class ResumenComponent implements OnInit {
  @Select() proyectos$: Observable<Proyecto[]>;
  private pro: any = [];

  proyecto$: Observable<Proyecto>;
  informacion$: Observable<InformacionStateModel>;
  administrativo: number = null;
  costoTotal: number = null;

  private sumatoria: { monto: string, etiqueta: string }[] = [
    { monto: 'adjudicado', etiqueta: 'Adjudicado' },
    { monto: 'calidad', etiqueta: 'Control de calidad' },
    { monto: 'comunales', etiqueta: 'Aportes Comunales' },
    { monto: 'ordenServicio', etiqueta: 'Orden de servicio' },
    { monto: 'especie', etiqueta: 'Aportes en especie' },
    { monto: 'especifica', etiqueta: 'Partida específica' },
    { monto: 'reajuste', etiqueta: 'Reajuste' },
    { monto: 'ampliacion', etiqueta: 'Ampliación' },
    { monto: 'instituciones', etiqueta: 'Instituciones' },
    { monto: 'extra1', etiqueta: 'Extra 1' },
    { monto: 'extra2', etiqueta: 'Extra 2' }
  ];

  chart: { labels: any[]; data: any[]; };

  public incomeOptions: any = {
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          let label = data.labels[tooltipItem.index] || '';
          if (label) {
            label += ': ₡';
          }
          label += this.dp.transform(data.datasets[0].data[tooltipItem.index]);
          return label;
        }
      }
    }
  };

  constructor(
    private store: Store,
    private bottomSheet: MatBottomSheet,
    private snackBar: MatSnackBar,
    private dp: DecimalPipe
  ) {
  }

  ngOnInit() {
    this.proyectos$.subscribe(x => (this.pro = x));
  }

  calcularCostos(costos: Costos) {
    if (costos) {
      const tempCostos = this.sumatoria.reduce(
        (costosSalida, campo) => {
          if (costos[campo.monto]) {
            costosSalida.push(costos[campo.monto]);
          }
          return costosSalida;
        }, []);

      this.costoTotal = tempCostos.reduce((costoSalida, actualCosto) => {
        costoSalida += actualCosto;
        return costoSalida;
      }, 0);

      if (this.costoTotal > 0) {
        this.administrativo = this.costoTotal * 0.05;
        this.costoTotal += this.administrativo;
      }
    }
  }

  cargarGrafico(costos: Costos) {
    if (costos) {
      this.chart = this.sumatoria.reduce((chart, campo) => {
        if (costos[campo.monto]) {
          chart.labels.push(campo.etiqueta);
          chart.data.push(costos[campo.monto]);
        }
        return chart;
      }, { labels: [], data: [] });

      this.costoTotal = this.chart.data.reduce((costoTotal, actualCosto) => {
        costoTotal += actualCosto;
        return costoTotal;
      }, 0);
      if (this.costoTotal > 0) {
        this.chart.labels.push('Costo administrativo');
        this.administrativo = this.costoTotal * 0.05;
        this.chart.data.push(this.administrativo);
        this.costoTotal += this.administrativo;
      }
    }
  }

  nuevoResumen = () => {
    this.proyecto$ = null;
    this.administrativo = null;
    this.costoTotal = null;
    this.chart = null; // = { labels: ['Sin definir'], data: [0] };
  }

  pendientesAtendidos = (pendientes: Pendiente[]) => {
    return pendientes.filter(pen => pen.resuelto);
  }

  pendientesSinAtender = (pendientes: Pendiente[]) => {
    return pendientes.filter(pen => !pen.resuelto);
  }

  private _filter = (criterio: string): InformacionStateModel[] => {
    const criterioFiltrar = criterio.toString().toLowerCase().trim();
    return this.pro.filter(v =>
      v.nombre.toLowerCase().indexOf(criterioFiltrar.toLowerCase()) > -1 ||
      v.sinonimo.toLowerCase().indexOf(criterioFiltrar.toLowerCase()) > -1 ||
      v.expediente.toLowerCase().indexOf(criterioFiltrar.toLowerCase()) > -1);
  }

  realizarBusqueda = (termino) => {
    const proyectos = this._filter(termino);
    if (proyectos.length > 0) {
      const proyectosSeleccionado = this.bottomSheet.open(BusquedaProyectoComponent, {
        data: proyectos,
      });

      proyectosSeleccionado.afterDismissed().subscribe(proyecto => {
        if (proyecto) {
          const token = this.store.selectSnapshot(AuthState.token);
          this.store.dispatch(new SincronizarProyecto(proyecto, token));
          this.proyecto$ = this.store.select(state =>
            state.proyectos.find(p => p.proId === proyecto)
          ).pipe(switchMap((_proyecto) => {
            if (_proyecto.bitacora) {
              _proyecto.bitacora = _proyecto.bitacora.slice(-2);
            } else {
              _proyecto.bitacora = null;
            }
            if (!!_proyecto.costos) {
              this.calcularCostos(_proyecto.costos);
              this.cargarGrafico(_proyecto.costos);
            }
            return of(_proyecto);
          }));
        }
      });
    } else {
      this.snackBar.open('No coincide con ningún proyecto', 'Ok', {
        duration: 3000
      });
    }
  }

}
