import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Store, Select, Actions, ofActionSuccessful, ofActionErrored } from '@ngxs/store';
import { ApiService } from 'src/app/services/api.service';
import { tap, finalize, catchError } from 'rxjs/operators';
import { empty, Observable } from 'rxjs';
import { MatInput } from '@angular/material/input';
import { Navigate } from '@ngxs/router-plugin';
import { environment } from 'src/environments/environment';
import { GetFecha, AdminState } from 'src/app/states/admin.state';
import { AuthState } from 'src/app/states/auth.state';

@Component({
  selector: 'app-fecha',
  templateUrl: './fecha.component.html',
  styleUrls: ['./fecha.component.css']
})
export class FechaComponent implements OnInit {
  filtros: any;
  parametros: any;
  consultando = false;
  complete = false;

  @Select(AdminState.fecha) result: Observable<any>;

  @ViewChild('inicio', {
    static: true
  }) inicio: MatInput;

  @ViewChild('fin', {
    static: true
  }) fin: MatInput;

  sumatoria: any;

  constructor(
    private fb: FormBuilder,
    private store: Store,
    private actions: Actions,

  ) {

    this.actions.pipe(
      ofActionSuccessful(
        GetFecha
      )
    ).subscribe(() => {
      this.consultando = false;
      this.complete = true;
    });

    this.actions.pipe(
      ofActionErrored(
        GetFecha
      )
    ).subscribe(() => {
      this.consultando = false;
      this.complete = true;
    });
  }

  ngOnInit() {
    this.actions
      .pipe(ofActionSuccessful(GetFecha))
      .subscribe(() => {
        this.consultando = false;
      });
    this.actions
      .pipe(ofActionErrored(GetFecha))
      .subscribe(() => {
        this.consultando = false;
      });

    this.filtros = this.fb.group({
      inicioA: [null],
      inicioB: [null],
      finA: [null],
      finB: [null],
      portafolio: [null],
      programa: [null]
    });
    this.parametros = this.store.select(state => state.parametros);
  }

  generarReporte = () => {
    this.consultando = true;
    const token = this.store.selectSnapshot(AuthState.token);
    this.store.dispatch(new GetFecha(token, this.filtros.value));
  }

  definirFecha = ({ value }, inicio: boolean, a: boolean) => {
    const ts = value.getTime();
    if (inicio) {
      if (a) {
        this.filtros.patchValue({
          inicioA: ts
        });
      } else {
        this.filtros.patchValue({
          inicioB: ts
        });
      }
    } else {
      if (a) {
        this.filtros.patchValue({
          finA: ts
        });
      } else {
        this.filtros.patchValue({
          finB: ts
        });
      }
    }
  }

  reset = () => {
    this.filtros.reset();
    this.complete = false;
  }

  irProyecto = id => {
    window.open(`https://perezzeledon.go.cr:803/gestion/proyectos/actual/${id}`, '_blank');
    // this.store.dispatch(new Navigate(['/gestion/proyectos/actual/', id]));
  }


}
