import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, empty, of } from 'rxjs';
import { tap, catchError, finalize } from 'rxjs/operators';
import { ApiService } from '../../services/api.service';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
// import { Acciones } from '../../listas/listas';
import { Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';
import { Parametros } from '../../interfaces/Parametros.interface';
import { AuthState } from 'src/app/states/auth.state';


@Component({
  selector: 'app-seguimiento-proyectos',
  templateUrl: './seguimiento-proyectos.component.html',
  styleUrls: ['./seguimiento-proyectos.component.css']
})
export class SeguimientoProyectosComponent implements OnInit {
  // readonly acciones: string[] = Acciones

  public accionFG: FormGroup;
  public consultando = false;
  public proyectos$: Observable<any>;
  public proyectos: any[] = [];
  public loading = faSpinner;
  public sortedData: any[];
  parametros: Observable<Parametros>;
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private store: Store) { }

  ngOnInit() {
    this.accionFG = this.fb.group({
      ejecutada: [[]],
      ejecutar: [[]]
    });

    this.parametros = this.store.select(state =>
      state.parametros
    );

  }

  generarReporte = () => {
    this.proyectos = [];
    this.sortedData = this.proyectos.slice();
    const datos = this.accionFG.value;
    this.accionFG.reset();
    this.accionFG.setValue({
      ejecutada: datos.ejecutada,
      ejecutar: datos.ejecutar
    });
    this.consultando = true;
    this.apiService
      .reporteSeguimiento(datos)
      .pipe(
        tap((res: { seguimiento: string }) => {
          const token = this.store.selectSnapshot(AuthState.token);
          this.proyectos = this.apiService.decrypt(res.seguimiento, token);
          this.sortedData = this.proyectos.slice();
        }),
        finalize(() => (this.consultando = false)),
        catchError(err => {
          this.consultando = false;
          return of();
        })
      )
      .subscribe();
  }

  sortData(sort) {
    const data = this.proyectos.slice();

    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'expediente':
          return this.compare(a.expediente, b.expediente, isAsc);
        case 'proyecto':
          return this.compare(a.proyecto, b.proyecto, isAsc);
        case 'fase':
          return this.compare(a.fase, b.fase, isAsc);
        case 'ejecutada':
          return this.compare(a.ejecutada, b.ejecutada, isAsc);
        case 'ejecutar':
          return this.compare(a.ejecutar, b.ejecutar, isAsc);
        default:
          return 0;
      }
    });
  }

  compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  irProyecto = (id) => {
    this.store.dispatch(new Navigate(['/gestion/proyectos/actual/', id]));
  }
}
