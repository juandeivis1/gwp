import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrioridadProyectosComponent } from './prioridad-proyectos.component';

describe('PrioridadProyectosComponent', () => {
  let component: PrioridadProyectosComponent;
  let fixture: ComponentFixture<PrioridadProyectosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrioridadProyectosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrioridadProyectosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
