import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { tap, finalize, catchError } from 'rxjs/operators';
import { empty, Observable } from 'rxjs';
// import { Prioridades, Portafolios } from '../../listas/listas';
import { FormGroup, FormBuilder } from '@angular/forms';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';
import { Parametros } from '../../interfaces/Parametros.interface';

@Component({
  selector: 'app-prioridad-proyectos',
  templateUrl: './prioridad-proyectos.component.html',
  styleUrls: ['./prioridad-proyectos.component.css']
})
export class PrioridadProyectosComponent implements OnInit {
  proyectos: any[] = [];
  accionFG: FormGroup;
  consultando: boolean = false;
  parametros: Observable<Parametros>
  public loading = faSpinner;


  constructor(
    private apiService: ApiService, 
    private fb: FormBuilder,
    private store :Store
  ) { }

  ngOnInit() {
    this.accionFG = this.fb.group({
      portafolio: ['Periodo 2018'],
      prioridad: [[]]
    })

    this.parametros = this.store.select(state =>
      state.parametros
    );   
  }

  generarReporte = () => {
    this.proyectos = [];
    let datos = this.accionFG.value;
    this.accionFG.reset();
    this.accionFG.setValue({
      portafolio: datos.portafolio,
      prioridad: datos.prioridad
    });
    this.consultando = true;
    this.apiService
      .reportePrioridad(datos)
      .pipe(
        tap((proyectos: any[]) => {
          this.proyectos = proyectos;
        }),
        finalize(() => (this.consultando = false)),
        catchError(() => {
          this.consultando = false;
          return empty();
        })
      )
      .subscribe();
  };

  irProyecto = (id) => {
    this.store.dispatch(new Navigate(["/gestion/proyectos/actual/", id]));
  };

}
