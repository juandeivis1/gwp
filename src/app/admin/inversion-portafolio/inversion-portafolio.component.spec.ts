import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InversionPortafolioComponent } from './inversion-portafolio.component';

describe('InversionPortafolioComponent', () => {
  let component: InversionPortafolioComponent;
  let fixture: ComponentFixture<InversionPortafolioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InversionPortafolioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InversionPortafolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
