import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { GetCostosPortafolio } from '../../states/admin.state';
import { Observable } from 'rxjs/internal/Observable';
import { Portafolio } from '../../interfaces/Admin.interface';
import { tap } from 'rxjs/operators';
import { DecimalPipe } from '@angular/common';
import { AuthState } from 'src/app/states/auth.state';

@Component({
  selector: 'app-inversion-portafolio',
  templateUrl: './inversion-portafolio.component.html',
  styleUrls: ['./inversion-portafolio.component.css']
})
export class InversionPortafolioComponent implements OnInit {

    public incomeOptions: any = {
      scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
            }
        }],
        xAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
    },
    responsive: true,
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          let label = data.labels[tooltipItem.index] || '';
          if (label) {
            label += ': ₡';
          }
          label += this.dp.transform(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
          return label;
        }
      }
    }
  };

  public pieOptions: any = {
    pieceLabel: {
      render: 'value'
    },
    responsive: true,
    // legend: {
    //   display: false,
    // }
  };

  constructor(private store: Store, private dp: DecimalPipe) { }
  costos: Observable<Portafolio[]>;
  portafolios: string[] = [];
  resultados: Array<any> = [{data: [], label: 'Inversión'}, {data: [], label: 'Previsión'}];
  imprimir: Array<boolean> = [];

  ngOnInit() {
    this.refrescar();
    this.costos = this.store.select(state => state.admin.portafolios).pipe(tap(portafolios => {
      this.portafolios = [];
      this.imprimir = [];
      this.resultados = [{data: [], label: 'Inversion'}, {data: [], label: 'Prevision'}]
      portafolios.map((costo: Portafolio) => {
        this.imprimir.push(false);
        this.portafolios.push(costo.portafolio),
        this.resultados[0].data.push(costo.resumen.inversion);
        this.resultados[1].data.push(costo.resumen.prevision);
      });
    }));
  }

  refrescar = () => {
    const token = this.store.selectSnapshot(AuthState.token);
     this.store.dispatch(new GetCostosPortafolio(token));
  }

}
