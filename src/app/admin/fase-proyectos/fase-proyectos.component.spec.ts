import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseProyectosComponent } from './fase-proyectos.component';

describe('FaseProyectosComponent', () => {
  let component: FaseProyectosComponent;
  let fixture: ComponentFixture<FaseProyectosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaseProyectosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaseProyectosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
