import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { tap, finalize, catchError } from 'rxjs/operators';
import { empty, Observable } from 'rxjs';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';
import { Parametros } from '../../interfaces/Parametros.interface';

@Component({
  selector: 'app-fase-proyectos',
  templateUrl: './fase-proyectos.component.html',
  styleUrls: ['./fase-proyectos.component.css']
})
export class FaseProyectosComponent implements OnInit {

  filtros: FormGroup
  consultando: boolean = false;
  sortedData: any[];
  proyectos: any[] = [];
  loading = faSpinner;

  parametros: Observable<Parametros>

  constructor(
    private fb: FormBuilder, 
    private apiService: ApiService,
    private store: Store) { }

  ngOnInit() {
    this.filtros = this.fb.group({
      fase: ['Ejecución'],
      portafolio: [[], [Validators.required]],
      ingenieria: [[]]
    });

    this.parametros = this.store.select(state =>
      state.parametros
    );    
  }

  generarReporte = () => {
    this.proyectos = [];
    this.sortedData = this.proyectos.slice();
    let datos = this.filtros.value;
    
    this.filtros.reset();
    this.filtros.setValue({
      fase: datos.fase,
      portafolio: datos.portafolio,
      ingenieria: datos.ingenieria
    });
    this.consultando = true;
    this.apiService
      .reporteFase(datos)
      .pipe(
        tap((proyectos: any) => {
          this.proyectos = proyectos;
          this.sortedData = this.proyectos.slice();
        }),
        finalize(() => (this.consultando = false)),
        catchError(err => {
          this.consultando = false;
          return empty();
        })
      )
      .subscribe();
  };

  irProyecto = (id) => {
    this.store.dispatch(new Navigate(["/gestion/proyectos/actual/", id]));
  };

}
