import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'chart'
})
export class ChartPipe implements PipeTransform {

  transform(data: any, args?: any): any {
    const labels = [];
    const costo = [];
    const poblacion = [];
    const volumen = [];
    const percapita = [];
    if (data.sumatorias) {
      data.sumatorias.map(
        programa => {
          labels.push(programa.programa);
          costo.push(programa.sumatoria.costoReal);
          poblacion.push(programa.sumatoria.poblacion);
          volumen.push(programa.sumatoria.proyectos.length);
          if (programa.sumatoria.costoReal > 0 && programa.sumatoria.poblacion > 0) {
            percapita.push(programa.sumatoria.costoReal / programa.sumatoria.poblacion);
          } else {
            percapita.push(0);
          }
        }
      );
    } else
      if (data) {
        data.map(
          portafolio => {
            labels.push(portafolio.portafolio);
            costo.push(portafolio.sumatoria.costoReal);
            poblacion.push(portafolio.sumatoria.poblacion);
            volumen.push(portafolio.sumatoria.volumen);
            if (portafolio.sumatoria.costoReal > 0 && portafolio.sumatoria.poblacion > 0) {
              percapita.push(portafolio.sumatoria.costoReal / portafolio.sumatoria.poblacion);
            } else {
              percapita.push(0);
            }
          }
        );
      }
    return { labels, costo, poblacion, volumen, percapita };
  }

}
