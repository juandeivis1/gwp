import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { GetPendientes } from '../../states/admin.state';
import { AuthState } from 'src/app/states/auth.state';

@Component({
  selector: 'app-pendientes',
  templateUrl: './pendientes.component.html',
  styleUrls: ['./pendientes.component.css']
})
export class PendientesComponent implements OnInit {
  pendientes: any;

  constructor(private store: Store) { }

  ngOnInit() {
    this.refrescar()
    this.pendientes = this.store.select(state =>
      state.admin.pendientes
    );
  }

  refrescar = () => {
    const token = this.store.selectSnapshot(AuthState.token);
    this.store.dispatch(new GetPendientes(token))
  }


}
