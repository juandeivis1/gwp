import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InversionProgramaComponent } from './inversion-programa/inversion-programa.component';
import { PlanRiegosComponent } from './plan-riegos/plan-riegos.component';
import { FaseProyectosComponent } from './fase-proyectos/fase-proyectos.component';
import { PrioridadProyectosComponent } from './prioridad-proyectos/prioridad-proyectos.component';
import { InversionPortafolioComponent } from './inversion-portafolio/inversion-portafolio.component';
import { SeguimientoProyectosComponent } from './seguimiento-proyectos/seguimiento-proyectos.component';
import { PendientesComponent } from './pendientes/pendientes.component';
import { ModalidadComponent } from './modalidad/modalidad.component';
import { ResumenComponent } from './resumen/resumen.component';
import { GestionComponent } from '../core/gestion/gestion.component';
import { FechaComponent } from './fecha/fecha.component';
import { PortafolioProgramaComponent } from './portafolio-programa/portafolio-programa.component';



const routes: Routes = [
  {
    path: '', component: GestionComponent, children:
      [
      { path: 'programa', component: InversionProgramaComponent },
      { path: 'plan/:proId', component: PlanRiegosComponent },
      { path: 'fase', component: FaseProyectosComponent },
      { path: 'prioridad', component: PrioridadProyectosComponent },
      { path: 'portafolio', component: InversionPortafolioComponent },
      { path: 'seguimiento', component: SeguimientoProyectosComponent },
      { path: 'pendientes', component: PendientesComponent },
      { path: 'modalidad', component: ModalidadComponent },
      { path: 'resumen', component: ResumenComponent },
      { path: 'fecha', component: FechaComponent },
      { path: 'porpro', component: PortafolioProgramaComponent },
      ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
