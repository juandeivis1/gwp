import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';

import { ResumenComponent } from './resumen/resumen.component';
import { ModalidadComponent } from './modalidad/modalidad.component';
import { PendientesComponent } from './pendientes/pendientes.component';
import { SeguimientoProyectosComponent } from './seguimiento-proyectos/seguimiento-proyectos.component';
import { InversionPortafolioComponent } from './inversion-portafolio/inversion-portafolio.component';
import { PrioridadProyectosComponent } from './prioridad-proyectos/prioridad-proyectos.component';
import { FaseProyectosComponent } from './fase-proyectos/fase-proyectos.component';
import { PlanRiegosComponent } from './plan-riegos/plan-riegos.component';
import { InversionProgramaComponent } from './inversion-programa/inversion-programa.component';
import { CoreModule } from '../core/core.module';
import 'chart.piecelabel.js';
import { FechaComponent } from './fecha/fecha.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatListModule } from '@angular/material/list';
import { PortafolioProgramaComponent } from './portafolio-programa/portafolio-programa.component';
import { ChartsModule } from 'ng2-charts';
import { ChartPipe } from './chart.pipe';
import { MatProgressBarModule } from '@angular/material/progress-bar';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    CoreModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    FlexLayoutModule,
    MatListModule,
    ChartsModule,
    MatProgressBarModule,
  ],
  declarations: [
    ResumenComponent,
    ModalidadComponent,
    PendientesComponent,
    SeguimientoProyectosComponent,
    InversionPortafolioComponent,
    PrioridadProyectosComponent,
    FaseProyectosComponent,
    PlanRiegosComponent,
    InversionProgramaComponent,
    FechaComponent,
    PortafolioProgramaComponent,
    ChartPipe,
  ]
})
export class AdminModule { }