import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanRiegosComponent } from './plan-riegos.component';

describe('PlanRiegosComponent', () => {
  let component: PlanRiegosComponent;
  let fixture: ComponentFixture<PlanRiegosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanRiegosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanRiegosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
