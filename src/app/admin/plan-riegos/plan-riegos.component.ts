import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { AuthState } from 'src/app/states/auth.state';
import { switchMap } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { SincronizarProyecto } from 'src/app/states/proyectos';

@Component({
  selector: 'app-plan-riegos',
  templateUrl: './plan-riegos.component.html',
  styleUrls: ['./plan-riegos.component.css']
})
export class PlanRiegosComponent implements OnInit {
  private proId: number;
  proyecto$: Observable<any>;

  constructor (
    private route: ActivatedRoute,
    private store: Store
    ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.proId = Number(params['proId']);
      const token = this.store.selectSnapshot(AuthState.token);
      this.store.dispatch(new SincronizarProyecto(this.proId, token));
      this.proyecto$ = this.store.select(state => state.proyectos.find(proyecto => proyecto.proId === this.proId))
      .pipe(
        switchMap(_pro => {
          return of({nombre: _pro.nombre, riesgos: _pro.riesgos, proId: _pro.proId, expediente: _pro.expediente });
        })
      );
    });
  }

}
