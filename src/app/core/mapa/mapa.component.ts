/// <reference types='@types/googlemaps' />
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Input,
  Output,
  EventEmitter,
  HostListener,
  ChangeDetectorRef,
  AfterViewInit
} from '@angular/core';
import { Map, Marker } from 'mapbox-gl';
import { ServicioCoordenadas } from './coordenadas';
import { Observable } from 'rxjs';
import { CaminosComponent } from '../caminos/caminos.component';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngxs/store';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MediaMatcher } from '@angular/cdk/layout';
import * as moment from 'moment';
import { FiltrosProyectosComponent } from '../filtros-proyectos/filtros-proyectos.component';
import { FiltrosCaminosComponent } from '../filtros-caminos/filtros-caminos.component';
declare var window;

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})

export class MapaComponent implements OnInit, AfterViewInit {
  @Input() type: string = null;
  @Input() zoom = 11;
  @Input() agregar = false;
  @Input() lat = 9.372871062496355;
  @Input() lng = -83.70275798608571;
  @Input() distritos = true;
  @Input() fillColor = '#0084bc';
  @Input() strokeColor = '#0084bc';
  @Input() strokeWeight = 2;
  @Input() radio = 100;

  @Output() ready: EventEmitter<any> = new EventEmitter();
  @Output() saveData: EventEmitter<any> = new EventEmitter();
  @Output() updateType: EventEmitter<any> = new EventEmitter();
  @HostListener('window:resize', ['$event'])

  mobileQuery: MediaQueryList;

  private proyectos;
  private mapBox: Map;

  private caminos: any[] = [];
  private markers: any[] = [];

  private marker: Marker = null;

  caminosFiltrados: Observable<any[]>;
  proyecto: any = null;

  @ViewChild('map', { static: false }) mapElement: ElementRef;
  @ViewChild('gmap', { static: false }) gmapElement: any;

  map: google.maps.Map;

  private _mobileQueryListener: () => void;
  height: number;
  maps: any;
  markerCluster: any;
  heatmap: any;
  calorActive = false;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.height = event.target.innerHeight;
  }

  constructor(
    private bottomSheet: MatBottomSheet,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private location: Location,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private dialog: MatDialog,
    private store: Store
  ) {
    this.height = window.innerHeight;
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.maps = window.google.maps;
  }

  ngAfterViewInit() {
    this.initMap();
  }

  ngOnInit() {
    const proId = this.route.snapshot.paramMap.get('proId');
    if (proId) {
      this.proyecto = this.store.selectSnapshot(state => state.proyectos).find(pro => pro.proId === proId);
      const coords = this.proyecto.informacion.coordenadas.map(_coords => Object.assign({ lat: _coords[1], lng: _coords[0] }));
      this.setQueryProject(this.proyecto, coords);
    }
  }

  setQueryProject = (proyecto, coords) => {
    const project = new this.maps.Polyline({
      path: coords,
      geodesic: true,
      strokeColor: '#0299ff',
      strokeOpacity: 1.0,
      strokeWeight: 2,
    });
    project.setMap(this.map);
    const marker = new google.maps.Marker({
      position: coords[0],
      map: this.map,
      title: proyecto.sinonimo
    });
    marker.addListener('click', () => {
      this.bottomSheet.open(CaminosComponent, { data: proyecto });
      this.map.setZoom(17);
      this.map.panTo(marker.getPosition());
    });
    const latLng = new this.maps.LatLng({ lat: coords[0].lat, lng: coords[0].lng });
    this.map.setZoom(17);
    this.map.panTo(latLng);
  }

  initMap = () => {
    const latLng = new this.maps.LatLng(this.lat, this.lng);
    const mapProp = {
      center: latLng,
      zoom: this.zoom,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new this.maps.Map(this.gmapElement.nativeElement, mapProp);
    const image = {
      url: 'https://perezzeledon.go.cr:803/assets/icons/gv-icon-512x512.png',
      scaledSize: new this.maps.Size(30, 30),
    };
    new this.maps.Marker({ position: { lat: this.lat, lng: this.lng }, map: this.map, title: 'Municipalidad de Pérez Zeledón', icon: image });
    this.polylineDistritos();
  }

  private polylineDistritos() {
    const listaPuntosDistritos: any[] = new ServicioCoordenadas().getCoordenadasDistrito();

    const listaColores: any[] = [
      '#996633',
      '#336600',
      '#0066ff',
      '#ff6600',
      '#3333ff',
      '#ff0000',
      '#990099',
      '#ffcc00',
      '#44357A',
      '#993366',
      '#1EB635',
      '#3399ff'
    ];
    let x = 0;
    for (const listaPuntos of listaPuntosDistritos) {
      const listaPuntosDistrito: any[] = listaPuntos.split(' ');
      const listaPuntosProcesada: any[] = [];

      for (const parPuntos of listaPuntosDistrito) {
        const temp = parPuntos.split(',');
        listaPuntosProcesada.push({
          lat: parseFloat(temp[1]),
          lng: parseFloat(temp[0])
        });
      }

      const polygonDistrito = new this.maps.Polyline({
        path: listaPuntosProcesada,
        strokeColor: listaColores[x],
        strokeOpacity: 0.4,
        strokeWeight: this.strokeWeight
      });
      polygonDistrito.setMap(this.map);
      x = x + 1;
    }
  }

  filtros = (tipo: boolean) => {
    if (tipo) {
      const after = this.dialog.open(FiltrosProyectosComponent);
      after.afterClosed().subscribe(projectos => {
        if (projectos) {
          if (projectos.length === 0) {
            this.snackBar.open('No se encontraron resultados', 'Ok', { duration: 2500 });
          }
          this.removeOldDate();
          this.setDataProjects(projectos);
        }
      });
    } else {
      const after = this.dialog.open(FiltrosCaminosComponent);
      after.afterClosed().subscribe(roads => {
        if (roads) {
          if (roads.length === 0) {
            this.snackBar.open('No se encontraron resultados', 'Ok', { duration: 2500 });
          }
          this.removeOldDate();
          this.setDataRoads(roads);
        }
      });
    }
  }

  removeOldDate = () => {
    this.caminos.map(x => {
      x.setMap(null);
    });
    this.markers.map(x => {
      x.setMap(null);
    });
    this.caminos = [];
    this.markers = [];
    this.heatmap ? this.heatmap.setMap(null) : null;
    this.calorActive = false;
  }

  setDataProjects = (proyects) => {
    proyects.map(proyecto => {
      if (proyecto.coordenadas) {
        const project = new this.maps.Polyline({
          path: proyecto.coordenadas,
          geodesic: true,
          strokeColor: '#0299ff',
          strokeOpacity: 1.0,
          strokeWeight: 2,
        });
        project.setMap(this.map);
        this.markerProject(proyecto);
        this.caminos.push(project);
      }
    });
    if (proyects.length === 1) {
      const latLng = new this.maps.LatLng({ lat: proyects[0].coordenadas[0].lat, lng: proyects[0].coordenadas[0].lng });
      this.map.setZoom(17);
      this.map.panTo(latLng);
    } else {
      this.map.setZoom(11);
    }
  }

  calor = ({ checked }) => {
    if (checked) {
      const heatmapData = this.markers.map(marker => {
        marker.setMap(null);
        return marker.getPosition();
      });
      this.heatmap = new this.maps.visualization.HeatmapLayer({
        data: heatmapData
      });
      this.heatmap.setMap(this.map);
      this.heatmap.set('radius', 20);
      this.calorActive = true;
    } else {
      this.markers.map(marker => {
        marker.setMap(this.map);
      });
      this.heatmap.setMap(null);
    }
  }

  markerProject = (proyecto) => {
    const marker = new google.maps.Marker({
      position: proyecto.coordenadas[0],
      map: this.map,
      title: proyecto.sinonimo
    });
    marker.addListener('click', () => {
      this.bottomSheet.open(CaminosComponent, { data: proyecto });
      this.map.setZoom(17);
      this.map.panTo(marker.getPosition());
    });
    this.markers.push(marker);
  }

  setDataRoads = (roads) => {
    roads.map(camino => {
      if (camino.coordenadas) {
        const road = new this.maps.Polyline({
          path: camino.coordenadas,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2,
        });
        road.setMap(this.map);
        this.markerRoad(camino);
        this.caminos.push(road);
      }
    });
    if (roads.length === 1) {
      const latLng = new this.maps.LatLng({ lat: roads[0].coordenadas[0].lat, lng: roads[0].coordenadas[0].lng });
      this.map.setZoom(17);
      this.map.panTo(latLng);
    } else {
      this.map.setZoom(11);
    }
  }

  markerRoad = (road) => {
    const marker = new google.maps.Marker({
      position: road.coordenadas[0],
      map: this.map,
      title: road.codigo
    });
    marker.addListener('click', () => {
      this.bottomSheet.open(CaminosComponent, { data: road });

      this.map.setZoom(17);
      this.map.panTo(marker.getPosition());
    });
    this.markers.push(marker);
  }

  // Codigo nuevo, google maps

  momentTS = ts =>
    moment(ts)
      .locale('es-us')
      .calendar()

  atras(): void {
    this.location.back();
  }

}
