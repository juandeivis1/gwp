import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store, Actions, ofActionSuccessful } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { AuthState } from '../../states/auth.state';
import { CompartirArchivo } from 'src/app/states/proyectos';

@Component({
  selector: 'app-compartir',
  templateUrl: './compartir.component.html',
  styleUrls: ['./compartir.component.css']
})
export class CompartirComponent implements OnInit {
  equipo: Observable<any>;
  enviar: string[] = []
  user: any;

  constructor(
    private store: Store,
    public dialogRef: MatDialogRef<CompartirComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private actions: Actions
  ) {
  }

  ngOnInit() {
    this.user = this.store.selectSnapshot(AuthState.user)
    this.equipo = this.store.select(state =>
      state.parametros.equipo
    )
  }

  enviarLink = () => {
    let datos = Object.assign(this.data, { users: this.enviar })
    this.store.dispatch(new CompartirArchivo(datos))
    this.actions
      .pipe(ofActionSuccessful(CompartirArchivo))
      .subscribe(res => {
        this.dialogRef.close()
      });

  }

}
