import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store, Actions } from '@ngxs/store';
import { Archivo } from '../../interfaces/Archivo.interface';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NuevoArchivo } from '../../states/proyectos';

@Component({
  selector: 'app-archivo',
  templateUrl: './archivo.component.html',
  styleUrls: ['./archivo.component.css']
})
export class ArchivoComponent implements OnInit {
  nuevoArchivo: FormGroup
  fases: string[] = ['Preinversión', 'Ejecución', 'Operación', 'Detenido']

  constructor(
    private store: Store,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ArchivoComponent>,
    private snack: MatSnackBar,
    private actions: Actions,
    @Inject(MAT_DIALOG_DATA) public data: { fase: string, type: string, proId: number, user: string, archivo?: Archivo }
  ) { }

  ngOnInit() {
    if (this.data.archivo) {
      const archivo = this.data.archivo;
      this.nuevoArchivo = this.fb.group({
        fileId: archivo.fileId,
        fase: archivo.fase,
        type: archivo.type,
        proId: archivo.proId,
        user: archivo.user,
        description: [archivo.description, [Validators.required, Validators.minLength(10), Validators.pattern("^[^'\"]*$")]],
        url: [archivo.url, [Validators.required]],
        ts: archivo.ts,
      })
    } else {
      this.nuevoArchivo = this.fb.group({
        fase: this.data.fase,
        type: this.data.type,
        proId: this.data.proId,
        user: this.data.user,
        description: ['', [Validators.required, Validators.minLength(10), Validators.pattern("^[^'\"]*$")]],
        url: ['', [Validators.required]]
      })
    }
  }

  get description() {
    return this.nuevoArchivo.get('description')
  }

  get url() {
    return this.nuevoArchivo.get('url')
  }

  subirArchivo = () => {
    let tempArchivo = this.nuevoArchivo.value;
    if (!tempArchivo.ts) {
      tempArchivo = Object.assign(tempArchivo, { ts: Date.now() })
    }
    this.store.dispatch(new NuevoArchivo(tempArchivo)).subscribe(() => {
      this.dialogRef.close(true)
      this.snack.open("Archivo exitoso", "Ok",{duration: 2500});
    });
  }

}
