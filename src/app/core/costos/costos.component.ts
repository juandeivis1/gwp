import { Component, OnInit, Input } from "@angular/core";
import { faEdit, faSave } from "@fortawesome/free-regular-svg-icons";
import {
  faTimes,
  faPlusCircle,
  faReplyAll,
  faSyncAlt,
  faCheck
} from "@fortawesome/free-solid-svg-icons";
import { Costos } from "../../interfaces/Costos.interface";
import { Observable, of } from "rxjs";
import {
  Store,
  ofActionSuccessful,
  Actions,
  ofActionErrored
} from "@ngxs/store";
import { DecimalPipe } from '@angular/common';
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NuevosCostos } from "../../states/proyectos";
import * as _ from 'lodash';
@Component({
  selector: "app-costos",
  templateUrl: "./costos.component.html",
  styleUrls: ["./costos.component.css"]
})
export class CostosComponent implements OnInit {
  @Input("proId") proId: number;

  @Input("costos")
  set costosNuevos(costos: Costos) {
    this.costos$ = _.isEmpty(costos) ? of(null) : of(costos)
    _.isEmpty(costos) ? of(null) : this.cargarGrafico(costos)
  }

  @Input("editor") editor: boolean;


  public editar = faEdit;
  public cancelar = faTimes;
  public guardar = faSave;
  public mas = faPlusCircle;
  public todas = faReplyAll;
  public sync = faSyncAlt;
  public listo = faCheck;
  public trabajando = false;

  public editando: boolean = false;

  public costos: FormGroup;

  public costos$: Observable<Costos>;
  private sumatoria: { monto: string, etiqueta: string }[] = [
    { monto: "adjudicado", etiqueta: 'Adjudicado' },
    { monto: "calidad", etiqueta: 'Control de calidad' },
    { monto: "comunales", etiqueta: 'Aportes Comunales' },
    { monto: "ordenServicio", etiqueta: 'Orden de servicio' },
    { monto: "especie", etiqueta: 'Aportes en especie' },
    { monto: "especifica", etiqueta: 'Partida específica' },
    { monto: "reajuste", etiqueta: 'Reajuste' },
    { monto: "ampliacion", etiqueta: 'Ampliación' },
    { monto: "instituciones", etiqueta: 'Instituciones' },
    { monto: "extra1", etiqueta: 'Extra 1' },
    { monto: "extra2", etiqueta: 'Extra 2' }
  ];
  public costoTotal: number;
  public administrativo: number;

  public incomeOptions: any = {
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          var label = data.labels[tooltipItem.index] || "";
          if (label) {
            label += ": ₡";
          }
          label += this.dp.transform(data.datasets[0].data[tooltipItem.index]);
          return label;
        }
      }
    }
  };

  public chart: any = { labels: [], data: [] };

  constructor(
    private store: Store,
    private actions: Actions,
    private dp: DecimalPipe,
    private snackBar: MatSnackBar,
    private fb: FormBuilder) {
    actions.pipe(ofActionSuccessful(NuevosCostos)).subscribe((res) => {
      this.trabajando = false;
    })
    actions.pipe(ofActionErrored(NuevosCostos)).subscribe(res => {
      this.trabajando = false;
      this.snackBar.open('Error de comunicación con MPZ...', 'Ok');
    })
  }

  ngOnInit() { }

  editarDatos = () => {
    this.editando = true;
    this.costos$.subscribe(costos => costos ? this.cargarDatos(costos) : this.cargarDatosNuevos());
  };

  private cargarDatos = (costos: Costos) => {
    this.costos = this.fb.group({
      procedencia: [costos.procedencia || null, [Validators.pattern("^[^'\"]*$")]],
      prevision: [costos.prevision || null],
      estimado: [costos.estimado || null],
      adjudicado: [costos.adjudicado || null],
      calidad: [costos.calidad || null],
      comunales: [costos.comunales || null],
      ordenServicio: [costos.ordenServicio || null],
      especie: [costos.especie || null],
      especifica: [costos.especifica || null],
      ampliacion: [costos.ampliacion || null],
      reajuste: [costos.reajuste || null],
      instituciones: [costos.instituciones || null],
      extra1: [costos.extra1 || null],
      extra2: [costos.extra2 || null],
    })
  };

  private cargarDatosNuevos = () => {
    this.costos = this.fb.group({
      procedencia: [null, [Validators.pattern("^[^'\"]*$")]],
      prevision: [null],
      estimado: [null],
      adjudicado: [null],
      calidad: [null],
      comunales: [null],
      ordenServicio: [null],
      especie: [null],
      especifica: [null],
      ampliacion: [null],
      reajuste: [null],
      instituciones: [null],
      extra1: [null],
      extra2: [null],
    })
  };

  cancelarCostos = () => {
    this.editando = false;
    this.costos.reset()
  };

  guardarCostos = () => {
    this.trabajando = true;
    this.store.dispatch(new NuevosCostos(this.proId, this.costos.value));
    this.actions.pipe(ofActionSuccessful(NuevosCostos)).subscribe(() => {
      this.trabajando = false;
      this.snackBar.open('Se ha actualizo de manera correta las adquisiciones', 'Ok', {
        duration: 2500
      });
      this.cancelarCostos();
    });
  };

  cargarGrafico(costos: Costos) {
    if (costos) {
      this.chart = this.sumatoria.reduce(
        (chart, campo) => {
          if (costos[campo.monto]) {
            chart.labels.push(campo.etiqueta);
            chart.data.push(costos[campo.monto]);
          }
          return chart;
        },
        { labels: [], data: [] }
      );

      this.costoTotal = this.chart.data.reduce((costoTotal, actualCosto) => {
        costoTotal += actualCosto;
        return costoTotal;
      }, 0);
      if (this.costoTotal > 0) {
        this.chart.labels.push("Costo administrativo");
        this.administrativo = this.costoTotal * 0.05;
        this.chart.data.push(this.administrativo);
        this.costoTotal += this.administrativo;
      }
    }
  }
}
