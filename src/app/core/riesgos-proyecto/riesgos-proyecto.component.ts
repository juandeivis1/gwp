import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Riesgo } from '../../interfaces/Riesgo.interface';
import { Observable, of } from 'rxjs';
import { Store, ofActionSuccessful, Actions, ofActionErrored } from '@ngxs/store';
import { EliminarRiesgo } from 'src/app/states/proyectos';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-riesgos-proyecto',
  templateUrl: './riesgos-proyecto.component.html',
  styleUrls: ['./riesgos-proyecto.component.css']
})
export class RiesgosProyectoComponent implements OnInit {
  @Input('riesgos')
  set riesgos(riesgos: Observable<Riesgo[]>) {
    this.riesgo$ = riesgos;
  }
  @Input() edit: boolean;

  riesgo$: Observable<Riesgo[]>;
  constructor(
    private store: Store,
    private actions: Actions,
    private snack: MatSnackBar
  ) { }

  ngOnInit() { }

  aceptable = (prob: number, imp: number) => {
    return prob < 4 && imp < 4;
  }

  monitorear = (prob: number, imp: number) => {
    return (
      (prob > 3 && prob <= 7 && imp <= 3) ||
      (prob <= 3 && imp > 3 && imp <= 6) ||
      (prob <= 3 && imp > 6)
    );
  }

  reducir = (prob: number, imp: number) => {
    return prob > 7 && imp > 0 && imp < 4;
  }

  contingencia = (prob: number, imp: number) => {
    return (
      (prob > 3 && prob <= 7 && imp > 3 && imp < 7) ||
      (prob > 3 && prob <= 7 && imp > 6 && imp <= 10)
    );
  }

  accion = (prob: number, imp: number) => {
    return (prob > 7 && imp > 3 && imp < 7) || (prob > 7 && imp > 6);
  }

  quitar = (riesgo) => {
    this.store.dispatch(new EliminarRiesgo(riesgo));
    this.actions.pipe(ofActionSuccessful(EliminarRiesgo))
      .subscribe(() => {
        this.snack.open('Riesgo elinado con exito', 'Ok', { duration: 3000 });
      });
    this.actions.pipe(ofActionErrored(EliminarRiesgo))
      .subscribe(() => {
        this.snack.open('Error elinado riesgo, intentelo de nuevo', 'Ok');
      });
  }
}
