import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiesgosProyectoComponent } from './riesgos-proyecto.component';

describe('RiesgosProyectoComponent', () => {
  let component: RiesgosProyectoComponent;
  let fixture: ComponentFixture<RiesgosProyectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiesgosProyectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiesgosProyectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
