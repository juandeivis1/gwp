import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Riesgo } from 'src/app/interfaces/Riesgo.interface';
import { ActualizarRiesgo } from 'src/app/states/proyectos';
import { ofActionSuccessful, ofActionErrored, Actions, Store } from '@ngxs/store';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-riesgos-resultados',
  templateUrl: './riesgos-resultados.component.html',
  styleUrls: ['./riesgos-resultados.component.css']
})
export class RiesgosResultadosComponent implements OnInit {
  @Input('resultados')
  set riesgos(riegosProyecto: Riesgo[]) {
    riegosProyecto.reduce(this.getTipo, this.resultados);
    this.resultados.promAceptables = this.obtenerPromedio(this.resultados.Aceptables);
    this.resultados.promMonitoreos = this.obtenerPromedio(this.resultados.Monitoreos);
    this.resultados.promReducir = this.obtenerPromedio(this.resultados.Reducir);
    this.resultados.promContingencias = this.obtenerPromedio(this.resultados.Contingencias);
    this.resultados.promImnediatos = this.obtenerPromedio(this.resultados.Imnediatos);

    console.log(this.resultados);
  }
  resultados: any = {
    Aceptables: [],
    Monitoreos: [],
    Reducir: [],
    Contingencias: [],
    Imnediatos: [],
    promAceptables: 0,
    promMonitoreos: 0,
    promReducir: 0,
    promContingencias: 0,
    promImnediatos: 0,
    Evaluados: false
  };

  public canvasWidth = 250;
  public centralLabel = '';
  public name = 'Promedio de riesgos';
  public bottomLabel = '65';
  public options = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 1000,
    arcColors: ['#ccf1ff', '#92e1ff', '#54d0ff', '#ffaaaa', '#ff7373', '#ff4f4f'],
    arcDelimiters: [10, 20, 30, 40, 70],
    rangeLabel: ['0', '100'],
    needleStartValue: 0
  };

  @Output() guardar: EventEmitter<any> = new EventEmitter();
  @Input() edit = true;

  constructor(
    private snack: MatSnackBar,
    private store: Store,
    private actions: Actions
  ) { }

  ngOnInit() { }

  getTipo = (riesgos, riesgo) => {
    // Inmmediato
    if (
      (riesgo.prob > 7 && riesgo.imp > 3 && riesgo.imp < 7) ||
      (riesgo.prob > 7 && riesgo.imp > 6)
    ) {
      riesgos.Imnediatos = [...riesgos.Imnediatos, riesgo];
      riesgos.Evaluados = true;
    }
    // Contingencias
    if (
      (riesgo.prob > 3 && riesgo.prob <= 7 && riesgo.imp > 3 && riesgo.imp < 7) ||
      (riesgo.prob > 3 && riesgo.prob <= 7 && riesgo.imp > 6 && riesgo.imp <= 10)
    ) {
      riesgos.Contingencias = [...riesgos.Contingencias, riesgo];
      riesgos.Evaluados = true;
    }
    // Reducir
    if (
      riesgo.prob > 7 && riesgo.imp > 0 && riesgo.imp < 4
    ) {
      riesgos.Reducir = [...riesgos.Reducir, riesgo];
      riesgos.Evaluados = true;
    }
    // Aceptables
    if (
      riesgo.prob < 4 && riesgo.imp < 4
    ) {
      riesgos.Aceptables = [...riesgos.Aceptables, riesgo];
      riesgos.Evaluados = true;
    }
    // Monitoreos
    if (
      (riesgo.prob > 3 && riesgo.prob <= 7 && riesgo.imp <= 3) ||
      (riesgo.prob <= 3 && riesgo.imp > 3 && riesgo.imp <= 6) ||
      (riesgo.prob <= 3 && riesgo.imp > 6)
    ) {
      riesgos.Monitoreos = [...riesgos.Monitoreos, riesgo];
      riesgos.Evaluados = true;
    }
    return riesgos;
  }

  obtenerPromedio = (resultados) => {
    return resultados
      .map(x => {
        return x.prob * x.imp;
      })
      .reduce((total, x) => {
        return total + x;
      }, 0) / resultados.length;

  }

  get hasRiesgosEvaluados() {
    return (
      !this.hasAceptables &&
      !this.hasMonitoreos &&
      !this.hasReducir &&
      !this.hasContingencias &&
      !this.hasImnediatos
    );
  }

  get hasAlgunoEvaluado() {
    return (
      this.hasAceptables ||
      this.hasMonitoreos ||
      this.hasReducir ||
      this.hasContingencias ||
      this.hasImnediatos
    );
  }

  get hasImnediatos() {
    return this.Imnediatos.length > 0;
  }

  get Imnediatos() {
    return this.resultados.filter(x => {
      return (
        (x.prob > 7 && x.imp > 3 && x.imp < 7) || (x.prob > 7 && x.imp > 6)
      );
    });
  }

  get promImnediatos() {
    return this.obtenerPromedio(this.Imnediatos);
  }

  get hasContingencias() {
    return this.Contingencias.length > 0;
  }

  get Contingencias() {
    return this.resultados.filter(x => {
      return (
        (x.prob > 3 && x.prob <= 7 && x.imp > 3 && x.imp < 7) ||
        (x.prob > 3 && x.prob <= 7 && x.imp > 6 && x.imp <= 10)
      );
    });
  }

  get promContingencias() {
    return this.obtenerPromedio(this.Contingencias);
  }

  get hasReducir() {
    return this.Reducir.length > 0;
  }

  get Reducir() {
    return this.resultados.filter(x => {
      return x.prob > 7 && x.imp > 0 && x.imp < 4;
    });
  }

  get promReducir() {
    return this.obtenerPromedio(this.Reducir);
  }

  get hasAceptables() {
    return this.Aceptables.length > 0;
  }

  get Aceptables() {
    return this.resultados.filter(x => {
      return x.prob < 4 && x.imp < 4;
    });
  }

  get promAceptables() {
    return this.obtenerPromedio(this.Aceptables);
  }

  get hasMonitoreos() {
    return this.Monitoreos.length > 0;
  }

  get Monitoreos() {
    return this.resultados.filter(x => {
      return (
        (x.prob > 3 && x.prob <= 7 && x.imp <= 3) ||
        (x.prob <= 3 && x.imp > 3 && x.imp <= 6) ||
        (x.prob <= 3 && x.imp > 6)
      );
    });
  }

  get promMonitoreos() {
    return this.obtenerPromedio(this.Monitoreos);
  }


  guardarRespuesta = (riesgo: Riesgo) => {
    riesgo.completado = true;
    this.store.dispatch(new ActualizarRiesgo(riesgo));
    this.actions
      .pipe(ofActionSuccessful(ActualizarRiesgo))
      .subscribe(() => {
        this.snack.open('Riesgo actualizado', 'Ok', { duration: 2500 });
      });
    this.actions
      .pipe(ofActionErrored(ActualizarRiesgo))
      .subscribe(() => this.snack.open('Error al actualizar riesgo, intentelo de nuevo', 'Ok'));
  }
}
