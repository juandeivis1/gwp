import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { switchMap } from 'rxjs/operators';
import { combineLatest, Observable, of } from 'rxjs';
import { SincronizarProyectos } from 'src/app/states/proyectos';
import { Navigate } from '@ngxs/router-plugin';
import { AuthState } from 'src/app/states/auth.state';
import { faPlusSquare } from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { NuevoProyectoComponent } from '../nuevo-proyecto/nuevo-proyecto.component';

@Component({
  selector: 'app-portafolios',
  templateUrl: './portafolios.component.html',
  styleUrls: ['./portafolios.component.css']
})
export class PortafoliosComponent implements OnInit {

  public mas = faPlusSquare;
  portafolio$: Observable<{ portafolio: string, cantidad: number, avance: number }[]>

  constructor(
    private store: Store,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    const token = this.store.selectSnapshot(AuthState.token);
    this.store.dispatch(new SincronizarProyectos(token));
    this.calcular();
  }

  calcular = () => {
    let proyectos = this.store.select(state => state.proyectos);
    let portafolios = this.store.select(state => state.parametros.portafolios);
    this.portafolio$ = combineLatest(proyectos, portafolios)
      .pipe(switchMap(([_proyectos, _portafolios]) => {
        _portafolios = _portafolios.map((portafolio) => {
          return { portafolio: portafolio.name, cantidad: 0, avance: 0 };
        })

        if (_portafolios.length > 0) {
          _portafolios = _proyectos.reduce((portafolios, proyecto) => {
            let indice = portafolios.findIndex(portafolio => portafolio.portafolio == proyecto.portafolio);

            if (proyecto.avance) {
              proyecto.avance.porcentaje ? portafolios[indice].avance += proyecto.avance.porcentaje : null;
            }
            portafolios[indice].cantidad += 1;
            return portafolios;

          }, _portafolios);

          _portafolios.map(portafolio => {
            if (portafolio.cantidad > 0) {
              portafolio.avance = portafolio.avance / portafolio.cantidad;
            }
            return portafolio;
          });

          return of(_portafolios);
        } else {
          return of(null);
        }
      }))
  }

  irPortafolio = (portafolio: string) => {
    this.store.dispatch(new Navigate(['/gestion/portafolio/', portafolio]));
  }

  isEditor = () => {
    const permisos: string[] = this.store.selectSnapshot(AuthState.permisos);
    return permisos.includes("edit");
  };

  mostrar = () => {
    this.dialog.open(NuevoProyectoComponent, { height: '710px', width: '600px', disableClose: true, data: { portafolio: null } });
  }

}
