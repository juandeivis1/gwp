import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import {
  Store,
  Actions
} from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { Riesgo } from '../../interfaces/Riesgo.interface';
import { faEdit, faTimes, faSave, faSyncAlt, faFileAlt } from '@fortawesome/free-solid-svg-icons';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Navigate } from '@ngxs/router-plugin';

@Component({
  selector: 'app-riesgos',
  templateUrl: './riesgos.component.html',
  styleUrls: ['./riesgos.component.css']
})
export class RiesgosComponent implements OnInit {
  @Output() emitPushIn: EventEmitter<boolean> = new EventEmitter();
  @Output() emitPushOut: EventEmitter<boolean> = new EventEmitter();

  @Input() proId: number;
  @Input('riesgos')
  set avance(riesgos: Riesgo[]) {
    this.riesgos$ = of(riesgos);
  }
  @Input() editor: boolean;

  public riesgos$: Observable<Riesgo[]>;
  public editando = false;
  public trabajando = false;
  public seccion = 'riesgos';

  // Iconos
  public editar = faEdit;
  public cancelar = faTimes;
  public guardar = faSave;
  public sync = faSyncAlt;
  public report = faFileAlt;

  constructor(
    private store: Store,
    private actions: Actions,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() { }

  switchPush = (event: boolean) => {
    this.emitPushIn.emit(event);
  }

  switchPushOut(show: boolean) {
    this.emitPushOut.emit(show);
  }

  editarDatos = () => {
    this.editando = true;
  }

  reporte = () => {
    this.store.dispatch(new Navigate(['/admin/plan', this.proId]));
  }

  cancelarInformacion = () => {
    this.editando = false;
  }

  setSeccion = seccion => {
    this.seccion = seccion;
  }
}
