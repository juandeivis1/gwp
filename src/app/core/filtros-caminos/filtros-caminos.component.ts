import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Store } from '@ngxs/store';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Parametros, Distrito, Ciudad, Comunidad, Barrio } from 'src/app/interfaces/Parametros.interface';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthState } from 'src/app/states/auth.state';

@Component({
  selector: 'app-filtros-caminos',
  templateUrl: './filtros-caminos.component.html',
  styleUrls: ['./filtros-caminos.component.css']
})
export class FiltrosCaminosComponent implements OnInit {
  parametros: Parametros;
  districts: Distrito[];
  cities: Ciudad[];
  communities: Comunidad[];
  neighborhoods: Barrio[];
  filtros: FormGroup;
  consultando = false;

  constructor(
    private api: ApiService,
    private store: Store,
    private fb: FormBuilder,
    private dialog: MatDialogRef<FiltrosCaminosComponent>
  ) { }

  ngOnInit() {
    this.districts = this.store.selectSnapshot(state =>
      state.parametros
    ).distritos;

    this.filtros = this.fb.group({
      distrito: [[]],
      caminoId: [null]
    });
  }

  buscar = () => {
    this.consultando = true;
    this.api.buscarCamino(this.filtros.value).subscribe((res: string) => {
      const token = this.store.selectSnapshot(AuthState.token);
      const caminosDecrypted: any[] = this.api.decrypt(res, token);
      const procesado = caminosDecrypted.map(camino => {
        if (camino.coordenadas) {
          const _camino = JSON.parse(camino.coordenadas);
          if (_camino) {
            camino.coordenadas = _camino.map(x => Object.assign({ lat: x[1], lng: x[0] }));
          }
        }
        return camino;
      });
      this.dialog.close(procesado);
    }, err => {
      this.consultando = true;
    });
  }



}
