import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltrosCaminosComponent } from './filtros-caminos.component';

describe('FiltrosCaminosComponent', () => {
  let component: FiltrosCaminosComponent;
  let fixture: ComponentFixture<FiltrosCaminosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltrosCaminosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltrosCaminosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
