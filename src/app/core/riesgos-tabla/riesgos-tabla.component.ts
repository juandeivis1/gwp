import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Riesgo } from '../../interfaces/Riesgo.interface';
import { Actions, Store, ofActionSuccessful, ofActionErrored } from '@ngxs/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActualizarRiesgo } from 'src/app/states/proyectos';

@Component({
  selector: 'app-riesgos-tabla',
  templateUrl: './riesgos-tabla.component.html',
  styleUrls: ['./riesgos-tabla.component.css']
})
export class RiesgosTablaComponent implements OnInit {
  public probabilidades: number[];
  public impactos: number[];
  risk: any[][] = [];

  @Input('riegosProyecto')
  set riesgos(riegosProyecto: Riesgo[]) {
    this.riegosProyecto = riegosProyecto;
    this.probabilidades = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.impactos = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.probabilidades.map(prob => {
      this.risk[prob] = [];
      this.impactos.map(imp => {
        this.risk[prob][imp] = [];
      });
    });
    this.riegosProyecto.map((_riesgo, index) => {
      _riesgo['indice'] = index;
      if (!!_riesgo.prob && !!_riesgo.imp) {
        this.risk[_riesgo.prob][_riesgo.imp] = [...this.risk[_riesgo.prob][_riesgo.imp], _riesgo];
      }
    });

  }
  @Input() drag = true;
  public riegosProyecto: Riesgo[];

  @Output() public controls: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private store: Store,
    private snack: MatSnackBar,
    private actions: Actions,
  ) { }

  ngOnInit() { }

  public agregar({ evento, prob, imp }): void {
    if (!!evento.dragData.riesgo.prob && !!evento.dragData.riesgo.imp) {
      this.risk[evento.dragData.riesgo.prob][evento.dragData.riesgo.imp]
      = [...this.risk[evento.dragData.riesgo.prob][evento.dragData.riesgo.prob]
      .filter(risk => risk.riesgoId !== evento.dragData.riesgo.riesgoId)];
    }
    evento.dragData.riesgo.prob = prob;
    evento.dragData.riesgo.imp = imp;
    this.risk[prob][imp] = [...this.risk[prob][imp], evento.dragData.riesgo];
    this.store.dispatch(new ActualizarRiesgo(evento.dragData.riesgo));
    this.actions
      .pipe(ofActionSuccessful(ActualizarRiesgo))
      .subscribe(() => {
        this.snack.open('Riesgo actualizado', 'Ok', { duration: 2500 });
        this.definirIndice();
      });
    this.actions
      .pipe(ofActionErrored(ActualizarRiesgo))
      .subscribe(() => this.snack.open('Error al actualizar riesgo, intentelo de nuevo', 'Ok'));
  }

  definirIndice = () => {
    this.riegosProyecto.map((riesgo, index) => riesgo['indice'] = index);
  }

  getRiesgos = (prob, imp): any[] => {
    return this.riegosProyecto.filter(res => res.prob === prob && res.imp === imp);
  }

  has = (prob, imp): boolean => {
    const salida = this.riegosProyecto.filter(res => {
      return res.prob === prob && res.imp === imp;
    });
    return salida.length !== 0;
  }

  completos = () => {
    const completos = this.riegosProyecto.filter(x => {
      return !('indice' in x);
    });
    if (completos.length === 0) {
      return false;
    } else {
      return true;
    }
  }

  public showControls = show => {
    this.controls.emit(show);
  }

  existenRiesgos = () => {
    if (this.riegosProyecto.length === 0) {
      return false;
    } else {
      return true;
    }
  }
}
