import { Component, OnInit, Input } from "@angular/core";
import { faSyncAlt } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-encabezado",
  templateUrl: "./encabezado.component.html",
  styleUrls: ["./encabezado.component.css"]
})
export class EncabezadoComponent implements OnInit {
  @Input("header") head = "*Encabezado aqui*";
  @Input("loading") trabajando = false;

  public sync = faSyncAlt;

  constructor() {}

  ngOnInit() {}
}
