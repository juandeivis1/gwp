import { Component, OnInit, Input } from "@angular/core";
import {
  faSyncAlt,
  faEdit,
  faTimes,
  faSave,
  faPlusCircle,
  faReplyAll,
  faCheck
} from "@fortawesome/free-solid-svg-icons";
import { Observable, of } from "rxjs";
import {
  Store,
  Actions,
  ofActionSuccessful,
  ofActionErrored
} from "@ngxs/store";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Adquisicion } from '../../interfaces/Adquisicion.interface';
import { DatePipe } from "@angular/common";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import * as _ from 'lodash';
import { NuevaAdquisicion } from "../../states/proyectos";

@Component({
  selector: "app-adquisicion",
  templateUrl: "./adquisicion.component.html",
  styleUrls: ["./adquisicion.component.css"]
})
export class AdquisicionComponent implements OnInit {
  @Input("proId")proId: number;

  @Input("adquisicion")
  set adquisicion(adquisicion: Adquisicion) {
    this.adquisicion$ = _.isEmpty(adquisicion) ? of(null) : of(adquisicion)
  }
  @Input("editor") editor: boolean;

  private 

  public adquisicion$: Observable<Adquisicion>;
  public trabajando = false;

  // Iconos
  public editar = faEdit;
  public cancelar = faTimes;
  public guardar = faSave;
  public mas = faPlusCircle;
  public todas = faReplyAll;
  public sync = faSyncAlt;
  public listo = faCheck;

  public editando: boolean = false;
  public adquisiciones: FormGroup;

  readonly config = {
    dateInputFormat: "DD/MM/YYYY",
    containerClass: "theme-red",
    locale: "es-us"
  };

  constructor(
    private store: Store,
    private actions: Actions,
    private dp: DatePipe,
    private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {
    actions.pipe(ofActionSuccessful(NuevaAdquisicion)).subscribe(res => {
      this.trabajando = false;
    });
    actions.pipe(ofActionErrored(NuevaAdquisicion)).subscribe(res => {
      this.trabajando = false;
      this.snackBar.open("Error de comunicación con MPZ...", "Ok");
    });
  }

  ngOnInit() { }

  generarFecha = (ts) => {
    return !!ts ? new Date(ts) : null;
  };

  definirFecha = ({value}) => {
    this.adquisiciones.patchValue({
      refrendo: value.getTime()
    })
  };

  guardarAdquisicion = () => {
    this.trabajando = true;
    this.store.dispatch(new NuevaAdquisicion( this.proId, this.adquisiciones.value ));
    this.actions.pipe(ofActionSuccessful(NuevaAdquisicion)).subscribe(res => {
      this.trabajando = false;
      this.snackBar.open("Adquisiciones correctas","Ok",{ duration: 2500 });
      this.cancelarAdquisicion();
    });
  };

  cancelarAdquisicion = () => {
    this.editando = false;
    this.adquisiciones.reset()
  };

  editarDatos = () => {
    this.editando = true;
    this.adquisicion$.subscribe(
      adquisicion => (adquisicion ? this.cargarDatos(adquisicion) : this.cargarDatosNuevos())
    );
  };

  private cargarDatos = (Adquisicion: Adquisicion) => {
    this.adquisiciones = this.fb.group({
      abs: [Adquisicion.abs || null, [Validators.pattern("^[^'\"]*$")]],
      mopt: [Adquisicion.mopt || null, [Validators.pattern("^[^'\"]*$")]],
      cne: [Adquisicion.cne || null, [Validators.pattern("^[^'\"]*$")]],
      ordenCompra: [Adquisicion.ordenCompra || null, [Validators.pattern("^[^'\"]*$")]],
      otras: [Adquisicion.otras || null, [Validators.pattern("^[^'\"]*$")]],
      contratoCalidad: [Adquisicion.contratoCalidad || null, [Validators.pattern("^[^'\"]*$")]],
      ordenCalidad: [Adquisicion.ordenCalidad || null, [Validators.pattern("^[^'\"]*$")]],
      contratacion: [Adquisicion.contratacion || null, [Validators.pattern("^[^'\"]*$")]],
      refrendo: [Adquisicion.refrendo || null],
    });
  };

  private cargarDatosNuevos = () => {
    this.adquisiciones = this.fb.group({
      abs: [null, [Validators.pattern("^[^'\"]*$")]],
      mopt: [null, [Validators.pattern("^[^'\"]*$")]],
      cne: [null, [Validators.pattern("^[^'\"]*$")]],
      ordenCompra: [null, [Validators.pattern("^[^'\"]*$")]],
      otras: [null, [Validators.pattern("^[^'\"]*$")]],
      contratoCalidad: [null, [Validators.pattern("^[^'\"]*$")]],
      ordenCalidad: [null, [Validators.pattern("^[^'\"]*$")]],
      contratacion: [null, [Validators.pattern("^[^'\"]*$")]],
      refrendo: [null],
    });
  };
}
