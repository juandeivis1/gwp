import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../services/api.service";
import { Router } from "@angular/router";
import {
  Store,
  Actions,
  ofActionSuccessful,
  ofActionErrored
} from "@ngxs/store";
import { AuthState, Login } from "../../states/auth.state";
import { Navigate } from "@ngxs/router-plugin";
import { MatSnackBar } from "@angular/material/snack-bar";
import { faSpinner, faSignInAlt } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  public spinner = faSpinner;
  public consultando: boolean = false;
  public loginIcon = faSignInAlt;

  constructor(
    private store: Store,
    private actions: Actions
  ) {
    const token = this.store.selectSnapshot(AuthState.token);
    if (token !== undefined) {
      this.store.dispatch(new Navigate(["/gestion/portafolios"]));
    }
  }

  ngOnInit() {}

  login = (user: string, password: string) => {
    this.consultando = true;
    this.store
      .dispatch(new Login({ user: user, password: password }))

    this.actions.pipe(ofActionSuccessful(Login)).subscribe(res => {
      this.consultando = false;
    });
    this.actions.pipe(ofActionErrored(Login)).subscribe(res => {
      this.consultando = false;
    });

  };
}
