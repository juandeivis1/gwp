import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GestionComponent } from './gestion/gestion.component';
import { AdminMapaGuard } from '../services/adminMapa.guard';
import { PortafolioComponent } from './portafolio/portafolio.component';
import { GeojsonComponent } from './geojson/geojson.component';
import { InspeccionComponent } from './inspeccion/inspeccion.component';
import { InspeccionesComponent } from './inspecciones/inspecciones.component';
import { PortafoliosComponent } from './portafolios/portafolios.component';
import { VideosComponent } from './videos/videos.component';
import { MapaComponent } from './mapa/mapa.component';
import { ProyectosComponent } from './proyectos/proyectos.component';
import { ActualComponent } from './actual/actual.component';

const routes: Routes = [
  {
    path: '', component: GestionComponent, children:
      [
        { path: 'proyectos/actual/:proId', component: ActualComponent },
        { path: 'proyectos/actual/:proId/:info', component: ActualComponent },
        { path: 'proyectos', component: ProyectosComponent },
        { path: 'mapa', component: MapaComponent },
        { path: 'mapa/:proId', component: MapaComponent },
        { path: 'videos', component: VideosComponent },
        { path: 'portafolios', component: PortafoliosComponent },
        { path: 'inspecciones', component: InspeccionesComponent },
        { path: 'inspeccion', component: InspeccionComponent },
        { path: 'portafolio/:portafolio', component: PortafolioComponent },
        { path: 'geojson', component: GeojsonComponent, canActivate: [AdminMapaGuard] },
        { path: '**', redirectTo: '/gestion/portafolios', pathMatch: 'full' }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
