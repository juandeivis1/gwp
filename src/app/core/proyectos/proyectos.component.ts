import { Component, OnInit } from "@angular/core";
import { faPlusSquare } from "@fortawesome/free-regular-svg-icons";
import {
  Store,
  Select
} from "@ngxs/store";
import { Observable } from "rxjs/internal/Observable";
import { Navigate } from "@ngxs/router-plugin";
import { AuthState } from "../../states/auth.state";
import { SincronizarProyectos, Proyectos } from "../../states/proyectos";
import { Proyecto } from "../../interfaces/Proyecto";

@Component({
  selector: "app-proyectos",
  templateUrl: "./proyectos.component.html",
  styleUrls: ["./proyectos.component.css"]
})
export class ProyectosComponent implements OnInit {
  public mas = faPlusSquare;
  public mostrarNuevo: boolean = false;

  @Select(Proyectos.todos) proyectos$: Observable<Proyecto[]>;

  constructor(
    private store: Store
  ) {}

  ngOnInit() {
    const token = this.store.selectSnapshot(AuthState.token);
    this.store.dispatch(new SincronizarProyectos(token))
  }

  mostrar = () => {
    this.mostrarNuevo = true;
  };

  noMostrar = () => {
    this.mostrarNuevo = false;
  };

  irProyecto = id => {
    this.store.dispatch(new Navigate(["/gestion/proyectos/actual/", id]));
  };

  isEditor = () => {
    const permisos: string[] = this.store.selectSnapshot(AuthState.permisos);
    return permisos.includes("edit");
  };
}
