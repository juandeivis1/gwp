import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, AbstractControl } from '@angular/forms';
import { Store, Actions, ofActionSuccessful, ofActionErrored } from '@ngxs/store';
import { AuthState } from 'src/app/states/auth.state';
import { ParametrosState } from 'src/app/states/parametros.state';
import { MatSnackBar } from '@angular/material/snack-bar';
import { InspeccionCamino } from 'src/app/states/interno.state';

@Component({
  selector: 'app-inspeccion',
  templateUrl: './inspeccion.component.html',
  styleUrls: ['./inspeccion.component.css']
})

export class InspeccionComponent implements OnInit {

  @ViewChild('inside',{static:false}) elementView: ElementRef;
  inspeccion: FormGroup;
  basico: FormGroup;
  accionado: FormGroup;
  finalizacion: FormGroup;
  revisado: FormGroup;

  tipos: { tipo: string, valor: number }[] = [
    { tipo: 'Conformación', valor: 1 },
    { tipo: 'Bacheo', valor: 2 },
    { tipo: 'Paso Alcantarillas', valor: 3 },
    { tipo: 'Puente', valor: 4 },
    { tipo: 'Cementados ', valor: 5 },
    { tipo: 'Asfaltos ', valor: 6 },
    { tipo: 'Recarpeteo', valor: 7 },
    { tipo: 'Ampliaciones ', valor: 8 },
    { tipo: 'Talud', valor: 9 },
    { tipo: 'Cabezal', valor: 10 },
    { tipo: 'Gaviones', valor: 11 },
    { tipo: 'Cunetas', valor: 12 },
    { tipo: 'Aceras', valor: 13 },
    { tipo: 'Barandas de protección', valor: 14 },
    { tipo: 'Pintura Puente', valor: 15 },
    { tipo: 'Cordon y caño', valor: 16 }
  ];

  distritos: { tipo: string, valor: number }[] = [
    { tipo: 'San Isidro', valor: 1 },
    { tipo: 'General', valor: 2 },
    { tipo: 'Daniel Flores', valor: 3 },
    { tipo: 'Rivas', valor: 4 },
    { tipo: 'San Pedro', valor: 5 },
    { tipo: 'Platanares', valor: 6 },
    { tipo: 'Pejibaye', valor: 7 },
    { tipo: 'Cajón', valor: 8 },
    { tipo: 'Barú', valor: 9 },
    { tipo: 'Río Nuevo', valor: 10 },
    { tipo: 'Páramo', valor: 11 },
    { tipo: 'La Amistad', valor: 12 }
  ];

  prioridades = [
    { tipo: 'Regular', valor: 3 },
    { tipo: 'Medio', valor: 2 },
    { tipo: 'Urgente', valor: 1 }
  ];

  equipo: string[]

  usuarioActual: string

  year

  constructor(
    private fb: FormBuilder,
    private store: Store,
    private snack: MatSnackBar,
    private actions: Actions
  ) { }

  ngOnInit() {

    this.usuarioActual = this.store.selectSnapshot(AuthState.user);
    this.equipo = this.store.selectSnapshot(ParametrosState.equipo);

    this.basico = this.fb.group({
      id: [null],
      tipo: [null, [Validators.required]],
      prioridad: [null, [Validators.required]],
      distrito: [null, [Validators.required]],
      camino: [null, [Validators.required]],
      asignado: [null, [Validators.required]],
    });

    this.inspeccion = this.fb.group({
      creador: [this.usuarioActual],
      creacion: [Date.now()],
      realizado: [null],
      inspector: [null],
      direccion: [null, [Validators.required]],
      objetivo: [null, [Validators.required]],
      detalle: this.fb.array([], this.validateLength.bind(this)),
      recomendacion: this.fb.array([], this.validateLength.bind(this))
    });

    this.accionado = this.fb.group({
      accionado: [null, [Validators.minLength(10)]],
      telefono: [null, [Validators.minLength(8)]],
      email: [null, [Validators.email]],
    });

    this.year = new Date().getUTCFullYear().toString().slice(-2)

    // this.revisado = this.fb.group({
    //   revisado: [null, [Validators.required]],
    //   aprueba: [null, [Validators.required]],
    // })

  }

  validateLength(control: AbstractControl) {
    return control.value.length > 0 ? null : { length: true };
  }

  getPrioridad = (valor) => valor && this.prioridades.find(prioridad => prioridad.valor == valor).tipo
  getDistrito = (valor) => valor && this.distritos.find(distrito => distrito.valor == valor).tipo
  getTipo = (valor) => valor && this.tipos.find(_tipo => _tipo.valor == valor).tipo

  get detalles() {
    return this.inspeccion.get('detalle') as FormArray;
  }

  get recomendaciones() {
    return this.inspeccion.get('recomendacion') as FormArray;
  }

  agregarDetalle = () => {
    this.detalles.push(
      this.fb.group({
        detalle: ['', [Validators.required, Validators.minLength(20)]],
        imgs: this.fb.array([])
      })
    )
  }

  agregarRecomendacion = () => {
    this.recomendaciones.push(
      this.fb.group({
        recomendacion: ['', [Validators.required, Validators.minLength(20)]],
        imgs: this.fb.array([])
      })
    )
  }

  quitarDetalle = (indice: number) => {
    this.detalles.removeAt(indice);
  }

  quitarRecomendacion = (indice: number) => {
    this.recomendaciones.removeAt(indice);
  }

  finalizar = () => {
    const basico = this.basico.value;
    const inspeccion = this.inspeccion.value;
    const accionado = this.accionado.value;
    inspeccion.inspector = this.usuarioActual;
    
    inspeccion.detalle.map(detalle => {
      delete detalle.imgs
    })
    inspeccion.recomendacion.map(detalle => {
      delete detalle.imgs
    })
    const finalizar = { ...basico, ...inspeccion, ...accionado }
    this.store.dispatch(new InspeccionCamino(finalizar))
    this.actions.pipe(ofActionSuccessful(InspeccionCamino))
      .subscribe(res => {
        console.log(res);
      });
    this.actions.pipe(ofActionErrored(InspeccionCamino))
      .subscribe(res => {
        console.log(res);
      });
  }

  imagenDetalle = (files: FileList, indice) => {
    const file = files.item(0)
    if (!file || file.type.split('/')[0] !== 'image') {
      this.snack.open('Archivo no es imagen', 'Ok', {
        duration: 2000
      })
      return;
    }
    if (FileReader && files && files.length) {
      const fr = new FileReader();
      fr.onload = () => {
        const imgs = this.detalles.controls[indice].get('imgs') as FormArray
        imgs.push(this.fb.control(fr.result))
      }
      fr.readAsDataURL(files[0]);
    }
  }

  imagenRecomendacion = (files: FileList, indice) => {
    const file = files.item(0)
    if (!file || file.type.split('/')[0] !== 'image') {
      this.snack.open('Archivo no es imagen', 'Ok', {
        duration: 2000
      })
      return;
    }
    if (FileReader && files && files.length) {
      const fr = new FileReader();
      fr.onload = () => {
        const imgs = this.recomendaciones.controls[indice].get('imgs') as FormArray
        imgs.push(this.fb.control(fr.result))
      }
      fr.readAsDataURL(files[0]);
    }
  }

  removeImgRecomendacion = (recomendacion, img) => {
    const tempImgs = this.recomendaciones.controls[recomendacion].get('imgs') as FormArray;
    tempImgs.removeAt(img)
  }

  removeImgDetalle = (recomendacion, img) => {
    const tempImgs = this.detalles.controls[recomendacion].get('imgs') as FormArray;
    tempImgs.removeAt(img)
  }




  // xPos = 0

  // onPan = ({center, deltaX}) => {
  //   center.x + 88 >= this.elementView.nativeElement.offsetWidth ? console.log('pagar') : null
  //   center.x + 30  <= this.elementView.nativeElement.offsetWidth ? this.xPos = center.x - 60 : null
  // }

  // end = ( e ) => {
  //   console.log(e);

  // }

}
