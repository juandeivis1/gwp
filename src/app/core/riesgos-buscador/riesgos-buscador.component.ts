import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Riesgo } from '../../interfaces/Riesgo.interface';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  Store,
  Actions,
  ofActionSuccessful,
  ofActionErrored
} from '@ngxs/store';
import { InsertarRiesgo, ObtenerRiesgos } from '../../states/interno.state';
import { Observable } from 'rxjs/internal/Observable';
import { tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthState } from 'src/app/states/auth.state';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { combineLatest } from 'rxjs';
import { AgregarRiesgo } from 'src/app/states/proyectos';

@Component({
  selector: 'app-riesgos-buscador',
  templateUrl: './riesgos-buscador.component.html',
  styleUrls: ['./riesgos-buscador.component.css']
})
export class RiesgosBuscadorComponent implements OnInit {
  @Output() emitPushIn: EventEmitter<boolean> = new EventEmitter();
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @Input() riesgo$: Riesgo[];
  @Input() proId: number;


  dataSource: MatTableDataSource<Riesgo>;

  public formNuevoRiesgo: FormGroup;
  public riesgos: Observable<Array<Riesgo>>;
  public riesgosBD: Riesgo[] = [];
  public subCategorias: any[] = [];

  public page = 1;
  public itemsPerPage = 10;
  public maxSize = 5;
  public numPages = 1;
  displayedColumns: string[] = ['riesgoId', 'fase', 'categoria', 'subcategoria', 'causa', 'consecuencia', 'actividad'];
  public listSubcategorias = [
    {
      idCat: 'Técnico',
      subCategorias: [
        { id: 1, nom: 'Requisitos' },
        { id: 2, nom: 'Tecnología' },
        { id: 3, nom: 'Complejidad e interfaces' },
        { id: 4, nom: 'Desempeño y fiabilidad' },
        { id: 5, nom: 'Calidad' }
      ]
    },
    {
      idCat: 'Externo',
      subCategorias: [
        { id: 1, nom: 'Subcontratistas y proveedores' },
        { id: 2, nom: 'Normativa' },
        { id: 3, nom: 'Mercado' },
        { id: 4, nom: 'Cliente' },
        { id: 5, nom: 'Clima' }
      ]
    },
    {
      idCat: 'Organizacional',
      subCategorias: [
        { id: 1, nom: 'Dependencias del proyecto' },
        { id: 2, nom: 'Recursos' },
        { id: 3, nom: 'Financiamiento' },
        { id: 4, nom: 'Priorización' }
      ]
    },
    {
      idCat: 'Dirección de proyectos',
      subCategorias: [
        { id: 1, nom: 'Estimación' },
        { id: 2, nom: 'Planificación' },
        { id: 3, nom: 'Control' },
        { id: 4, nom: 'Comunicación' }
      ]
    },
    {
      idCat: 'Operativo',
      subCategorias: [
        { id: 1, nom: 'Insumos' },
        { id: 2, nom: 'Equipos' },
        { id: 3, nom: 'Mano de obra' },
        { id: 4, nom: 'Informes' }
      ]
    }
  ];

  public categorias = [
    { id: 1, nom: 'Técnico' },
    { id: 2, nom: 'Externo' },
    { id: 3, nom: 'Organizacional' },
    { id: 4, nom: 'Dirección de proyectos' },
    { id: 5, nom: 'Operativo' }
  ];

  public fases = [
    { id: 1, nom: 'Preinversión' },
    { id: 2, nom: 'Promoción, Negociación y Financiamiento' },
    { id: 3, nom: 'Diseños finales' },
    { id: 4, nom: 'Ejecución' },
    { id: 5, nom: 'Operación' }
  ];

  public guardando = false;

  public constructor(
    private fb: FormBuilder,
    private store: Store,
    private actions: Actions,
    private snackBar: MatSnackBar
  ) {
    const token = this.store.selectSnapshot(AuthState.token);
    this.store.dispatch(new ObtenerRiesgos(token));
    const riesgosStore = this.store.select(state => state.interno.riesgos);

    this.riesgos = riesgosStore.pipe(
      tap((riesgos: Riesgo[]) => {
        this.dataSource = new MatTableDataSource(riesgos);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.riesgosBD = riesgos;
      })
    );
  }

  agregar = (newRiesgo) => {
    const nuevoRisgo = Object.assign({}, newRiesgo);
    const riesgo = this.riesgo$.find(
      _riesgo => _riesgo.idRiesgo === nuevoRisgo.riesgoId
    );
    riesgo === undefined
      ? this.agregarRiesgoProyecto(nuevoRisgo)
      : this.snackBar.open('Riesgo ya existe en el proyecto', 'Ok');
  }

  agregarRiesgoProyecto = (riesgo) => {
    this.store.dispatch(new AgregarRiesgo(this.proId, riesgo));

    this.actions.pipe(ofActionSuccessful(AgregarRiesgo))
      .subscribe(() => {
        this.snackBar.open('Riesgo agregado con exito', 'Ok', { duration: 3000 });
      });
    this.actions.pipe(ofActionErrored(AgregarRiesgo))
      .subscribe(() => {
        this.snackBar.open('Error agregando riesgo, intentelo de nuevo', 'Ok');
      });
  }

  public ngOnInit(): void {
  }

  get fase() {
    return this.formNuevoRiesgo.get('fase');
  }

  get categoria() {
    return this.formNuevoRiesgo.get('categoria');
  }

  get subcategoria() {
    return this.formNuevoRiesgo.get('subcategoria');
  }

  get causa() {
    return this.formNuevoRiesgo.get('causa');
  }

  get consecuencia() {
    return this.formNuevoRiesgo.get('consecuencia');
  }

  get actividad() {
    return this.formNuevoRiesgo.get('actividad');
  }

  setSubcategorias = (categoriaId) => {
    this.subCategorias = this.listSubcategorias.find(
      subcategoria => subcategoria.idCat === categoriaId
    ).subCategorias;

    const temp = this.subCategorias.map(x => {
      return x.nom;
    });

    if (!temp.includes(this.subcategoria.value)) {
      this.subcategoria.setValue(this.subCategorias[0].nom);
    }
  }

  public changePage(page: any, data: Array<any>): Array<any> {
    const start = (page.page - 1) * page.itemsPerPage;
    const end = page.itemsPerPage > -1 ? start + page.itemsPerPage : data.length;
    return data.slice(start, end);
  }

  copiar = (riesgo): void => {
    const { categoria } = riesgo;
    this.nuevoRiesgo(riesgo);
    this.setSubcategorias(categoria);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  nuevoRiesgo = (riesgo?: Riesgo) => {
    !riesgo
      ? (this.formNuevoRiesgo = this.fb.group({
        fase: [null, Validators.required],
        categoria: [null, Validators.required],
        subcategoria: [null, Validators.required],
        causa: [
          null,
          Validators.compose([Validators.required, Validators.minLength(5)])
        ],
        consecuencia: [
          null,
          Validators.compose([Validators.required, Validators.minLength(5)])
        ],
        actividad: [
          null,
          Validators.compose([Validators.required, Validators.minLength(5)])
        ]
      }))
      : (this.formNuevoRiesgo = this.fb.group({
        fase: [riesgo.fase, Validators.required],
        categoria: [riesgo.categoria, Validators.required],
        subcategoria: [riesgo.subcategoria, Validators.required],
        causa: [
          riesgo.causa,
          Validators.compose([Validators.required, Validators.minLength(5)])
        ],
        consecuencia: [
          riesgo.consecuencia,
          Validators.compose([Validators.required, Validators.minLength(5)])
        ],
        actividad: [
          riesgo.actividad,
          Validators.compose([Validators.required, Validators.minLength(5)])
        ]
      }));
  }

  existeRiesgo = (NuevoRiesgo) => {
    let existe = false;
    for (const riesgo of this.riesgosBD) {
      for (const campo in NuevoRiesgo) {
        if (NuevoRiesgo[campo].trim() === riesgo[campo]) {
          existe = true;
        } else {
          existe = false;
          break;
        }
      }
      if (existe) {
        return existe;
      }
    }
    return existe;
  }


  insertaRiesgo = (): void => {
    const tempRiesgo: Riesgo = this.formNuevoRiesgo.value;
    if (!this.existeRiesgo(tempRiesgo)) {
      this.store.dispatch(new InsertarRiesgo(tempRiesgo));
      this.actions.pipe(ofActionSuccessful(InsertarRiesgo)).subscribe(() => {
        this.cancelar();
        this.snackBar.open('Se ha agrego de manera correcta el riesgo', 'Ok', { duration: 2000 });
      });
      this.actions.pipe(ofActionErrored(InsertarRiesgo)).subscribe(() => {
        this.snackBar.open('Ocurrio un error al ingresar el riesgo', 'Ok');
      });
    } else {
      this.snackBar.open('El riesgo ya existe en la lista', 'Ok');
    }
  }

  cancelar() {
    this.formNuevoRiesgo = null;
  }

  borrar() {
    this.formNuevoRiesgo.reset();
  }

  setPushIn(show: boolean) {
    this.emitPushIn.emit(show);
  }

  push = (pushIt) => {
    this.emitPushIn.emit(pushIt);
  }
}
