import { Component, OnInit, Input } from "@angular/core";
import { faEdit, faSave } from "@fortawesome/free-regular-svg-icons";
import {
  faTimes,
  faPlusCircle,
  faReplyAll,
  faSyncAlt,
  faCheck
} from "@fortawesome/free-solid-svg-icons";
import { Observable, of } from "rxjs";
import { Calidad } from "../../interfaces/Calidad.interface";
import {
  Store,
  ofActionSuccessful,
  Actions,
  ofActionErrored
} from "@ngxs/store";
import { MatSnackBar } from "@angular/material/snack-bar";
import * as _ from 'lodash';
import { NuevaCalidad } from "../../states/proyectos";

@Component({
  selector: "app-calidad",
  templateUrl: "./calidad.component.html",
  styleUrls: ["./calidad.component.css"]
})
export class CalidadComponent implements OnInit {
  @Input("proId") proId: number;

  @Input("calidad") 
  set calidad(calidad: Calidad) {
    this.calidad$ = _.isEmpty(calidad) ? of(null) : of(calidad)
  }

  @Input("editor") editor: boolean;

  private 

  public calidad$: Observable<Calidad>;

  public editar = faEdit;
  public cancelar = faTimes;
  public guardar = faSave;
  public mas = faPlusCircle;
  public todas = faReplyAll;
  public sync = faSyncAlt;
  public listo = faCheck;
  public trabajando = false;

  public editando: boolean = false;
  public alerta = null;

  public newCalidad: Calidad = {};

  public estados = ["Pendiente", "Finalizado"];

  constructor(
    private store: Store,
    private actions: Actions,
    private snackBar: MatSnackBar
  ) {
    actions.pipe(ofActionSuccessful(NuevaCalidad)).subscribe(res => {
      this.trabajando = false;
    });
    actions.pipe(ofActionErrored(NuevaCalidad)).subscribe(res => {
      this.trabajando = false;
      this.snackBar.open("Ocurrio un error de comunicación con MPZ, intentelo de nuevo","Ok");
    });
  }
  ngOnInit() {}


  editarDatos = () => {
    this.editando = true;
    this.calidad$.subscribe(
      seguimiento => (seguimiento ? this.cargarDatos(seguimiento) : null)
    );
  };

  private cargarDatos = (calidad: Calidad) => {
    this.newCalidad = Object.assign({}, calidad);
  };

  cancelarCalidad = () => {
    this.editando = false;
    this.newCalidad = {};
  };

  guardarCalidad = () => {
    let nuevo = Object.assign({}, this.newCalidad);
    this.trabajando = true;
    this.store.dispatch(new NuevaCalidad(this.proId, nuevo));
    this.actions.pipe(ofActionSuccessful(NuevaCalidad)).subscribe(res => {
      this.trabajando = false;
      this.snackBar.open("Control de calidad correto :)","Ok",{ duration: 2500 });
      this.cancelarCalidad();
    });
  };
}
