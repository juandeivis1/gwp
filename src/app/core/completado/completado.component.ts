import { OnInit, Component, Input } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Completar } from '../../interfaces/Seguimiento.interface';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store, ofActionSuccessful, Actions, ofActionErrored } from '@ngxs/store';
import { PuntoCompletado, AgregarCompletar } from 'src/app/states/proyectos';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-completado',
  templateUrl: './completado.component.html',
  styleUrls: ['./completado.component.css']
})

export class CompletadoComponent implements OnInit {
  @Input('lista')  puntosCompletar: Completar[];
  @Input('agregar') agregarMas: boolean = false;
  @Input("proId") proId: number;
  public mas = faPlusCircle;
  
  nuevoPunto: FormControl
  constructor(
    private fb: FormBuilder,
    private snack: MatSnackBar,
    private store: Store,
    private actions: Actions
  ) { }

  ngOnInit() {
    this.nuevoPunto = this.fb.control('', [Validators.required, Validators.minLength(10)])
  }

  marcar (completar: Completar) {
    this.store.dispatch(new PuntoCompletado(completar));
    this.actions.pipe(ofActionSuccessful(PuntoCompletado)).subscribe(() => {
      this.snack.open("Se ha completado con exito","Ok",{duration: 2000});
    });
    this.actions.pipe(ofActionErrored(PuntoCompletado)).subscribe(() => {
      this.snack.open("Ha fallado el complado del punto, intentelo de nuevo","Ok");
    });
  }
  
  agregar = () => {
    let nuevo = {punto: this.nuevoPunto.value, check: false, proId: this.proId }
    this.store.dispatch(new AgregarCompletar(nuevo));
    this.actions.pipe(ofActionSuccessful(AgregarCompletar)).subscribe(() => {
      this.nuevoPunto.reset()
      this.snack.open("Punto completado con exito","Ok",{duration: 2000});
    });
    this.actions.pipe(ofActionErrored(AgregarCompletar)).subscribe(() => {
      this.snack.open("Ha fallado la creación del punto, intentelo de nuevo","Ok");
    });
  }

}
