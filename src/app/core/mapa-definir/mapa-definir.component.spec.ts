import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapaDefinirComponent } from './mapa-definir.component';

describe('MapaDefinirComponent', () => {
  let component: MapaDefinirComponent;
  let fixture: ComponentFixture<MapaDefinirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapaDefinirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapaDefinirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
