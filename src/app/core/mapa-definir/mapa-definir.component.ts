import { Component, OnInit, Inject, Input, ChangeDetectorRef, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store, Actions, ofActionSuccessful, ofActionErrored } from '@ngxs/store';
import { Map, Marker } from 'mapbox-gl';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicioCoordenadas } from '../mapa/coordenadas';
import * as mapboxgl from 'mapbox-gl/dist/mapbox-gl.js';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { CaminosComponent } from '../caminos/caminos.component';
import { MediaMatcher } from '@angular/cdk/layout';
import { ApiService } from 'src/app/services/api.service';

declare var window;


@Component({
  selector: 'mapa-definir',
  templateUrl: './mapa-definir.component.html',
  styleUrls: ['./mapa-definir.component.css']
})
export class MapaDefinirComponent implements OnInit {
  @Input() lat: number = 9.37305209270302;
  @Input() lng: number = -83.70277600343857;
  @Input() strokeColor: string = "#0084bc";
  @Input() strokeWeight: number = 2;
  @Input() zoom: number = 16;
  @ViewChild('gmap',{static:false}) gmapElement: any;

  puntosLinea: any[] = [];
  private mapBox: Map;
  private markerProyecto;
  private caminos: any[] = [];
  multiple: boolean = false;
  height: number;
  mobileQuery: any;
  _mobileQueryListener: () => void;
  maps: any;
  map: any;
  road: any;
  markers: any[] = []

  constructor(
    public dialogRef: MatDialogRef<MapaDefinirComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private snackBar: MatSnackBar,
    private bottomSheet: MatBottomSheet,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private api: ApiService
  ) {
    this.height = 550;
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.maps = window.google.maps;
  }

  ngOnInit() {
    this.initMap();
  }

  initMap = () => {
    const latLng: google.maps.LatLng = new this.maps.LatLng(this.lat, this.lng);
    var mapProp = {
      center: latLng,
      zoom: 11,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new this.maps.Map(this.gmapElement.nativeElement, mapProp);
    var image = {
      url: 'https://perezzeledon.go.cr:803/assets/icons/gv-icon-512x512.png',
      scaledSize: new this.maps.Size(30, 30),

    };
    const marker = new this.maps.Marker({ position: { lat: this.lat, lng: this.lng }, map: this.map, title: 'Municipalidad de Pérez Zeledón', icon: image });
    this.polylineDistritos()

    this.map.addListener('click', ({ latLng }) => {
      if (this.multiple) {
        this.puntosLinea.push([latLng.lng(), latLng.lat()])
      } else {
        this.puntosLinea = [[latLng.lng(), latLng.lat()]]
      }
      this.actualizarProyecto(this.puntosLinea);
    });
  }


  actualizarProyecto = (puntos: any[]) => {
    if (this.markerProyecto) {
      this.markerProyecto.setMap(null);
    }
    if (this.road) {
      this.road.setMap(null);
    }
    if (puntos.length > 0) {
      this.markerProyecto = new google.maps.Marker({
        position: { lat: puntos[0][1], lng: puntos[0][0] },
        draggable: false,
        map: this.map
      })
      const coords = puntos.map(punto => {
        return { lat: punto[1], lng: punto[0] }
      })
      this.road = new this.maps.Polyline({
        path: coords,
        geodesic: true,
        strokeColor: '#0299ff',
        strokeOpacity: 1.0,
        strokeWeight: 2,
      });
      this.road.setMap(this.map);
    }
  }

  private polylineDistritos() {
    let listaPuntosDistritos: any[] = new ServicioCoordenadas().getCoordenadasDistrito();

    let listaColores: any[] = [
      "#996633",
      "#336600",
      "#0066ff",
      "#ff6600",
      "#3333ff",
      "#ff0000",
      "#990099",
      "#ffcc00",
      "#44357A",
      "#993366",
      "#1EB635",
      "#3399ff"
    ];
    let x: number = 0;
    for (let listaPuntos of listaPuntosDistritos) {
      let listaPuntosDistrito: any[] = listaPuntos.split(" ");
      let listaPuntosProcesada: any[] = [];

      for (let parPuntos of listaPuntosDistrito) {
        let temp = parPuntos.split(",");
        listaPuntosProcesada.push({
          lat: parseFloat(temp[1]),
          lng: parseFloat(temp[0])
        });
      }

      var polygonDistrito = new this.maps.Polyline({
        path: listaPuntosProcesada,
        strokeColor: listaColores[x],
        strokeOpacity: 0.4,
        strokeWeight: this.strokeWeight
      });
      polygonDistrito.setMap(this.map);
      x = x + 1;
    }
  }

  definirTipo = (multiple: boolean) => {
    this.multiple = multiple;
    if (!this.multiple && this.puntosLinea.length > 1) {
      this.puntosLinea = [this.puntosLinea[0]]
      this.actualizarProyecto(this.puntosLinea)
    }
  }

  quitarUltimo = () => {
    this.puntosLinea.pop()
    this.actualizarProyecto(this.puntosLinea)
  }


  cancelar = () => {
    this.dialogRef.close();
  }

  guardar = () => {
    if (this.puntosLinea) {
      this.dialogRef.close(this.puntosLinea);
    } else {
      this.cancelar();
    }
  }

  filter(value: string) {
    const codigo = value.toString().toLowerCase();
    this.api.buscarCamino({ caminoId: codigo }).subscribe((res: any[]) => {
      if (res.length > 0) {
        this.removeOldData();
        this.setDataRoads(res)
      } else {
        this.snackBar.open('No coincide con ningún codigo de camino', 'Ok', {
          duration: 3000
        })
      }
    })
  }

  removeOldData = () => {
    this.caminos.map(x => {
      x.setMap(null);
    });
    this.markers.map(x => {
      x.setMap(null);
    });
  }

  setDataRoads = (roads) => {
    roads.map(camino => {
      if (camino.coordenadas) {
        const roadCoords = JSON.parse(camino.coordenadas);
        const coords = roadCoords.map(punto => {
          return { lat: punto[1], lng: punto[0] }
        })
        camino.coordenadas = coords;
        const road = new this.maps.Polyline({
          path: coords,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2,
        });
        road.setMap(this.map);
        this.markerRoad(camino);
        this.caminos.push(road);
      }
    })
    if (roads.length == 1) {
      const latLng = new this.maps.LatLng({ lat: roads[0].coordenadas[0].lat, lng: roads[0].coordenadas[0].lng })
      this.map.setZoom(17);
      this.map.panTo(latLng);
    } else {
      this.map.setZoom(11);
    }
  }

  markerRoad = (road) => {
    const marker = new google.maps.Marker({
      position: road.coordenadas[0],
      map: this.map,
      title: road.codigo
    });
    marker.addListener('click', () => {
      this.bottomSheet.open(CaminosComponent, { data: road });

      this.map.setZoom(17);
      this.map.panTo(marker.getPosition());
    });
    this.markers.push(marker);
  }
}
