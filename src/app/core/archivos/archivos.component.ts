import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import { faCloudUploadAlt } from '@fortawesome/free-solid-svg-icons';
import { faFileImage, faFileAlt } from '@fortawesome/free-regular-svg-icons';
import { combineLatest, BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { tap, filter, switchMap, map } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoComponent } from '../archivo/archivo.component';
import { AuthState } from '../../states/auth.state';
import { Archivo } from '../../interfaces/Archivo.interface';
import * as moment from "moment";
import { CompartirComponent } from '../compartir/compartir.component';
import { Informacion } from '../../interfaces/Informacion.interface';

@Component({
  selector: 'app-archivos',
  templateUrl: './archivos.component.html',
  styleUrls: ['./archivos.component.css']
})
export class ArchivosComponent implements OnInit {
  @Input('proId') proId: number;

  @Input('archivos')
  set archivos(archivos: Archivo[]) {
    this.archivo$ = of(archivos)
  }

  @Input('informacion')
  set informacion(informacion: Informacion) {
    if (informacion.fase) {
      this.faseActual.next(informacion.fase)
    } else {
      this.faseActual.next('Preinversión')
    }
  }

  @Input('editor') editor: boolean;

  imageIcon = faFileImage
  fileIcon = faFileAlt
  upload = faCloudUploadAlt
  user: string;
  fases: string[] = ['Preinversión', 'Ejecución', 'Operación', 'Detenido']

  archivosFiltrado$: Observable<Archivo[]>
  archivo$: Observable<Archivo[]>

  faseActual: BehaviorSubject<string> = new BehaviorSubject('Preiversión')
  tipoActual: BehaviorSubject<string> = new BehaviorSubject('img')
  
  constructor(
    private store: Store,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.user = this.store.selectSnapshot(AuthState.user)
    this.realizarFiltado()
  }

  momentTS = ts =>
    moment(ts)
      .locale("es-us")
      .calendar();


  realizarFiltado = () => {
    this.archivosFiltrado$ = combineLatest(this.faseActual, this.tipoActual, this.archivo$)
    .pipe(
      switchMap(([fase, type, archivos]) => {
        return of(archivos.filter(archivo => archivo.fase == fase && archivo.type == type))
      })
    )
  }

  nuevaFase = (fase) => {
    this.faseActual.next(fase)
  }

  nuevoTipo = (tipo) => {
    this.tipoActual.next(tipo)
  }

  nuevoArchivo = (archivo?: Archivo) => {
    let data
    if (archivo) {
      data = { archivo: archivo }
    } else {
      data = { fase: this.faseActual.getValue(), type: this.tipoActual.getValue(), user: this.user, proId: this.proId }
    }

    let file = this.dialog.open(ArchivoComponent, {
      height: '520px',
      width: '600px',
      data: data
    })
    file.afterClosed().subscribe(res => {
      if (res) {
        // this.filtradoSus.unsubscribe()
        this.realizarFiltado()
      }
    })
  }

  compartir = ({ url, fase }) => {
    let user = this.user
    this.dialog.open(CompartirComponent, {
      height: '200px',
      width: '600px',
      data: { user, url, fase }
    })
  }

  irArchivo = (url: string) => {
    window.open(url);
  }

}
