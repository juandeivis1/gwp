import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Parametros, Distrito, Ciudad, Comunidad, Barrio } from 'src/app/interfaces/Parametros.interface';
import { ApiService } from 'src/app/services/api.service';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthState } from 'src/app/states/auth.state';

@Component({
  selector: 'app-filtros-proyectos',
  templateUrl: './filtros-proyectos.component.html',
  styleUrls: ['./filtros-proyectos.component.css']
})
export class FiltrosProyectosComponent implements OnInit {
  parametros: Parametros;
  districts: Distrito[];
  cities: Ciudad[];
  communities: Comunidad[];
  neighborhoods: Barrio[];
  filtros: FormGroup;
  consultando = false;

  constructor(
    private store: Store,
    private fb: FormBuilder,
    private api: ApiService,
    private dialog: MatDialogRef<FiltrosProyectosComponent>
  ) { }

  ngOnInit() {
    this.parametros = this.store.selectSnapshot(state =>
      state.parametros
    );
    this.districts = this.parametros.distritos;
    this.cities = this.parametros.ciudades;
    this.communities = this.parametros.comunidades;
    this.neighborhoods = this.parametros.barrios;
    this.filtros = this.fb.group({
      portafolio: [''],
      programa: [''],
      prioridad: [''],
      district: [''],
      city: [''],
      community: [''],
      neighborhood: ['']
    });
  }

  filtrarCiudades = (distritos) => {
    this.cities = this.parametros.ciudades.filter(ciudad => distritos.includes(ciudad.district));
    this.filtrarComunidades(this.cities.map(ciudad => ciudad.id));
  }

  filtrarComunidades = (ciudades) => {
    this.communities = this.parametros.comunidades.filter(comunidad => ciudades.includes(comunidad.city));
    this.filtrarBarrios(this.communities.map(comunidad => comunidad.id));
  }

  filtrarBarrios = (comunidades) => {
    this.neighborhoods = this.parametros.barrios.filter(comunidad => comunidades.includes(comunidad.community));
  }

  buscar = () => {
    this.consultando = true;
    this.api.buscarProyectos(this.filtros.value).subscribe((res: string) => {
      const token = this.store.selectSnapshot(AuthState.token);
      const proyectosDecrypted: any[] = this.api.decrypt(res, token);
      const procesado = proyectosDecrypted.map(camino => {
        if (camino.coordenadas) {
          const _camino = JSON.parse(camino.coordenadas);
          if (_camino) {
            camino.coordenadas = _camino.map(x => Object.assign({ lat: x[1], lng: x[0] }));
          }
        }
        return camino;
      });
      this.dialog.close(procesado);
    }, err => {
      this.consultando = false;
    });
  }


}
