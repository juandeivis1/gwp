import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltrosProyectosComponent } from './filtros-proyectos.component';

describe('FiltrosProyectosComponent', () => {
  let component: FiltrosProyectosComponent;
  let fixture: ComponentFixture<FiltrosProyectosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltrosProyectosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltrosProyectosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
