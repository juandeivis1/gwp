import { Component, OnInit, Input } from "@angular/core";
import { faEdit, faSave } from "@fortawesome/free-regular-svg-icons";
import {
  faTimes,
  faPlusCircle,
  faReplyAll,
  faSyncAlt,
  faCheck
} from "@fortawesome/free-solid-svg-icons";
import { Observable, of } from "rxjs";
import {
  Store,
  ofActionSuccessful,
  Actions,
  ofActionErrored
} from "@ngxs/store";
import { Informacion } from "../../interfaces/Informacion.interface";
import { DatePipe } from "@angular/common";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Miembro } from "../../interfaces/Miembro.interface";
import { Parametros } from "../../interfaces/Parametros.interface";
import { MatDialog } from '@angular/material/dialog';
import { MapaDefinirComponent } from "../mapa-definir/mapa-definir.component";
import { Navigate } from '@ngxs/router-plugin';
import { NuevaInformacion } from "../../states/proyectos";
import * as _ from 'lodash';

@Component({
  selector: "app-informacion",
  templateUrl: "./informacion.component.html",
  styleUrls: ["./informacion.component.css"]
})
export class InformacionComponent implements OnInit {
  @Input("proId") proId: number;
  
  @Input("informacion")
  set informacion(informacion: Informacion) {
    this.informacion$ = _.isEmpty(informacion) ? of(null) : of(informacion)
  }
  @Input("editor") editor: boolean;

  public informacion$: Observable<Informacion>;
  // iconos
  public editar = faEdit;
  public cancelar = faTimes;
  public guardar = faSave;
  public mas = faPlusCircle;
  public todas = faReplyAll;
  public sync = faSyncAlt;
  public listo = faCheck;

  public trabajando = false;
  public tempEquipo: string[];
  public editando: boolean = false;

  parametros: Observable<Parametros>

  private defaultInformacion: Informacion = {
    equipo: [],
    descripcion: null,
    ubicacion: null,
    coordenadas: null,
    dimensiones: null,
    fase: null,
    vidaUtil: null
  };
  public NewInformacion: Informacion = {};
  constructor(
    private store: Store,
    private actions: Actions,
    private dp: DatePipe,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    actions.pipe(ofActionSuccessful(NuevaInformacion)).subscribe(res => {
      this.trabajando = false;
    });
    actions.pipe(ofActionErrored(NuevaInformacion)).subscribe(res => {
      this.trabajando = false;
      this.snackBar.open("Error de comunicación con MPZ...", "Ok");
    });
  }

  ngOnInit() {
    this.parametros = this.store.select(state =>
      state.parametros
    );    
  }

  private obtenerInformacion = () => {
    this.informacion$ = this.store.select(
      state => state.informacion.find(p => p.proId == this.proId).informacion
    );
  };

  editarDatos = () => {
    this.editando = true;
    this.informacion$.subscribe(
      informacion =>
        informacion
          ? this.cargarDatos(informacion)
          : this.cargarDatos(this.defaultInformacion)
    );
  };

  private cargarDatos = (informacion: Informacion) => {
    informacion.equipo
      ? (this.tempEquipo = informacion.equipo.map(miembro => miembro.nombre))
      : [];
    this.NewInformacion = Object.assign({}, informacion);
  };

  cancelarInformacion = () => {
    this.editando = false;
    this.NewInformacion = {};
  };

  guardarInformacion = () => {
    let nuevo = Object.assign({}, this.NewInformacion);
    this.trabajando = true;
    this.store.dispatch(new NuevaInformacion(this.proId, nuevo));
    this.actions.pipe(ofActionSuccessful(NuevaInformacion)).subscribe(res => {
      this.trabajando = false;
      this.snackBar.open("Información existosa :) ","Ok",{duration: 2500});
      this.cancelarInformacion();
    });
  };

  agregarMiembro = (miembrosEquipo) => {
    if (this.NewInformacion.equipo) {
      this.NewInformacion.equipo = this.NewInformacion.equipo.filter(
        miembro =>
          miembrosEquipo.value.findIndex(nuevo => nuevo == miembro.nombre) > -1
      );
    }

    this.NewInformacion.equipo = miembrosEquipo.value.reduce(
      (miembros: Miembro[], actual: string) => {
        let indice = miembros.findIndex(miembro => miembro.nombre == actual);
        indice == -1 ? miembros.push({ nombre: actual }) : null;
        return miembros;
      },
      this.NewInformacion.equipo || []
    );
  };

  verEnMapa = () => {
    this.store.dispatch( new Navigate(["/gestion/mapa", this.proId ]))
  }

  abrirMapa = () => {
    let puntos = []
    if (this.NewInformacion.coordenadas) {
      puntos = this.NewInformacion.coordenadas
    } 

    let dialogRef = this.dialog.open(MapaDefinirComponent, {
      width: "600px",
      hasBackdrop: true,
      disableClose: true,
      data: { puntos: puntos }
    });

    dialogRef.afterClosed().subscribe(nuevosPuntos => {
      if (nuevosPuntos) {
         this.NewInformacion.coordenadas = nuevosPuntos;
       }
    });
  }
}
