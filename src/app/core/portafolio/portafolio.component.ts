import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable, BehaviorSubject, Subscription, combineLatest, of } from 'rxjs';
import { Proyecto } from '../../interfaces/Proyecto';
import { switchMap, tap, map } from 'rxjs/operators';
import { SincronizarPortafolio } from '../../states/proyectos';
import { Navigate } from '@ngxs/router-plugin';
import { NuevoProyectoComponent } from '../nuevo-proyecto/nuevo-proyecto.component';
import { MatDialog } from '@angular/material/dialog';
import { AuthState } from 'src/app/states/auth.state';
import { faPlusSquare } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-portafolio',
  templateUrl: './portafolio.component.html',
  styleUrls: ['./portafolio.component.css']
})
export class PortafolioComponent implements OnInit {

  proyectos$: Observable<Proyecto[]>;
  portafolio: string;
  cantidad: BehaviorSubject<number> = new BehaviorSubject(10);
  cantidadProyectos = 0;
  private update = false;
  public mas = faPlusSquare;


  private paramsSus: Subscription;
  constructor(
    private route: ActivatedRoute,
    private store: Store,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    const proyectos = this.store.select<Proyecto[]>(state => state.proyectos);
    this.proyectos$ = combineLatest(proyectos, this.cantidad, this.route.params)
    .pipe(switchMap(([_proyectos, cantidad, { portafolio }]) => {

      if (!this.update) {
        const token = this.store.selectSnapshot(AuthState.token);
        this.store.dispatch(new SincronizarPortafolio(portafolio, token));
        this.update = true;
      }

      setTimeout(() => {
        this.portafolio = portafolio;
      }, 100);

      const proyectosProcesados = _proyectos
        .filter(pro => pro.portafolio === portafolio)
        .map((proyecto: Proyecto) => {
          !!proyecto.bitacora ? proyecto.bitacora = proyecto.bitacora.slice(-2) : null;
          return proyecto;
        })
        .slice(0, cantidad);
      this.cantidadProyectos = _proyectos.length;
      return of(proyectosProcesados);
    }));
  }

  cargarMas = () => {
    this.cantidad.next(this.cantidad.getValue() + 10);
  }

  get cantidadActual(): number {
    return this.cantidad.getValue();
  }

  irProyecto = id => {
    this.store.dispatch(new Navigate(['/gestion/proyectos/actual/', id]));
  }

  mostrar = () => {
    this.dialog.open(NuevoProyectoComponent, { height: '710px', width: '600px', disableClose: true, data: { portafolio: this.portafolio } })
  }

  isEditor = () => {
    const permisos: string[] = this.store.selectSnapshot(AuthState.permisos);
    return permisos.includes('edit');
  }



}
