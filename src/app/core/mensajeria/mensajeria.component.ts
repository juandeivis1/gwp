import { Component, OnInit, Input } from "@angular/core";
import {
  Store,
  Actions,
  ofActionSuccessful,
  ofActionErrored
} from "@ngxs/store";
import { Observable, of, BehaviorSubject, combineLatest } from "rxjs";
import { Mensaje } from "../../interfaces/Mensaje.interface";
import {
  faSyncAlt,
  faPencilAlt,
  faPlusCircle,
  faReplyAll,
  faPaperPlane,
  faThumbsDown,
  faThumbsUp,
  faSpinner
} from "@fortawesome/free-solid-svg-icons";
import { MatDialog } from "@angular/material/dialog";
import { FormControl, Validators, FormBuilder } from "@angular/forms";
import { AuthState } from "../../states/auth.state";
import { MatSnackBar } from "@angular/material/snack-bar";
import * as moment from "moment";
import { NuevoMensaje } from "src/app/states/proyectos";
import { Informacion } from "src/app/interfaces/Informacion.interface";
import * as _ from 'lodash';
import { switchMap } from "rxjs/operators";

@Component({
  selector: "app-mensajeria",
  templateUrl: "./mensajeria.component.html",
  styleUrls: ["./mensajeria.component.css"]
})
export class MensajeriaComponent implements OnInit {
  @Input("proId") proId: number;

  @Input("mensajes")
  set mensajes(mensajes: Mensaje[]) {
    this.lineasMensajes = of(mensajes);
    this.mensajes$ = combineLatest(this.lineasMensajes, this.mensajesMostrados).pipe(switchMap(([_mensajes, _mostrados]) => {
      return of(_mensajes.slice(_mostrados))
    }));
  }

  @Input("informacion")
  set informacion(informacion: Informacion) {
    _.isEmpty(informacion)
      ? (this.sinMiembros = true)
      : informacion.equipo.length == 0
        ? (this.sinMiembros = true)
        : null;
  }

  @Input("editor") editor: boolean;

  public mensajes$: Observable<Mensaje[]>;
  public sync = faSyncAlt;
  public escribir = faPencilAlt;

  public trabajando = false;
  public lineasMensajes: Observable<Mensaje[]>;
  public mas = faPlusCircle;
  public enviar = faPaperPlane;
  public todas = faReplyAll;
  public mal = faThumbsDown;
  public bien = faThumbsUp;
  public faSpinner = faSpinner;
  public sinMiembros: boolean = false;
  public enviando: boolean = false;
  public usuario: string;
  public nuevoMensaje: FormControl;
  public mensajesMostrados = new BehaviorSubject(-5);

  constructor(
    private store: Store,
    public dialog: MatDialog,
    private actions: Actions,
    private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.nuevoMensaje = this.fb.control("", [
      Validators.required,
      Validators.minLength(10),
      Validators.pattern("^[^'\"]*$")
    ]);
    this.usuario = this.store.selectSnapshot(AuthState.user);
  }

  momentTS = ts =>
    moment(ts)
      .locale("es-us")
      .calendar();

  final = (lineas) => {
    return Math.abs(this.mensajesMostrados.getValue()) < lineas
  }

  enviarMensaje = () => {
    this.enviando = true;
    let mensajeEnviar: Mensaje = Object.assign({ mensaje: this.nuevoMensaje.value }, { usuario: this.usuario, ts: Date.now(), proId: this.proId });
    this.store.dispatch(new NuevoMensaje(mensajeEnviar));
    this.actions.pipe(ofActionSuccessful(NuevoMensaje)).subscribe(res => {
      this.enviando = false;
      this.nuevoMensaje.reset();
    });
    this.actions.pipe(ofActionErrored(NuevoMensaje)).subscribe(res => {
      this.enviando = false;
      this.snackBar.open(
        "Error al enviar el mensaje, intente de nuevo...",
        "Ok"
      );
    });
  };

  verMas = () => {
    this.mensajesMostrados.next(this.mensajesMostrados.getValue() - 5);
  };
}
