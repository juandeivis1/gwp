import "rxjs/add/observable/of";
import { Observable, of, combineLatest, BehaviorSubject } from "rxjs";
import {
  Store,
  Actions,
  ofActionSuccessful,
  ofActionErrored
} from "@ngxs/store";
import { Component, OnInit, Input } from "@angular/core";
import { Avance } from "../../interfaces/Avance.interface";
import {
  faEdit,
  faTimes,
  faSave,
  faPlusCircle,
  faReplyAll,
  faSyncAlt,
  faCheck,
  faPencilAlt
} from "@fortawesome/free-solid-svg-icons";

import { Bitacora } from "../../interfaces/Bitacora.interface";
import { AuthState } from "../../states/auth.state";
import { Pendiente } from "../../interfaces/Pendiente.interface";
import { FormControl, Validators, FormBuilder, FormGroup } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import * as _ from 'lodash'
import { NuevaBitacora, NuevoPendiente, FinalizarPendiente, NuevoAvance } from "../../states/proyectos";
import { tap, switchMap } from "rxjs/operators";
import { MAT_DATE_LOCALE, DateAdapter } from "@angular/material/core";
@Component({
  selector: "app-avance",
  templateUrl: "./avance.component.html",
  styleUrls: ["./avance.component.css"]
})
export class AvanceComponent implements OnInit {
  @Input("proId") private proId: number;


  @Input("bitacora")
  set bitacora(bitacora: Bitacora[]) {
    this.bitacora$ = of(bitacora);
    this.lineasBitacora = combineLatest(this.lineas, this.bitacora$).pipe(switchMap(([lines, _bitacora]) => {
      return of(_bitacora.slice(lines));
    }))

  }

  @Input("pendientes")
  set pendientes(pendientes: Pendiente[]) {
    this.pendiente$ = of(pendientes)
  }

  @Input("avance")
  set avance(avance: Avance) {
    this.avance$ = _.isEmpty(avance) ? of(null) : of(avance)
  }

  @Input("editor") editor: boolean;

  // Observables
  public avance$: Observable<Avance>;
  public bitacora$: Observable<Bitacora[]>;
  public pendiente$: Observable<Pendiente[]>;

  //
  mostarLiena: boolean = false;
  inicio: Date;
  final: Date;
  entrega: number;
  hoy: number;

  // Iconos
  public editar = faEdit;
  public cancelar = faTimes;
  public guardar = faSave;
  public mas = faPlusCircle;
  public todas = faReplyAll;
  public sync = faSyncAlt;
  public listo = faCheck;
  public agregar = faPencilAlt;

  public porcentajes: number[] = [
    1,
    5,
    10,
    15,
    20,
    25,
    30,
    35,
    40,
    45,
    50,
    55,
    60,
    65,
    70,
    75,
    80,
    85,
    90,
    95,
    100
  ];

  public editando: boolean = false;
  public lineasBitacora: Observable<Bitacora[]>;
  public lineas = new BehaviorSubject(-1);
  public trabajando = false;

  // obejtos
  public avanceEditar: FormGroup;
  public linea: FormControl;
  public pendiente: FormControl;

  public pen: Pendiente = null;

  constructor(
    private store: Store,
    private actions: Actions,
    private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {
    actions
      .pipe(
        ofActionSuccessful(
          NuevaBitacora,
          NuevoPendiente,
          FinalizarPendiente,
          NuevoAvance
        )
      )
      .subscribe(res => {
        this.trabajando = false;
      });

    actions
      .pipe(
        ofActionErrored(
          NuevaBitacora,
          NuevoPendiente,
          FinalizarPendiente,
          NuevoAvance
        )
      )
      .subscribe(res => {
        this.trabajando = false;
        this.snackBar.open("Error de comunicación con MPZ...", "Ok");
      });
  }

  ngOnInit() {
    this.nuevaBitacora();
    this.hoy = Date.now();
  }

  getDate = (ts: number) => {
    return !!ts ? new Date(ts) : null
  }

  private crearNuevos = () => {
    this.iniciarPendiente();
  };

  private iniciarPendiente = () => {
    this.pendiente = this.fb.control('', [Validators.required, Validators.minLength(10)]);
  };

  masLineas = () => {
    this.lineas.next(this.lineas.getValue() - 2);
  };

  mostrarBitacora = () => {
    this.mostarLiena = !this.mostarLiena;
    this.linea.reset()
  }

  todasLineas = () => {
    this.lineasBitacora = this.bitacora$.pipe(switchMap(lineas => {
      return of(lineas)
    }))
  };

  definirFecha = ({ value }, inicio: boolean) => {
    const ts = this.generarFecha(value);
    if (inicio) {
      this.avanceEditar.patchValue({
        inicio: ts
      });
    } else {
      this.avanceEditar.patchValue({
        fin: ts
      });
    }
  };

  editarDatos = () => {
    this.crearNuevos();
    let avanceSubs = this.avance$.subscribe(
      avance => {
        if (avance) {
          this.cargarDatos(avance);
        } else {
          this.avanceEditar = this.fb.group({
            proId: [this.proId],
            porcentaje: [null],
            inicio: [null],
            fin: [null],
            plazo: [null]
          });
        }
      });
    avanceSubs.unsubscribe();
    this.editando = true;
  };

  private cargarDatos = (avance: Avance) => {
    this.inicio = this.getDate(avance.inicio);
    this.final = this.getDate(avance.fin);
    if (!!avance.inicio && !!avance.plazo) {
      this.entrega = avance.inicio + (avance.plazo * 86400000)
    }
    this.avanceEditar = this.fb.group({
      avanceId: [avance.avanceId],
      proId: [avance.proId],
      porcentaje: [avance.porcentaje],
      inicio: [avance.inicio],
      fin: [avance.fin],
      plazo: [avance.plazo]
    });
  };

  guardarAvance = () => {
    this.trabajando = true;
    const avanceGuardar = this.avanceEditar.value;
    this.store.dispatch(new NuevoAvance(avanceGuardar, this.proId));
    this.actions.pipe(ofActionSuccessful(NuevoAvance)).subscribe(res => {
      this.snackBar.open("Avance actualizo :)", "Ok", { duration: 2000 });
      this.cancelarAvance();
    });
  };

  agregarPendiente = () => {
    let pendienteCrear = { asunto: this.pendiente.value, generado: Date.now() };
    this.trabajando = true;
    this.store.dispatch(new NuevoPendiente(pendienteCrear, this.proId));
    this.actions.pipe(ofActionSuccessful(NuevoPendiente)).subscribe(res => {
      this.pendiente.reset()
      this.snackBar.open("Pendiente agregado :)", "Ok", { duration: 2000 });
    });
  };

  agregarBitacora = () => {
    let newBitacora = {
      usuario: this.store.selectSnapshot(AuthState.user),
      linea: this.linea.value.trim(),
      fecha: Date.now()
    };

    this.trabajando = true;
    this.store.dispatch(new NuevaBitacora(newBitacora, this.proId));
    this.actions.pipe(ofActionSuccessful(NuevaBitacora)).subscribe(res => {
      this.linea.reset()
      this.snackBar.open("Bitacora agregada :)", "Ok", { duration: 2000 });
    });
  };

  cancelarAvance = () => {
    this.editando = false;
  };

  private nuevaBitacora = () => {
    this.linea = this.fb.control('', [Validators.required, Validators.minLength(10)])
  };

  generarFecha = (fecha): number => {
    return fecha.getTime();
  };

  pendientesAtendidos = (pendientes: Pendiente[]) => {
    return pendientes.filter(pen => pen.resuelto);
  };

  pendientesSinAtender = (pendientes: Pendiente[]) => {
    return pendientes.filter(pen => !pen.resuelto);
  };

  finalizarPendiente = (pendienteFinalizar: Pendiente) => {
    this.trabajando = true;
    let { pendienteId } = pendienteFinalizar
    let ts = Date.now();
    this.store.dispatch(new FinalizarPendiente(ts, pendienteId, this.proId));
    this.actions.pipe(ofActionSuccessful(FinalizarPendiente)).subscribe(() => {
      pendienteFinalizar = null;
      this.snackBar.open("Pendiente atendido :)", "Ok", { duration: 2000 });
    });
  };

}
