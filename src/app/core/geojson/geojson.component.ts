import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApiService } from '../../services/api.service';
import { tap, finalize, map } from 'rxjs/operators';

@Component({
  selector: 'app-geojson',
  templateUrl: './geojson.component.html',
  styleUrls: ['./geojson.component.css']
})
export class GeojsonComponent implements OnInit {
  geojson: any[] = null;
  subiendo = false;
  cantidad = 0;
  subidos = 0;

  constructor(
    private snack: MatSnackBar,
    private api: ApiService
  ) { }

  ngOnInit() {
  }

  cargarArchivo = (evento) => {
    const file: File = evento.item(0);
    if (!file || file.name.split('.')[1] !== 'geojson') {
      this.snack.open('Extensión de archivo incompatible', 'ok');
      console.error('unsupported file type :( ');
      return;
    }
    const reader = new FileReader();
    reader.readAsText(file, 'UTF-8');
    reader.onload = (evt: any) => {
      const geojson = JSON.parse(evt.target.result).features;
      this.geojson = geojson.map(camino => {
        const a = camino.properties.A ? camino.properties.A.trim() : null;
        const de = camino.properties.De ? camino.properties.De.trim() : null;
        const tempCodigo = camino.properties.Codigo.split('-');

        let tempCamino = Object.assign({
          coordenadas: [camino.geometry.coordinates[0]],
          a: a,
          acuerdo: camino.properties['Acuerdo Consejo'],
          anchoReal: camino.properties['Ancho Real'],
          anchoSuper: camino.properties['Ancho Sup. Ruedo'],
          asfalto: camino.properties['Asfalto (km)'],
          categoria: camino.properties['Categoria'],
          cementado: camino.properties['Cementado (km)'],
          codigo: camino.properties.Codigo,
          de: de,
          derechoVia: camino.properties['DerechoVia'],
          distrito: camino.properties['Distrito'],
          estado: camino.properties['Estado'],
          lastre: camino.properties['Lastre (km)'],
          long: camino.properties['Long(km)'],
          finca: camino.properties['Nº Finca'],
          plano: camino.properties['Nº Plano'],
          tierra: camino.properties['Tierra (km)'],
        });
        if (camino.geometry.coordinates.length === 0) {
          tempCamino = Object.assign(tempCamino, { coordenadas: null });
        } else {
          tempCamino = Object.assign(tempCamino, { coordenadas: camino.geometry.coordinates[0] });
        }
        if (tempCodigo.length > 0) {
          tempCamino = Object.assign(tempCamino, { provincia: Number(tempCodigo[0]),
            canton: Number(tempCodigo[1]), caminoId: Number(tempCodigo[2]) });
        } else {
          tempCamino = Object.assign(tempCamino, { provincia: null, canton: null, caminoId: null });
        }
        return tempCamino;
      });
      this.cantidad = this.geojson.length;
    };
    reader.onerror = function (evt) {
      console.log('error');
    };
  }

  iniciarProceso = () => {
    this.api.borrarCaminos().subscribe(res => {
      this.subiendo = true;
      this.geojson.map((camino, index) => {
        if (camino.id !== 143 && camino.id !== 2686) {
          this.subir(index);
        }
      });
    });
  }

  subir = (actual) => {
    const camino = this.geojson[actual];
    this.api.camino(camino)
      .pipe(
        tap(() => {
          this.subidos = this.subidos + 1;
        })
      )
      .subscribe();
  }

}
