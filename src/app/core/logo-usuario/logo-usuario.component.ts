import { Component, OnInit, Input } from "@angular/core";
import { Store } from "@ngxs/store";
import { AuthState } from "../../states/auth.state";

@Component({
  selector: "app-logo-usuario",
  templateUrl: "./logo-usuario.component.html",
  styleUrls: ["./logo-usuario.component.css"]
})
export class LogoUsuarioComponent implements OnInit {
  @Input("header") head: string = null;

  usuario: string = "*Usuario minicipal*";
  fecha: number = new Date().getTime();

  constructor(private store: Store) {}

  ngOnInit() {
    this.usuario = this.store.selectSnapshot(AuthState.user);
    setInterval(() => {
      this.fecha = new Date().getTime();
    }, 1000);
  }
}
