import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoUsuarioComponent } from './logo-usuario.component';

describe('LogoUsuarioComponent', () => {
  let component: LogoUsuarioComponent;
  let fixture: ComponentFixture<LogoUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
