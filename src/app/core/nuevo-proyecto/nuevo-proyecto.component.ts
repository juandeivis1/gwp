import { Component, OnInit, Input, Output, EventEmitter, Inject, Optional } from "@angular/core";
import { faTimesCircle, faSave } from "@fortawesome/free-regular-svg-icons";
import {
  Store,
  Actions,
  ofActionSuccessful,
  ofActionErrored
} from "@ngxs/store";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Observable } from 'rxjs';
import { Parametros, Distrito, Ciudad, Comunidad, Barrio } from "../../interfaces/Parametros.interface";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { NuevoProyecto } from "src/app/states/proyectos";


@Component({
  selector: "nuevo-proyecto",
  templateUrl: "./nuevo-proyecto.component.html",
  styleUrls: ["./nuevo-proyecto.component.css"]
})
export class NuevoProyectoComponent implements OnInit {
  @Input() mostrar: boolean;
  @Output() noMostrar: EventEmitter<void> = new EventEmitter();

  public cerrar = faTimesCircle;
  public guardar = faSave;
  public anio: number = 18;
  public nuevoProyecto: FormGroup;
  public tipo: string = 'PGV';

  parametros: Parametros;

  constructor(
    private store: Store,
    private actions: Actions,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: { portafolio: string },
    public dialogRef: MatDialogRef<NuevoProyectoComponent>
  ) {
    let portafolio = this.data ? this.data.portafolio : null
    this.nuevoProyecto = this.fb.group({
      nombre: [null, [Validators.required, Validators.minLength(10), Validators.pattern("^[^'\"]*$")]],
      sinonimo: [null, [Validators.required, Validators.minLength(7), Validators.pattern("^[^'\"]*$")]],
      portafolio: [portafolio, [Validators.required]],
      expediente: [null, [Validators.required, Validators.minLength(3)]],
      prioridad: [0, [Validators.required]],
      programa: [null, [Validators.required]]
    });
  }

  ngOnInit() {
    this.parametros = this.store.selectSnapshot(state =>
      state.parametros
    );
  }

  guardarProyecto = () => {
    let proyecto = this.nuevoProyecto.value;
    proyecto.expediente = `Pro-${this.expediente.value}-${this.anio}-${this.tipo}`;
    this.store.dispatch(new NuevoProyecto(proyecto));
    this.actions.pipe(ofActionSuccessful(NuevoProyecto)).subscribe(() => {
      this.snackBar.open("Proyecto agregado con exito", "Ok", {duration: 2500});
      this.nuevoProyecto.reset();
      this.anio = 18;
      this.cerrarNuevo();
    });

    this.actions.pipe(ofActionErrored(NuevoProyecto)).subscribe(() => {
      this.snackBar.open("Algo salio mal, intenterlo de nuevo.", "Ok");
    });
  };

  portafolio = portafolio => {
    switch (portafolio) {
      case "Periodo 2017":
        this.anio = 17;
        break;

      case "Periodo 2018":
        this.anio = 18;
        break;

      case "Periodo 2019":
        this.anio = 19;
        break;

      default:
        let tempAnio = new Date().getFullYear()
        this.anio = Number(tempAnio.toString().split("0")[1])
        break;
    }
  };

  cerrarNuevo = () => {
    this.dialogRef.close()
  };

  get expediente() {
    return this.nuevoProyecto.get("expediente");
  }
}
