import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualEditarComponent } from './actual-editar.component';

describe('ActualEditarComponent', () => {
  let component: ActualEditarComponent;
  let fixture: ComponentFixture<ActualEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
