import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ProyectoStateModel } from "../../interfaces/ProyectoStateModel.interface";
import {
  faSyncAlt,
  faEdit,
  faEllipsisV,
  faTimesCircle,
  faSave
} from "@fortawesome/free-solid-svg-icons";
import { ofActionSuccessful, Actions, Store } from "@ngxs/store";

import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from "rxjs";
import { Parametros } from "../../interfaces/Parametros.interface";
import { ActualizarProyecto } from "src/app/states/proyectos";

@Component({
  selector: "app-actual-editar",
  templateUrl: "./actual-editar.component.html",
  styleUrls: ["./actual-editar.component.css"]
})
export class ActualEditarComponent implements OnInit {
  @Input("proId")
  set avance(proId: number) {
    this.proId = proId;
  }

  @Input("proyecto")
  set proyecto(actual: ProyectoStateModel) {
    this.proyectoActualizar = this.fb.group({
      nombre: actual.nombre,
      sinonimo: actual.sinonimo,
      portafolio: actual.portafolio,
      expediente: actual.expediente,
      prioridad: actual.prioridad,
      programa: actual.programa
    })
    this.procesarNumeros(actual.expediente);
  }
  @Output() editando = new EventEmitter();

  private proId: number;

  public anio: number;
  public numeroExpediente: string = "";
  public tipo: string = 'PGV';
  public sync = faSyncAlt;
  public partes = faEllipsisV;
  public editar = faEdit;
  public cerrar = faTimesCircle;
  public guardar = faSave;
  
  public syncPro: boolean = false;
  
  parametros: Observable<Parametros>

  proyectoActualizar : FormGroup;


  constructor(
    private store: Store, 
    private actions: Actions,
    private fb: FormBuilder) {}

  ngOnInit() {
    this.parametros = this.store.select(state =>
      state.parametros
    );    
  }

  portafolio = (portafolio) => {
    switch (portafolio) {
      case "Periodo 2018":
        this.anio = 18;
        break;

      case "Periodo 2019":
        this.anio = 19;
        break;

      default:
        let tempAnio = new Date().getFullYear()
        this.anio = Number(tempAnio.toString().split("0")[1])
        break;
    }
  };

  procesarNumeros = (expediente) => {
    let datos = expediente.split("-");
    this.anio = datos[2];
    this.tipo = datos[3]
    this.expediente.setValue(datos[1])
  };

  guardarProyecto = () => {
    this.syncPro = true;
    let nuevosDatos = this.proyectoActualizar.value;
    nuevosDatos.expediente = `Pro-${this.expediente.value}-${this.anio}-${this.tipo}`;

    this.store.dispatch(new ActualizarProyecto(this.proId, nuevosDatos));
    this.actions
      .pipe(ofActionSuccessful(ActualizarProyecto))
      .subscribe(res => {
        this.numeroExpediente = "";
        this.syncPro = false;
        this.cerrarNuevo();
      });
  };

  cerrarNuevo = () => {
    this.editando.emit(false);
  };

  get expediente() {
    return this.proyectoActualizar.get("expediente");
  }
}
