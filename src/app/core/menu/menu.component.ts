import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngxs/store';
import { Logout, AuthState } from '../../states/auth.state';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  permisos: string[] = []
  constructor(
    public dialogRef: MatDialogRef<MenuComponent>,
    private store: Store
  ) { }

  ngOnInit() {
    this.permisos = this.store.selectSnapshot(AuthState.permisos);
  }

  cerrarSesion = () => this.store.dispatch(new Logout());

  
  isAdmin = () => {
    return this.permisos.includes("admin");
  };

  isAdminMapa = () => {
    return this.permisos.includes("mapa");
  };

  menuClose = () => {
    this.dialogRef.close()
  }

}
