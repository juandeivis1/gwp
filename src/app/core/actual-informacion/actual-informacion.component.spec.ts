import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualInformacionComponent } from './actual-informacion.component';

describe('ActualInformacionComponent', () => {
  let component: ActualInformacionComponent;
  let fixture: ComponentFixture<ActualInformacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualInformacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualInformacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
