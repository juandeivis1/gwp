import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProyectoStateModel } from '../../interfaces/ProyectoStateModel.interface';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-actual-informacion',
  templateUrl: './actual-informacion.component.html',
  styleUrls: ['./actual-informacion.component.css']
})
export class ActualInformacionComponent implements OnInit {
  @Input() proyecto: ProyectoStateModel;
  @Input() editor: boolean;
  public masInformacion = false;
  public editar = faEdit;

  @Output() editando = new EventEmitter();
  constructor() {}

  ngOnInit() {}

  editarInfo = () => {
    this.editando.emit(true);
  }
}
