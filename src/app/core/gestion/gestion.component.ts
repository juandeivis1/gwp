import { Observable } from "rxjs";
import { Store, Select } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef
} from "@angular/core";
import { MessagingService } from "../../services/messaging.service";
import { CheckParamaters } from '../../states/parametros.state';
import { FormControl } from "@angular/forms";
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BusquedaProyectoComponent } from "../busqueda-proyecto/busqueda-proyecto.component";
import { MatDialog } from '@angular/material/dialog';
import { MenuComponent } from "../menu/menu.component";
import { SwUpdate } from "@angular/service-worker";
import { Proyectos } from "../../states/proyectos";
import { Proyecto } from "src/app/interfaces/Proyecto";

@Component({
  selector: "app-gestion",
  templateUrl: "./gestion.component.html",
  styleUrls: ["./gestion.component.css"]
})
export class GestionComponent implements OnInit {
  
  private pro: any = [];
  @Select(Proyectos.todos) proyectos$: Observable<Proyecto[]>;

  @ViewChild("navbar",{static:false}) navElement: ElementRef;
  @ViewChild("contenidoNav",{static:false}) navContent: ElementRef;

  navHeight: number;
  busqueda = new FormControl();
  constructor(
    private msg: MessagingService,
    private store: Store,
    private bottomSheet: MatBottomSheet,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private swUpdate: SwUpdate
  ) {
    this.msg.getPermission();
    this.proyectos$.subscribe(x => {
      this.pro = x
    });
  }

  ngOnInit() {

    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        let snackBarRef = this.snackBar.open('Nueva versión de GWPVI. Cargar nueva versión?', 'Ok')
        snackBarRef.onAction().subscribe(() => {
          window.location.reload();
        });
      });
    }

    this.store.dispatch(new CheckParamaters())
  }

  irProyecto = (proId) => {
    this.store.dispatch(new Navigate(["/gestion/proyectos/actual/", proId]));
  }

  openMenu = () => {
    this.dialog.open(MenuComponent, {
      height: '600px',
      width: '600px'
    })
  }

  private _filter = (criterio: string): Proyecto[] => {
    const criterioFiltrar = criterio.toString().toLowerCase();
    return this.pro.filter(v =>
      v.nombre.toLowerCase().indexOf(criterioFiltrar.toLowerCase()) > -1 ||
      v.sinonimo.toLowerCase().indexOf(criterioFiltrar.toLowerCase()) > -1 ||
      v.expediente.toLowerCase().indexOf(criterioFiltrar.toLowerCase()) > -1);
  }

  realizarBusqueda = (termino) => {
    let proyectos = this._filter(termino)
    if (proyectos.length > 0) {
      const proyectosSeleccionado = this.bottomSheet.open(BusquedaProyectoComponent, {
        data: proyectos,
      });

      proyectosSeleccionado.afterDismissed().subscribe(proyecto => {
        if (proyecto) {
          this.irProyecto(proyecto)
        }
      })
    } else {
      this.snackBar.open('No coincide con ningún proyecto', 'Ok', {
        duration: 3000
      })
    }
  }
}
