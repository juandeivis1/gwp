import { Component, OnInit, Inject } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-caminos',
  templateUrl: './caminos.component.html',
  styleUrls: ['./caminos.component.css']
})

export class CaminosComponent implements OnInit {

  constructor(
    private bottomSheetRef: MatBottomSheetRef<CaminosComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public camino: any
  ) { }

  ngOnInit() {
  }

  salir = () => {
    this.bottomSheetRef.dismiss();
  }

}
