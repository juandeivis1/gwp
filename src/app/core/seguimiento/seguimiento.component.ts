import { Component, OnInit, Input } from '@angular/core';
import { Seguimiento, Completar } from '../../interfaces/Seguimiento.interface';
import { Observable, of } from 'rxjs';
import {
  Store,
  ofActionSuccessful,
  Actions,
  ofActionErrored
} from '@ngxs/store';
import {
  faSyncAlt,
  faEdit,
  faTimes,
  faPlusCircle,
  faReplyAll,
  faCheck,
  faSave
} from '@fortawesome/free-solid-svg-icons';

import { MatSnackBar } from '@angular/material/snack-bar';
import { Inspeccion } from '../../interfaces/Inspeccion.interface';
import { FormControl, Validators } from '@angular/forms';
import { AuthState } from '../../states/auth.state';
import { DatePipe } from '@angular/common';
import { Parametros } from '../../interfaces/Parametros.interface';
import * as _ from 'lodash';
import { NuevoSeguimiento, InspeccionNueva, Finalizada } from '../../states/proyectos';

@Component({
  selector: 'app-seguimiento',
  templateUrl: './seguimiento.component.html',
  styleUrls: ['./seguimiento.component.css']
})

export class SeguimientoComponent implements OnInit {
  @Input() proId: number;

  @Input() inspecciones: Inspeccion[];

  @Input('seguimiento')
  set seguimiento(seguimiento: Seguimiento) {
    this.seguimiento$ = _.isEmpty(seguimiento) ? of(null) : of(seguimiento);
  }

  @Input() completar: Completar[];

  @Input() editor: boolean;

  // public proId: number;

  public editar = faEdit;
  public cancelar = faTimes;
  public guardar = faSave;
  public mas = faPlusCircle;
  public todas = faReplyAll;
  public sync = faSyncAlt;
  public listo = faCheck;
  public trabajando = false;

  public editando = false;

  public newSeguimiento: Seguimiento = {};

  public seguimiento$: Observable<Seguimiento>;

  public newInspeccion: Inspeccion;

  parametros: Observable<Parametros>;

  public detallesInspeccion: FormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(20)
  ]);

  public ins: Inspeccion = null;

  private defaultSeguimiento;

  constructor(
    private store: Store,
    private actions: Actions,
    private snackBar: MatSnackBar
  ) {
    actions
      .pipe(ofActionSuccessful(NuevoSeguimiento, InspeccionNueva, Finalizada))
      .subscribe(res => {
        this.trabajando = false;
      });

    actions
      .pipe(ofActionErrored(NuevoSeguimiento, InspeccionNueva, Finalizada))
      .subscribe(res => {
        this.trabajando = false;
        this.snackBar.open('Error de comunicación con MPZ...', 'Ok');
      });
  }

  ngOnInit() {
    this.parametros = this.store.select(state =>
      state.parametros
    );

    this.nuevaInspeccion();

    this.defaultSeguimiento = {
      ejecutado: '',
      ejecutar: ''
    };
  }

  editarDatos = () => {
    this.editando = true;
    const subs = this.seguimiento$.subscribe(
      seguimiento =>
        seguimiento
          ? this.cargarDatos(seguimiento)
          : this.cargarDatos(this.defaultSeguimiento)
    );
    subs.unsubscribe();
  }

  cancelarSeguimiento = () => {
    this.editando = false;
    this.newSeguimiento = {};
  }

  guardarSeguimiento = () => {
    const seguimiento = Object.assign({}, this.newSeguimiento);
    this.trabajando = true;
    this.store.dispatch(new NuevoSeguimiento(seguimiento, this.proId));
    this.actions.pipe(ofActionSuccessful(NuevoSeguimiento)).subscribe(res => {
      this.trabajando = false;
      this.snackBar.open('Se ha actualizo de manera correta el seguimiento', 'Ok', { duration: 2000 });
      this.cancelarSeguimiento();
    });
  }

  private cargarDatos = (seguimiento: Seguimiento) => {
    this.newSeguimiento = Object.assign({}, seguimiento);
  }

  inspeccionesPendientes = (inspecciones: Inspeccion[]) => {
    return inspecciones.filter(ins => !ins.usuario);
  }

  inspeccionesCumplidas = (inspecciones: Inspeccion[]) => {
    return inspecciones.filter(ins => ins.usuario);
  }

  inspeccionFinalizada = (inspeccionFinalizar: Inspeccion) => {
    this.trabajando = true;
    inspeccionFinalizar.detalles = this.detallesInspeccion.value;
    inspeccionFinalizar.realizada = Date.now();
    inspeccionFinalizar.usuario = this.store.selectSnapshot(AuthState.user);
    this.store.dispatch(
      new Finalizada(inspeccionFinalizar)
    );
    const subs = this.actions
      .pipe(ofActionSuccessful(Finalizada))
      .subscribe(res => {
        inspeccionFinalizar = null;
        this.ins = null;
        this.detallesInspeccion.reset();
        this.snackBar.open('Se ha finalizo de manera correta la inspección', 'Ok', { duration: 2000 });
      });
    subs.unsubscribe();
  }

  generarFecha = (fecha) => {
    return !!fecha ? new Date(fecha) : null;
  };

  definirInspeccion = ({value}) => {
    this.newInspeccion.programada = value.getTime();
  }

  agregarInspeccion = () => {
    this.trabajando = true;
    this.store.dispatch(new InspeccionNueva(this.proId, this.newInspeccion));
    const subs = this.actions.pipe(ofActionSuccessful(InspeccionNueva)).subscribe(res => {
      this.nuevaInspeccion();
      this.snackBar.open('Inspección agregada :)', 'Ok', { duration: 2000 });
    });
    subs.unsubscribe();
  }

  private nuevaInspeccion = () => {
    this.newInspeccion = {
      programada: (Date.now() + 1000 * 60 * 60 * 24 * 365 * 5)
    };
  }

  checkIns = () => {
    const cantidad = this.inspecciones.filter(
      ins => ins.programada === this.newInspeccion.programada
    );
    return cantidad.length > 0;
  }
}
