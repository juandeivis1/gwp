import { Component, OnInit, Input } from '@angular/core';
import { Proyecto } from '../../interfaces/Proyecto';
import { Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';

@Component({
  selector: 'info-proyecto',
  templateUrl: './info-proyecto.component.html',
  styleUrls: ['./info-proyecto.component.css']
})
export class InfoProyectoComponent implements OnInit {
  @Input('datos') proyecto: Proyecto;
  @Input() index: number;
  constructor(private store: Store) { }

  ngOnInit() {
  }

  irProyecto = (id: number) => {
    this.store.dispatch(new Navigate(['/gestion/proyectos/actual/', id]));
  };

}
