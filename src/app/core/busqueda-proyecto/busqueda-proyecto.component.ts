import { Component, OnInit, Inject } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-busqueda-proyecto',
  templateUrl: './busqueda-proyecto.component.html',
  styleUrls: ['./busqueda-proyecto.component.css']
})
export class BusquedaProyectoComponent implements OnInit {

  constructor(
    private bottomSheetRef: MatBottomSheetRef<BusquedaProyectoComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public proyectos: any
  ) { }

  ngOnInit() {
  }

  seleccionar = ({proId}) => {
    this.bottomSheetRef.dismiss(proId)
  }

}
