import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusquedaProyectoComponent } from './busqueda-proyecto.component';

describe('BusquedaProyectoComponent', () => {
  let component: BusquedaProyectoComponent;
  let fixture: ComponentFixture<BusquedaProyectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusquedaProyectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusquedaProyectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
