import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  Store,
  Actions
} from '@ngxs/store';
import { Observable } from 'rxjs';
import { faMoneyBillAlt, faSave } from '@fortawesome/free-regular-svg-icons';
import {
  faCartPlus,
  faForward,
  faSearch,
  faInfo,
  faExclamation,
  faSyncAlt,
  faEllipsisV,
  faEye,
  faEdit,
  faTimesCircle,
  faEnvelope,
  faCloud
} from '@fortawesome/free-solid-svg-icons';
import { ProyectoStateModel } from '../../interfaces/ProyectoStateModel.interface';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthState } from '../../states/auth.state';
import { RiesgosComponent } from '../riesgos/riesgos.component';
import { SincronizarProyecto, AgregarRiesgo, EliminarRiesgo } from '../../states/proyectos';
import { Proyecto } from 'src/app/interfaces/Proyecto';

@Component({
  selector: 'app-actual',
  templateUrl: './actual.component.html',
  styleUrls: ['./actual.component.css']
})
export class ActualComponent implements OnInit {
  @ViewChild(RiesgosComponent, { static: false }) riesgosApp: RiesgosComponent;

  // iconos
  public file = faCloud;
  public sync = faSyncAlt;
  public partes = faEllipsisV;
  public editar = faEdit;
  public costos = faMoneyBillAlt;
  public adquisiones = faCartPlus;
  public avance = faForward;
  public calidad = faSearch;
  public informacion = faInfo;
  public riesgos = faExclamation;
  public seguimiento = faEye;
  public mensajeria = faEnvelope;

  public proId: number;
  public proyecto$: Observable<Proyecto>;
  public syncPro = false;
  public actualAbierto: string = null;

  public cerrar = faTimesCircle;
  public guardar = faSave;

  public editando = false;
  public nuevosDatos: ProyectoStateModel;

  public numeroExpediente = '';
  public anio: number;

  public pushIn = false;
  public pushOut = false;

  constructor(
    private route: ActivatedRoute,
    private store: Store,
    private actions: Actions,
    private snackBar: MatSnackBar,
  ) {

    this.route.params.subscribe(params => {
      this.actualAbierto = null;
      this.proId = Number(params['proId']);
      const token = this.store.selectSnapshot(AuthState.token);
      this.store.dispatch(new SincronizarProyecto(this.proId, token));
      this.proyecto$ = this.store.select(state => state.proyectos.find(proyecto => proyecto.proId === this.proId));
      params['info'] ? (this.actualAbierto = params['info']) : null;
    });

  }

  ngOnInit() {
  }

  getProyectoInfo = () => {
    this.proyecto$ = this.store.select(state =>
      state.proyectos.find(p => p.proId === this.proId)
    );
  }

  cambiar = (actual) => {
    this.actualAbierto !== actual ? (this.actualAbierto = actual) : null;
  }

  noCambiar = (actual) => {
    this.actualAbierto === actual ? (this.actualAbierto = null) : null;
  }

  esActual = actual => (this.actualAbierto === actual ? true : false);

  portafolio = protafolio => {
    const tempString = protafolio.split('0');
    this.anio = tempString[1];
  }

  procesarNumeros = expediente => {
    const datos = expediente.split('-');
    this.numeroExpediente = datos[1];
    this.anio = datos[2];
  }

  cerrarNuevo = () => {
    this.editando = false;
    this.nuevosDatos = {
      nombre: '',
      sinonimo: '',
      programa: '',
      portafolio: '',
      prioridad: 1,
      expediente: ''
    };
  }

  isEditor = () => {
    const permisos: string[] = this.store.selectSnapshot(AuthState.permisos);
    return permisos.includes('edit');
  }
}
