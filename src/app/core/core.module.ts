import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { ProyectosComponent } from './proyectos/proyectos.component';
import { GestionComponent } from './gestion/gestion.component';
import { InfoProyectoComponent } from './info-proyecto/info-proyecto.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NuevoProyectoComponent } from './nuevo-proyecto/nuevo-proyecto.component';
import { FormsModule } from '@angular/forms';
import { ActualComponent } from './actual/actual.component';
import { SeguimientoComponent } from './seguimiento/seguimiento.component';
import { RiesgosComponent } from './riesgos/riesgos.component';
import { InformacionComponent } from './informacion/informacion.component';
import { CostosComponent } from './costos/costos.component';
import { CalidadComponent } from './calidad/calidad.component';
import { AvanceComponent } from './avance/avance.component';
import { AdquisicionComponent } from './adquisicion/adquisicion.component';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esUsLocale } from 'ngx-bootstrap/locale';
import { ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { DecimalPipe } from '@angular/common';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MapaComponent } from './mapa/mapa.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MensajeriaComponent } from './mensajeria/mensajeria.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MapaDefinirComponent } from './mapa-definir/mapa-definir.component';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { DndModule } from 'ng2-dnd';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ActualEditarComponent } from './actual-editar/actual-editar.component';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ActualInformacionComponent } from './actual-informacion/actual-informacion.component';
import { RiesgosProyectoComponent } from './riesgos-proyecto/riesgos-proyecto.component';
import { RiesgosResultadosComponent } from './riesgos-resultados/riesgos-resultados.component';
import { RiesgosTablaComponent } from './riesgos-tabla/riesgos-tabla.component';
import { RiesgosBuscadorComponent } from './riesgos-buscador/riesgos-buscador.component';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { CaminosComponent } from './caminos/caminos.component';
import { MatListModule } from '@angular/material/list';
import { BusquedaProyectoComponent } from './busqueda-proyecto/busqueda-proyecto.component';
import { GeojsonComponent } from './geojson/geojson.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { ArchivosComponent } from './archivos/archivos.component';
import { ArchivoComponent } from './archivo/archivo.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CompletadoComponent } from './completado/completado.component';
import { MenuComponent } from './menu/menu.component';
import { CompartirComponent } from './compartir/compartir.component';
import 'chart.piecelabel.js';
import { VideosComponent } from './videos/videos.component';
import { PortafoliosComponent } from './portafolios/portafolios.component';
import { PortafolioComponent } from './portafolio/portafolio.component';

import { InspeccionesComponent } from './inspecciones/inspecciones.component';
import { InspeccionComponent } from './inspeccion/inspeccion.component';
import { MatStepperModule } from '@angular/material/stepper';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FiltrosProyectosComponent } from './filtros-proyectos/filtros-proyectos.component';
import { FiltrosCaminosComponent } from './filtros-caminos/filtros-caminos.component';
import { CoreRoutingModule } from './core-routing.module';
import { LogoUsuarioComponent } from './logo-usuario/logo-usuario.component';
import { EncabezadoComponent } from './encabezado/encabezado.component';
defineLocale('es-us', esUsLocale);
import { GaugeChartModule } from 'angular-gauge-chart';

@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    MatMenuModule,
    MatSnackBarModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatSidenavModule,
    MatDialogModule,
    ProgressbarModule.forRoot(),
    DndModule.forRoot(),
    PaginationModule.forRoot(),
    MatBadgeModule,
    MatButtonModule,
    MatTooltipModule,
    MatSortModule,
    MatTableModule,
    MatCardModule,
    MatIconModule,
    MatAutocompleteModule,
    MatBottomSheetModule,
    MatListModule,
    MatProgressBarModule,
    MatButtonToggleModule,
    CarouselModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatStepperModule,
    MatMenuModule,
    DragDropModule,
    MatPaginatorModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    GaugeChartModule,
  ],
  declarations: [
    ProyectosComponent,
    GestionComponent,
    InfoProyectoComponent,
    NuevoProyectoComponent,
    ActualComponent,
    SeguimientoComponent,
    RiesgosComponent,
    InformacionComponent,
    CostosComponent,
    CalidadComponent,
    AvanceComponent,
    AdquisicionComponent,
    MapaComponent,
    MensajeriaComponent,
    MapaDefinirComponent,
    RiesgosTablaComponent,
    RiesgosResultadosComponent,
    RiesgosBuscadorComponent,
    ActualEditarComponent,
    ActualInformacionComponent,
    RiesgosProyectoComponent,
    CaminosComponent,
    BusquedaProyectoComponent,
    GeojsonComponent,
    ArchivosComponent,
    ArchivoComponent,
    CompletadoComponent,
    MenuComponent,
    CompartirComponent,
    VideosComponent,
    PortafoliosComponent,
    PortafolioComponent,
    InspeccionesComponent,
    InspeccionComponent,
    FiltrosProyectosComponent,
    FiltrosCaminosComponent,
    EncabezadoComponent,
    LogoUsuarioComponent,
  ],
  entryComponents: [
    MapaComponent,
    MapaDefinirComponent,
    CaminosComponent,
    BusquedaProyectoComponent,
    ArchivoComponent,
    MenuComponent,
    CompartirComponent,
    NuevoProyectoComponent,
    FiltrosProyectosComponent,
    FiltrosCaminosComponent
  ],
  providers: [
    DecimalPipe,
    DatePipe,
    MatNativeDateModule
  ],
  exports: [
    EncabezadoComponent,
    RiesgosTablaComponent,
    RiesgosResultadosComponent,
    ProgressbarModule,
    GestionComponent,
    ChartsModule,
    FormsModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    MatToolbarModule,
    MatInputModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatDialogModule,
    GaugeChartModule,
  ]
})
export class CoreModule { }
