import { Avance } from './Avance.interface';
import { Archivo } from './Archivo.interface';
import { Calidad } from './Calidad.interface';
import { Costos } from './Costos.interface';
import { Riesgo } from './Riesgo.interface';
import { Seguimiento, Completar } from './Seguimiento.interface';
import { Informacion } from './Informacion.interface';
import { Adquisicion } from './Adquisicion.interface';
import { Mensaje } from './Mensaje.interface';
import { Bitacora } from './Bitacora.interface';
import { Pendiente } from './Pendiente.interface';
import { Inspeccion } from './Inspeccion.interface';

export interface Proyecto {
    proId?: number;
    actualizado?: number;
    creado?: number;
    expediente: string;
    nombre: string;
    sinonimo: string;
    portafolio: string;
    prioridad: number;
    programa: string;
    avance?: Avance;
    bitacora: Bitacora[];
    pendiente: Pendiente[];
    completar: Completar[];
    inspecciones?: Inspeccion[];
    archivos?: Archivo[];
    calidad?: Calidad;
    costos?: Costos;
    riesgos?: Riesgo[];
    seguimiento?: Seguimiento;
    informacion?: Informacion;
    adquisicion?: Adquisicion;
    mensajes?: Mensaje[];
}
