import { Avance } from './Avance.interface';
import { Costos } from './Costos.interface';
import { Riesgo } from './Riesgo.interface';
import { Calidad } from './Calidad.interface';
import { Seguimiento } from './Seguimiento.interface';
import { Informacion } from './Informacion.interface';
import { Adquisicion } from './Adquisicion.interface';
import { Mensaje } from './Mensaje.interface';
import { Archivo } from './Archivo.interface';

export interface InformacionStateModel {
  proId?: number;
  actualizado?: number;
  avance?: Avance;
  archivos?: Archivo[];
  calidad?: Calidad;
  costos?: Costos;
  riesgos?: Riesgo[];
  seguimiento?: Seguimiento;
  informacion?: Informacion;
  adquisicion?: Adquisicion;
  mensajes?: Mensaje[];
}
