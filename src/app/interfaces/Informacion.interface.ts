import { Miembro } from './Miembro.interface';

export interface Informacion {
  proId	?: number;
  informacionId	?: number;
  equipo?: Miembro[];
  descripcion?: string;
  ubicacion?: string;
  coordenadas?: any[];
  dimensiones?: string;
  fase?: string;
  poblacion?: number;
  vidaUtil?: number;
  modalidad?: string;
}
