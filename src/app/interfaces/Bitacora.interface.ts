export interface Bitacora {
  fecha: number;
  linea: string;
  usuario: string;
}
