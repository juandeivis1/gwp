import { Riesgo } from './Riesgo.interface';

export interface Interno {
    riesgos?: Riesgo[];
}
