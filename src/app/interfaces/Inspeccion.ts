export interface InspeccionVial {
    id?: number;
    tipo: number;
    prioridad: number;
    distrito: number;
    camino: string;
    asignado: string;
    creador: string;
    creacion?: number;
    realizado?: number;
    inspector?: string;
    direccion: string;
    objetivo: string;
    detalle: string[];
    recomendacion: string[];
    accionado?: string;
    telefono?: string;
    email?:string;
    revisado?: number;
    aprueba?: string;
}