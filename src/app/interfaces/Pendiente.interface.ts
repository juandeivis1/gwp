export interface Pendiente {
  pendienteId?: number;
  proId?: number;
  asunto: string;
  generado?: number;
  resuelto?: number;
}

