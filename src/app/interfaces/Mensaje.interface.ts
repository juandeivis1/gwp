export interface Mensaje {
    mensajeId?: number;
    proId: number;
    usuario: string;
    mensaje: string;
    ts: number;
}
