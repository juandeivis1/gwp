export interface Admin {
    portafolios: Portafolio[];
    programas: any[];
    pendientes: Pendiente[];
    fecha: any[];
    porpro: any[];
    modalidades: any;
}

export interface Pendiente {
    expediente: string;
    nombre: string;
    proId: number;
    pendientes: PendientePro[];
}

export interface PendientePro {
    asunto: string;
    generado: number;
}

export interface Portafolio {
    portafolio: string;
    resumen: Costo;
}

export interface Fases {
   preinversion: number;
   detenido: number;
   ejecucion: number;
   operacion: number;
}

export interface Costo {
    avance: number;
    prevision: number;
    inversion: number;
    cantidad: number;
    fases: Fases;
}
