export interface Parametros {
    portafolios: DbObject[];
    roles: string[];
    profesionales: DbObject[];
    programas: DbObject[];
    prioridades: number[];
    fases: string[];
    acciones: string[];
    equipo: string[];
    rolesGwpvi: string[];
    cumplir: DbObject[];
    distritos: Distrito[];
    ciudades: Ciudad[];
    comunidades: Comunidad[];
    barrios: Barrio[];
    updates: {
        distritos: number;
        ciudades: number;
        comunidades: number;
        barrios: number;
        portafolios: number;
        programas: number;
        roles: number,
        fases: number;
        profesionales: number;
        acciones: number;
        cumplir: number;
        prioridades: number;
        rolesGwpvi: number;
    }
}

export interface Distrito extends DbObject {
}

export interface Ciudad  extends DbObject {
    district: number;
}

export interface Comunidad extends DbObject {
    city: number;
}

export interface DbObject {
    id?: number;
    name: string;
}

export interface Barrio extends DbObject {
    community: number;
    population: number;
    // roads: number[];
}