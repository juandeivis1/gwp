export interface Inspeccion {
  proId?: number;
  inspeccionId?: number;
  programada: number;
  usuario?: string;
  detalles?: string;
  realizada?: number;
}
