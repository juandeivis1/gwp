export interface Avance {
    avanceId?: number;
    proId?: number;
    porcentaje?: number;
    inicio?: number;
    fin?: number;
    plazo?: number;
}
