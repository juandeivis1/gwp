export interface Riesgo {
  fase: string;
  categoria: string;
  subcategoria: string;
  causa: string;
  consecuencia: string;
  actividad: string;
  idRiesgo?: number;
  prob?: number;
  imp?: number;
  indice?: number;
  respuesta?: string;
  completado?: boolean;
  resId?: number;
  proId?: number;
  riesgoId?: number;
}
