import { Bitacora } from './Bitacora.interface';

export interface ProyectoStateModel {
  nombre: string;
  sinonimo: string;
  portafolio: string;
  prioridad: number;
  programa: string;
  expediente: string;
  porcentaje?: number;
  proId?: number;
  creado?: number;
  actualizado?: number;
  bitacoras?: Bitacora[];
  coordenadas?: [number[]];
}
