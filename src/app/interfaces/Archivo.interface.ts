export interface Archivo {
    fileId?: number;
    url: string;
    ts: number;
    user: string;
    proId: number;
    description: string;
    fase: string;
    type: string;
}
