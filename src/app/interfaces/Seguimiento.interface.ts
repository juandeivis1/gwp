export interface Completar {
    proId?: number;
    completarId?: number;
    punto: string;
    check: boolean;
}

export interface Seguimiento {
    ejecutado?: string;
    ejecutar?: string;
}
