export interface Costos {
    procedencia?: string;
    prevision?: number;
    estimado?: number;
    adjudicado?: number;
    calidad?: number;
    comunales?: number;
    ordenServicio?: number;
    especie?: number;
    especifica?: number;
    ampliacion?: number;
    reajuste?: number;
    instituciones?: number;
    extra1?: number;
    extra2?: number;
}
