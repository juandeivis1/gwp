import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './core/login/login.component';
import { GuardService } from './services/guard.service';
import { AdminGuard } from './services/admin.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'cms', loadChildren: './cms/cms.module#CmsModule', canActivate: [GuardService] },
  { path: 'dev', loadChildren: './dev/dev.module#DevModule', canActivate: [GuardService] },
  { path: 'gestion', loadChildren: './core/core.module#CoreModule', canActivate: [GuardService] },
  { path: 'admin', loadChildren: './admin/admin.module#AdminModule', canActivate: [AdminGuard] },

  { path: '**', redirectTo: '/gestion/portafolios', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
