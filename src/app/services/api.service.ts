import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Avance } from '../interfaces/Avance.interface';
import { Bitacora } from '../interfaces/Bitacora.interface';
import { Inspeccion } from '../interfaces/Inspeccion.interface';
import { Pendiente } from '../interfaces/Pendiente.interface';
import { Seguimiento, Completar } from '../interfaces/Seguimiento.interface';
import { Costos } from '../interfaces/Costos.interface';
import { Calidad } from '../interfaces/Calidad.interface';
import { Informacion } from '../interfaces/Informacion.interface';
import { Adquisicion } from '../interfaces/Adquisicion.interface';
import { Mensaje } from '../interfaces/Mensaje.interface';
import { Riesgo } from '../interfaces/Riesgo.interface';
import { environment } from '../../environments/environment';
import { Archivo } from '../interfaces/Archivo.interface';
import { Proyecto } from '../interfaces/Proyecto';
import * as CryptoJS from 'crypto-js';
import { InspeccionVial } from '../interfaces/Inspeccion';

@Injectable()
export class ApiService {
  private API = null;

  constructor(private http: HttpClient) {
    this.API = environment.API;
  }

  // Common Functions

  decrypt = (data: string, userToken: string) => {
    const bytes = CryptoJS.AES.decrypt(data, userToken);
    const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    return decryptedData;
  }

  // Auth requests

  authApi = ({ user, password }) => {
    return this.http.post(
      `${this.API}auth2/`,
      { user: user, password: password }
    );
  }

  logout = (token: string, fcm: string, user: string) => {
    return this.http.post(
      `${this.API}auth2/logout`,
      { user: user, token: token, fcm: fcm }
    );
  }

  fcmToken = (fcmToken: string) => {
    return this.http.post(
      `${this.API}auth2/fcm`,
      { fcmToken: fcmToken }
    );
  }

  replaceFcmToken = (oldFcmToken: string, newFcmToken: string) => {
    return this.http.post(
      `${this.API}auth2/newfcm`,
      {
        oldFcmToken: oldFcmToken,
        newFcmToken: newFcmToken
      }
    );
  }

  removeFcmToken = (user: string, fcmToken: string) => {
    return this.http.post(
      `${this.API}auth2/removefcm`,
      { user: user, fcmToken: fcmToken }
    );
  }

  // HTTP request del proyectos

  nuevoProyecto = (proyecto: Proyecto) => {
    return this.http.post(
      `${this.API}proyectos2/registrar`,
      { proyecto: proyecto }
    );
  }

  actualizarProyecto = (proId: number, proyecto: Proyecto) => {
    return this.http.post(
      `${this.API}proyectos2/actualizar`,
      { proId: proId, proyecto: proyecto }
    );
  }

  proyectoSincronizar = (proId: number) => {
    return this.http.post(
      `${this.API}proyectos2/sincronizar`,
      { proId: proId }
    );
  }

  portafolioSincronizar = (portafolio: string) => {
    return this.http.post(
      `${this.API}proyectos2/portafolio`,
      { portafolio: portafolio }
    );
  }

  proyectosSincronizar = () => {
    return this.http.get(
      `${this.API}proyectos2/todos`
    );
  }

  // HTTP request del avance

  proyectoAvance = (proId: number, avance: Avance) => {
    return this.http.post(
      `${this.API}avance2/actualizar`,
      { proId: proId, avance: avance }
    );
  }

  proyectBitacora = (proId: number, bitacora: Bitacora) => {
    return this.http.post(
      `${this.API}avance2/bitacora`,
      { proId: proId, bitacora: bitacora }
    );
  }

  proyectoPendiente = (proId: number, pendiente: Pendiente) => {
    return this.http.post(
      `${this.API}avance2/pendiente`,
      { proId: proId, pendiente: pendiente }
    );
  }

  finalizarPendiente = (resuelto: number, pendienteId: number, proId: number) => {
    return this.http.post(
      `${this.API}avance2/pendiente/atendido`,
      { resuelto: resuelto, pendienteId: pendienteId, proId: proId }
    );
  }

  // HTTP request de adquisicion

  proyectoAdquisicion = (proId: number, adquisicion: Adquisicion) => {
    return this.http.post(
      `${this.API}adquisicion2/actualizar`,
      { proId: proId, adquisicion: adquisicion }
    );
  }

  // HTTP request de seguimiento

  proyectoSeguimiento = (proId: number, seguimiento: Seguimiento) => {
    return this.http.post(
      `${this.API}seguimiento2/actualizar`,
      { proId: proId, seguimiento: seguimiento }
    );
  }

  proyectoInspeccion = (proId: number, inspeccion: Inspeccion) => {
    return this.http.post(
      `${this.API}seguimiento2/inspeccion/nueva`,
      { proId: proId, inspeccion: inspeccion }
    );
  }

  proyectoFinalizarInspeccion = (inspeccion: Inspeccion) => {
    return this.http.post(
      `${this.API}seguimiento2/inspeccion/finalizar`,
      { inspeccion: inspeccion }
    );
  }

  proyectoCompletar = (proId: number) => {
    return this.http.post(
      `${this.API}seguimiento2/completar`,
      { proId: proId }
    );
  }

  puntoCompletado = (completar: Completar) => {
    return this.http.post(
      `${this.API}seguimiento2/completar/listo`,
      { completar: completar }
    );
  }

  puntoNuevo = (completar: Completar) => {
    return this.http.post(
      `${this.API}seguimiento2/completar/nuevo`,
      { completar: completar }
    );
  }

  // HTTP request de costos

  proyectoCostos = (proId: number, costos: Costos) => {
    return this.http.post(
      `${this.API}costos2/actualizar`,
      { proId: proId, costos: costos }
    );
  }

  // HTTP request de calidad

  proyectoCalidad = (proId: number, calidad: Calidad) => {
    return this.http.post(
      `${this.API}calidad2/actualizar`,
      { proId: proId, calidad: calidad }
    );
  }

  // HTTP request de información

  proyectoInformacion = (proId: number, informacion: Informacion) => {
    return this.http.post(
      `${this.API}informacion2/actualizar`,
      { proId: proId, informacion: informacion }
    );
  }

  // HTTP request de archivos

  proyectoArchivo = (archivo: Archivo) => {
    return this.http.post(
      `${this.API}archivo2/subir`,
      { archivo: archivo }
    );
  }

  compartirArchivo = (datos) => {
    return this.http.post(
      `${this.API}archivo2/enviar`,
      datos
    );
  }

  // HTTP request de mensajes

  proyectoMensaje = (mensaje: Mensaje) => {
    return this.http.post(
      `${this.API}mensajes2/nuevo`,
      { mensaje: mensaje }
    );
  }

  // HTTP request de reporte

  reporteSeguimiento = (datos) => {
    return this.http.post(
      `${this.API}reporte2/seguimiento`,
      datos
    );
  }

  reporteFase = (datos) => {
    return this.http.post(
      `${this.API}reporte2/fase`,
      datos
    );
  }

  reportePrioridad = (datos) => {
    return this.http.post(
      `${this.API}reporte2/prioridad`,
      datos
    );
  }

  reportePortafolio = () => {
    return this.http.get(
      `${this.API}reporte2/portafolio`
    );
  }

  reportePrograma = () => {
    return this.http.get(
      `${this.API}reporte2/programa`
    );
  }

  reportePendientes = () => {
    return this.http.get(
      `${this.API}reporte2/pendientes`
    );
  }

  reporteModalidad = () => {
    return this.http.get(
      `${this.API}reporte2/modalidad`
    );
  }

  // HTTP request de riesgos

  obtenerRiesgos = () => {
    return this.http.get(this.API + 'riesgos2/obtener');
  }

  insertarRiesgo = (riesgo: Riesgo) => {
    return this.http.post(
      `${this.API}riesgos2/insertar`,
      { riesgo: riesgo }
    );
  }

  agregarRiesgo = (proId: number, riesgo: Riesgo) => {
    return this.http.post(
      `${this.API}riesgos2/agregar`,
      { proId: proId, riesgo: riesgo }
    );
  }

  eliminarRiesgo = (riesgo: Riesgo) => {
    return this.http.post(
      `${this.API}riesgos2/eliminar`,
      { riesgo: riesgo }
    );
  }

  actualizarRiesgo = (riesgo: Riesgo) => {
    return this.http.post(
      `${this.API}riesgos2/actualizar`,
      { riesgo: riesgo }
    );
  }

  // HTTP request de inspecciones

  inspeccionVial = (inspeccion: InspeccionVial) => {
    return this.http.post(
      `${this.API}inspecciones2/guardar`,
      { inspeccion: inspeccion }
    );
  }

  // HTTP request de parametros

  // parametros = () => {
  //   return this.http.get(
  //     `${this.API}parameters/check`
  //   );
  // }

  team = () => {
    return this.http.get(
      `${this.API}parameters/team`
    );
  }

  camino = (camino) => {
    return this.http.post(
      `${this.API}parameters/camino`,
      { camino: camino }
    );
  }

  caminos = (version) => {
    return this.http.post(
      `${this.API}parameters/caminos`,
      { version: version }
    );
  }

  borrarCaminos = () => {
    return this.http.get(
      `${this.API}parameters/borrarCaminos`
    );
  }

  distritos = (update: number) => {
    return this.http.post(
      `${this.API}parameters/district`, {
        update: update
      }
    );
  }

  ciudades = (update: number) => {
    return this.http.post(
      `${this.API}parameters/city`, {
        update: update
      }
    );
  }

  comunidades = (update: number) => {
    return this.http.post(
      `${this.API}parameters/community`, {
        update: update
      }
    );
  }

  barrios = (update: number) => {
    return this.http.post(
      `${this.API}parameters/neighborhood`, {
        update: update
      }
    );
  }

  portafolios = (update: number) => {
    return this.http.post(
      `${this.API}parameters/portfolio`, {
        update: update
      }
    );
  }

  programas = (update: number) => {
    return this.http.post(
      `${this.API}parameters/programs`, {
        update: update
      }
    );
  }

  roles = (update: number) => {
    return this.http.post(
      `${this.API}parameters/roles`, {
        update: update
      }
    );
  }

  fases = (update: number) => {
    return this.http.post(
      `${this.API}parameters/phase`, {
        update: update
      }
    );
  }

  profesionales = (update: number) => {
    return this.http.post(
      `${this.API}parameters/profesionals`, {
        update: update
      }
    );
  }

  acciones = (update: number) => {
    return this.http.post(
      `${this.API}parameters/actions`, {
        update: update
      }
    );
  }

  cumplir = (update: number) => {
    return this.http.post(
      `${this.API}parameters/comply`, {
        update: update
      }
    );
  }

  prioridades = (update: number) => {
    return this.http.post(
      `${this.API}parameters/priority`, {
        update: update
      }
    );
  }

  rolesGwpvi = (update: number) => {
    return this.http.post(
      `${this.API}parameters/permission`, {
        update: update
      }
    );
  }

  newCity = (ciudad) => {
    return this.http.post(
      `${this.API}parameters/addcity`,
      { ciudad: ciudad }
    );
  }

  newCommunity = (comunidad) => {
    return this.http.post(
      `${this.API}parameters/addcommunity`,
      { comunidad: comunidad }
    );
  }

  newNeighborhood = (barrio) => {
    return this.http.post(
      `${this.API}parameters/addneighborhood`,
      { barrio: barrio }
    );
  }

  newComply = (cumplir) => {
    return this.http.post(
      `${this.API}parameters/addcomply`,
      { cumplir: cumplir }
    );
  }

  updateCity = (id, ciudad) => {
    return this.http.post(
      `${this.API}parameters/updatecity`,
      { ciudad, id }
    );
  }

  updateCommunity = (id, comunidad) => {
    return this.http.post(
      `${this.API}parameters/updatecommunity`,
      { id, comunidad }
    );
  }

  updateNeighborhood = (id, barrio) => {
    return this.http.post(
      `${this.API}parameters/updateneighborhood`,
      { id, barrio }
    );
  }

  portfolioCms = () => {
    return this.http.get(
      `${this.API}parameters/portfoliocms`
    );
  }

  newPortfolio = (portafolio) => {
    return this.http.post(
      `${this.API}parameters/addportfolio`,
      { portafolio }
    );
  }

  newProgram = (programa) => {
    return this.http.post(
      `${this.API}parameters/addprogram`,
      { programa }
    );
  }

  newProfesional = (profesional) => {
    return this.http.post(
      `${this.API}parameters/addprofesional`,
      { profesional }
    );
  }

  // HTTP request de mapa

  buscarCamino = (camino) => {
    return this.http.post(
      `${this.API}map/searchRoads`, {
        camino: camino
      }
    );
  }

  buscarProyectos = (proyecto) => {
    return this.http.post(
      `${this.API}map/searchProjects`, {
        filtros: proyecto
      }
    );
  }

  // HTTP request de Dev

  implemented = () => {
    return this.http.get(
      `${this.API}dev/implemented`
    );
  }

  requested = () => {
    return this.http.get(
      `${this.API}dev/requested`
    );
  }

  users = () => {
    return this.http.get(
      `${this.API}dev/users`
    );
  }

  user = (user, userId) => {
    return this.http.post(
      `${this.API}dev/user`,
      { user: user, userId: userId }
    );
  }

  requestImplementation = (request) => {
    return this.http.post(
      `${this.API}dev/request`,
      { request: request }
    );
  }

  finishImplementation = (implementado) => {
    return this.http.post(
      `${this.API}dev/finish`,
      implementado
    );
  }

  nuevosParametros = () => {
    return this.http.post(`${this.API}parameters/nuevosParametros`, null);
  }

  nuevosCaminos = () => {
    return this.http.post(`${this.API}parameters/nuevosCaminos`, null);
  }

  reporteFecha = (datos) => {
    return this.http.post(
      `${this.API}reporte2/fecha`,
      datos
    );
  }

  reportePortafolioPrograma = (datos) => {
    return this.http.post(
      `${this.API}reporte2/porpro`,
      datos
    );
  }
}
