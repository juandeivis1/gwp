import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagingService } from './messaging.service';
import { GuardService } from './guard.service';
import { ApiService } from './api.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    MessagingService,
    GuardService,
    ApiService
  ]
})
export class ServicesModule { }
