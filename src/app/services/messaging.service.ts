import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import {
  AuthState,
  RegistrarFCM,
  ReemplazarFCM,
  TokenInvalido
} from '../states/auth.state';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { mergeMapTo } from 'rxjs/operators';

@Injectable()
export class MessagingService {

  constructor(
    private messaging: AngularFireMessaging,
    private store: Store,
    private snackBar: MatSnackBar
  ) {
  }

  getPermission() {
    this.messaging.requestPermission
      .pipe(mergeMapTo(this.messaging.getToken))
      .subscribe(
        (token) => {
          this.saveToken(token);
        },
        (error) => {
          this.store.dispatch(new TokenInvalido());
          this.snackBar.open('Notificaciones requeridas para GWPVI', 'Ok');
          console.error(error);
        },
      );
  }

  monitorRefresh() {
    this.messaging.tokenChanges
      .subscribe(
        (token) => {
          this.saveToken(token);
        },
        (error) => {
          console.log(error, 'Unable to retrieve new token');
        });
  }

  private saveToken(token): void {
    const currentToken = this.store.selectSnapshot(AuthState.fcm);
    currentToken
      ? currentToken === token
        ? this.store.dispatch(new RegistrarFCM(token))
        : this.store.dispatch(new ReemplazarFCM(currentToken, token))
      : this.store.dispatch(new RegistrarFCM(token));
  }
}
