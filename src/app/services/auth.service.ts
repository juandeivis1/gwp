import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Store } from '@ngxs/store';
import { AuthState } from '../states/auth.state';
import { Observable } from 'rxjs/internal/Observable';
import 'rxjs/add/operator/mergeMap';
import { from } from 'rxjs';
import { Coords } from '../interfaces/Coords';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private coords: Coords = null;

  constructor(private store: Store) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return this.getCoords().mergeMap((_coords: Coords) => {
      req = req.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          Authorization: `${this.store.selectSnapshot(AuthState.token)}`,
          Geolocation: JSON.stringify(this.coords)
        }
      });
      return next.handle(req);
    });
  }

  getCoords() {
    return from(this.getGeolocation());
  }

  getGeolocation = (): Promise<{ lat; lng }> => {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        position => {
          this.coords = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          resolve({
            lat: position.coords.latitude,
            lng: position.coords.longitude
          });
        },
        err => {
          reject({ error: { mpz: 'Geolocalización requerida para GWP!' } });
        }
      );
    });
  }
}
