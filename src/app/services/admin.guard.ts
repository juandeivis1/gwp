import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthState } from '../states/auth.state';
import { Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(private store: Store) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    const permisos: string[] = this.store.selectSnapshot(AuthState.permisos);
    const permiso = permisos.includes('admin');
    if (!permiso) {
      this.store.dispatch(new Navigate(['/gestion/portafolios']));
    }
    return of(permiso);
  }
}
