import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngxs/store';
import { AuthState } from '../states/auth.state';
import { Navigate } from '@ngxs/router-plugin';

@Injectable()
export class GuardService implements CanActivate {

  constructor(private store: Store) { }

  canActivate(): boolean {
    const token = this.store.selectSnapshot(AuthState.token);
    if (token === undefined) {
      this.store.dispatch(new Navigate(['/login']));
    }
    return token !== undefined;
  }

}
