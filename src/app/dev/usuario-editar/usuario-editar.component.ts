import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApiService } from 'src/app/services/api.service';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Parametros } from '../../interfaces/Parametros.interface';

@Component({
  selector: 'app-usuario-editar',
  templateUrl: './usuario-editar.component.html',
  styleUrls: ['./usuario-editar.component.css']
})
export class UsuarioEditarComponent implements OnInit {

  usuario: FormGroup;
  parametros: Observable<Parametros>;
  roles: string[];
  userId: number;

  constructor(
    public dialogRef: MatDialogRef<UsuarioEditarComponent>,
    @Inject(MAT_DIALOG_DATA) public usuarioData: any,
    private api: ApiService,
    private fb: FormBuilder,
    private store: Store,
    private snack: MatSnackBar
  ) { }

  ngOnInit() {
    if (this.usuarioData) {
      this.userId = this.usuarioData.userId;
      this.usuario = this.fb.group({
        user: [this.usuarioData.user, [Validators.required]],
        name: [this.usuarioData.name, [Validators.required]],
        active: [this.usuarioData.active, [Validators.required]],
        team: [this.usuarioData.team, [Validators.required]],
        permissions: this.fb.array(this.usuarioData.permissions)
      });
    } else {
      this.usuario = this.fb.group({
        user: ['', [Validators.required]],
        name: ['', [Validators.required]],
        active: [0, [Validators.required]],
        team: [1, [Validators.required]],
        permissions: this.fb.array([])
      });
    }

    this.parametros = this.store.select(state =>
      state.parametros
    );

    this.parametros.subscribe(params => {
      this.roles = params.rolesGwpvi;
    });
  }

  get permissions() {
    return this.usuario.get('permissions') as FormArray;
  }

  cancelar = () => {
    this.dialogRef.close();
  }

  cambio = (agregar, rol) => {
    if (agregar) {
      this.permissions.push(this.fb.control(rol));
    } else {
      const nuevosRoles: string[] = this.permissions.value;
      const indice = nuevosRoles.indexOf(rol);
      this.permissions.removeAt(indice);
    }
  }

  guardar = () => {
    const usuarioGuardar = this.usuario.value;
    this.api.user(usuarioGuardar, this.userId)
      .subscribe(res => {
        this.dialogRef.close(Object.assign(usuarioGuardar, { userId: this.userId }));
      }, (err) => {
        this.snack.open('Algo salio mal al guardar el usuario', 'Ok');
      });
  }

}
