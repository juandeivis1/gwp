import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Store } from '@ngxs/store';
import { AuthState } from '../../states/auth.state';
import { ApiService } from 'src/app/services/api.service';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.css']
})
export class SolicitudesComponent implements OnInit {

  tipos = ['Mejora', 'Nueva funcionalidad'];
  secciones = [
    'Archivos',
    'Adquisición',
    'Avance',
    'Calidad',
    'Costos',
    'Información',
    'Mensajería',
    'Riesgos',
    'Seguimiento',
    'Nueva sección'
  ];
  solicitud: FormGroup;

  requested: Observable<any>;
  implementado: FormGroup;
  finalizar: number;

  constructor(
    private fb: FormBuilder,
    private store: Store,
    private api: ApiService,
    private snack: MatSnackBar
  ) { }

  ngOnInit() {

    this.requested = this.api.requested();

    this.solicitud = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(10)]],
      tipo: ['', [Validators.required]],
      seccion: ['', [Validators.required]],
      puntos: this.fb.array([])
    });

    this.implementado = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(10)]],
      tipo: ['', [Validators.required]],
      seccion: ['', [Validators.required]],
      implementados: this.fb.array([])
    });

    this.solicitud.get('seccion').valueChanges.subscribe(res => {
      if (res === 'Nueva sección') {
        this.solicitud.addControl('nueva', this.fb.control('', [Validators.required, Validators.minLength(5)]));
      } else {
        this.solicitud.removeControl('nueva');
      }
    });

    this.implementado.get('seccion').valueChanges.subscribe(res => {
      if (res === 'Nueva sección') {
        this.implementado.addControl('nueva', this.fb.control('', [Validators.required, Validators.minLength(5)]));
      } else {
        this.implementado.removeControl('nueva');
      }
    });
  }

  isDeveloper = () => {
    const permisos: string[] = this.store.selectSnapshot(AuthState.permisos);
    return permisos.includes('dev');
  }

  get puntos() {
    return this.solicitud.get('puntos') as FormArray;
  }

  get implementados() {
    return this.implementado.get('implementados') as FormArray;
  }

  agregarPunto = () => {
    this.puntos.push(this.fb.control('', [Validators.required, Validators.minLength(20)]));
  }

  agregarImplementacion = () => {
    this.implementados.push(this.fb.control('', [Validators.required, Validators.minLength(20)]));
  }

  registrarSolicitud = () => {
    const solicitud = this.solicitud.value;
    this.snack.open('Registrando solicitud');
    this.api.requestImplementation(solicitud)
      .subscribe(res => {
        this.solicitud.reset();
        this.snack.open('Solicitud registada con id: ' + res, 'Ok', {
          duration: 2500
        });
      });
  }

  finalizarSoliciud = (id: number) => {
    const datosImplementacion = this.implementado.value;
    const implementado = Object.assign({ implementado: datosImplementacion, id: this.finalizar });
    this.api.finishImplementation(implementado)
      .subscribe(res => {
        this.snack.open(`Implementado de manera correcta: ${res}`, 'Dev', { duration: 2500 });
      });
  }

  quitarPunto = (indice: number) => {
    this.puntos.removeAt(indice);
  }

  quitarImplementacion = (indice: number) => {
    this.implementados.removeAt(indice);
  }

  modificarForm = (implementar) => {
    this.finalizar = implementar.id;
    this.implementado = this.fb.group({
      nombre: [implementar.nombre, [Validators.required, Validators.minLength(10)]],
      tipo: [implementar.tipo, [Validators.required]],
      seccion: [implementar.seccion, [Validators.required]],
      implementados: this.fb.array(implementar.puntos)
    });
    if (implementar.nueva) {
      this.implementado.addControl('nueva', this.fb.control(implementar.nueva, [Validators.required, Validators.minLength(5)]));
    }
  }

}
