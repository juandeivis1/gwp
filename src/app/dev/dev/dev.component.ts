import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { AuthState } from '../../states/auth.state';

@Component({
  selector: 'app-dev',
  templateUrl: './dev.component.html',
  styleUrls: ['./dev.component.css']
})
export class DevComponent implements OnInit {

  constructor(private store: Store) { }

  ngOnInit() {
  }

  isDeveloper = () => {
    const permisos: string[] = this.store.selectSnapshot(AuthState.permisos);
    return permisos.includes("dev");
  };


}
