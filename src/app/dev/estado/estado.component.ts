import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs/Observable';
import { finalize } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-estado',
  templateUrl: './estado.component.html',
  styleUrls: ['./estado.component.css']
})
export class EstadoComponent implements OnInit {

  implemented: Observable<any>;

  constructor(
    private api: ApiService,
    private snack: MatSnackBar
  ) { }

  ngOnInit() {
    setTimeout(() => {      
      this.snack.open('Cargando implementaciones...', 'Ok')
  
      this.implemented = this.api.implemented().pipe(finalize(()=> {
        this.snack.open('Cargadas con exito', 'Ok', {
          duration: 2000
        })
      }))
    }, 500);
  }

}
