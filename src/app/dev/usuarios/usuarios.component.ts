import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Observable } from 'rxjs/internal/Observable';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioEditarComponent } from '../usuario-editar/usuario-editar.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  usuarios: Observable<any>;
  constructor(
    private api: ApiService,
    private dialog: MatDialog
    ) { }

  ngOnInit() {
    this.usuarios = this.api.users();
  }

  nuevoUsuario = () => {
    this.dialog.open(UsuarioEditarComponent, {
      disableClose: true
    });
  }

  editarUsuario = (usuario) => {
    this.dialog.open(UsuarioEditarComponent, {
      disableClose: true,
      data: usuario
    });
  }

}
