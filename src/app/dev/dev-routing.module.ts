import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DevComponent } from "./dev/dev.component";
import { EstadoComponent } from "./estado/estado.component";
import { HerramientasComponent } from "./herramientas/herramientas.component";
import { SolicitudesComponent } from "./solicitudes/solicitudes.component";
import { UsuariosComponent } from "./usuarios/usuarios.component";
import { ReportesComponent } from "./reportes/reportes.component";


const routes: Routes = [
  {
    path: "", component: DevComponent, children: [
      { path: "estado", component: EstadoComponent },
      { path: "herramientas", component: HerramientasComponent },
      { path: "solicitudes", component: SolicitudesComponent },
      { path: "reportes", component: ReportesComponent },
      { path: "usuarios", component: UsuariosComponent },
      { path: "**", redirectTo: "estado", pathMatch: "full" }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevRoutingModule { }
