import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DevRoutingModule } from "./dev-routing.module";
import { MglTimelineModule } from "angular-mgl-timeline";
import { UsuariosComponent } from "./usuarios/usuarios.component";
import { ReportesComponent } from "./reportes/reportes.component";
import { SolicitudesComponent } from "./solicitudes/solicitudes.component";
import { HerramientasComponent } from "./herramientas/herramientas.component";
import { EstadoComponent } from "./estado/estado.component";
import { DevComponent } from "./dev/dev.component";
import { UsuarioEditarComponent } from "./usuario-editar/usuario-editar.component";
import { CoreModule } from "../core/core.module";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDialogModule } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatToolbarModule } from "@angular/material/toolbar";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    DevRoutingModule,
    MglTimelineModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatIconModule,
    MatToolbarModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatDialogModule
  ],
  declarations: [
    UsuariosComponent,
    ReportesComponent,
    SolicitudesComponent,
    HerramientasComponent,
    EstadoComponent,
    DevComponent,
    UsuarioEditarComponent
  ],
  entryComponents: [
    UsuarioEditarComponent
  ]
})
export class DevModule { }