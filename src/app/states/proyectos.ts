import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { Proyecto } from '../interfaces/Proyecto';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApiService } from '../services/api.service';
import { tap } from 'rxjs/operators';
import { Bitacora } from '../interfaces/Bitacora.interface';
import { Pendiente } from '../interfaces/Pendiente.interface';
import { Avance } from '../interfaces/Avance.interface';
import { Seguimiento, Completar } from '../interfaces/Seguimiento.interface';
import { Inspeccion } from '../interfaces/Inspeccion.interface';
import { Costos } from '../interfaces/Costos.interface';
import { Calidad } from '../interfaces/Calidad.interface';
import { Informacion } from '../interfaces/Informacion.interface';
import { Adquisicion } from '../interfaces/Adquisicion.interface';
import { Archivo } from '../interfaces/Archivo.interface';
import { Mensaje } from '../interfaces/Mensaje.interface';
import { Riesgo } from '../interfaces/Riesgo.interface';

export class NuevoProyecto {
    static readonly type = '[Proyecto] Nuevo proyecto';
    constructor(public readonly nuevoProyecto: Proyecto) { }
}

export class ActualizarProyecto {
    static readonly type = '[Proyecto] Actualizar]';
    constructor(public readonly proId: number, public readonly proyecto: Proyecto) { }
}

export class SincronizarProyectos {
    static readonly type = '[Proyectos] Sincronizar todos los proyectos';
    constructor(public readonly token: string) { }
}

export class SincronizarProyecto {
    static readonly type = '[Proyectos] Sincronizar proyecto';
    constructor(public readonly proId: number, public readonly token: string) { }
}

export class SincronizarPortafolio {
    static readonly type = '[Proyectos] Sincronizar portafolio';
    constructor(public readonly portafolio: string, public readonly token: string) { }
}

export class NuevaBitacora {
    static readonly type = '[Proyecto] Nueva bitacora';
    constructor(public readonly bitacora: Bitacora, public readonly proId: number) { }
}

export class NuevoPendiente {
    static readonly type = '[Pendiente Proyecto] Agregar pendiente';
    constructor(public pendiente: Pendiente, public proId: number) { }
}

export class PendienteFinalizado {
    static readonly type = '[Pendiente Proyecto] Finalizar pendiente';
    constructor(public resuelto: number, public pendienteId: number, public proId: number) { }
}

export class FinalizarPendiente {
    static readonly type = '[Pendiente Proyecto] Finalizar pendiente';
    constructor(public resuelto: number, public pendienteId: number, public proId: number) { }
}

export class NuevoAvance {
    static readonly type = '[Avance Proyecto] Nuevo avance]';
    constructor(public readonly avance: Avance, public readonly proId: number) { }
}

export class NuevoSeguimiento {
    static readonly type = '[Seguimiento Proyecto] Nuevo seguimiento]';
    constructor(public readonly seguimiento: Seguimiento, public readonly proId: number) { }
}

export class OrdenarProyectos {
    static readonly type = '[Ordenar Proyectos] Ordenar lista de proyectos';
    constructor(public readonly proyectos: Proyecto[]) { }
}

export class InspeccionNueva {
    static readonly type = '[Inspeccion Proyecto] Nueva inspeccion';
    constructor(public readonly proId: number, public readonly inspeccion: Inspeccion) { }
}

export class Finalizada {
    static readonly type = '[Inspeccion Proyecto] Inspeccion finalizada';
    constructor(public readonly inspeccionFinalizada: Inspeccion) { }
}

export class NuevosCostos {
    static readonly type = '[Costos Proyecto] Nuevos Costos';
    constructor(public readonly proId: number, public readonly costos: Costos) { }
}

export class NuevaCalidad {
    static readonly type = '[Calidad Proyecto] Nuevo control calidad';
    constructor(public readonly proId: number, public readonly calidad: Calidad) { }
}

export class NuevaInformacion {
    static readonly type = '[Información Proyecto] Nueva  información';
    constructor(public readonly proId: number, public readonly informacion: Informacion) { }
}

export class NuevaAdquisicion {
    static readonly type = '[Adquisición Proyecto] Nueva adquisicion';
    constructor(public readonly proId: number, public readonly adquisicion: Adquisicion) { }
}

export class NuevoArchivo {
    static readonly type = '[Archivo Proyecto] Nuevo archivo';
    constructor(public readonly archivo: Archivo) { }
}

export class CompartirArchivo {
    static readonly type = '[Compartir Archivo] Compartir archivo';
    constructor(public readonly archivo: any) { }
}

export class NuevoMensaje {
    static readonly type = '[Mensaje Proyecto] Nuevo mensaje';
    constructor(public readonly mensaje: Mensaje) { }
}

export class EliminarRiesgo {
    static readonly type = '[Riesgo Proyecto quitar] Eliminar riesgo';
    constructor(public readonly riesgoEliminar: Riesgo) { }
}

export class ActualizarRiesgo {
    static readonly type = '[Riesgo Proyecto] Actualizar riesgo';
    constructor(public readonly riesgoActualizar: Riesgo) { }
}

export class AgregarRiesgo {
    static readonly type = '[Riesgo Proyecto agreagar] Nuevo riesgo';
    constructor(public readonly proId: number, public readonly riesgo: Riesgo) { }
}

export class GenerarCompletar {
    static readonly type = '[Completar Proyecto] Puntos por completar';
    constructor(public readonly proId: number) { }
}

export class PuntoCompletado {
    static readonly type = '[Completar Proyecto] Puntos por completar listo';
    constructor(public readonly completar: Completar) { }
}

export class AgregarCompletar {
    static readonly type = '[Completar Proyecto] Puntos por completar nuevo';
    constructor(public readonly completar: Completar) { }
}

export class Desencriptar {
    static readonly type = '[Data] Desencriptar]';
    constructor(public readonly data: any) { }

}


export class BorrarProyectos {
    static readonly type = '[Borrar proyectos]';
}

@State<Proyecto[]>({
    name: 'proyectos',
    defaults: []
})

export class Proyectos {
    constructor(
        private api: ApiService,
        private snackBar: MatSnackBar
    ) { }

    @Selector()
    static todos(state: Proyecto[]) {
        return state;
    }

    @Action(SincronizarProyectos, { cancelUncompleted: true })
    sincronizarProyectos({ dispatch, getState }: StateContext<Proyecto[]>, {token}: SincronizarProyectos) {
        return this.api.proyectosSincronizar().pipe(tap((res: {proyectos: string}) => {
            const _proyectos = getState();
            const proyectosDecrypted: Proyecto[] = this.api.decrypt(res.proyectos, token);
            // ** Seguir con los proyectos meramente; JarvisDev
            const indices = proyectosDecrypted.map( proyecto => proyecto.proId );
            const actualizar = _proyectos.filter(pro => indices.includes(pro.proId));
            const merge = proyectosDecrypted.reduce((_pro, actual) => {
                const index = _pro.findIndex(pro => pro.proId === actual.proId);
                index !== -1 ? _pro[index] = {..._pro[index], ...actual} : _pro = [..._pro, actual];
                return _pro;
            }, actualizar);

            dispatch(new OrdenarProyectos(merge));
            this.snackBar.open('Proyectos listos... :)', 'Ok', { duration: 2500 });
        }));
    }

    @Action(NuevoProyecto)
    nuevoProyecto({ dispatch, getState }: StateContext<Proyecto[]>, { nuevoProyecto }: NuevoProyecto) {
        return this.api.nuevoProyecto(nuevoProyecto).pipe(
            tap((res: {proId: number, ts: number}) => {

                const proyectos = getState();

                nuevoProyecto.proId = res.proId;
                nuevoProyecto.creado = res.ts;

                dispatch(new OrdenarProyectos([...proyectos, nuevoProyecto]));
                this.snackBar.open('Proyecto actualizado... :)', 'Ok', { duration: 2500 });
            })
        );
    }

    @Action(ActualizarProyecto)
    actualizarProyecto({ dispatch, getState }: StateContext<Proyecto[]>, { proId, proyecto }: ActualizarProyecto) {
        return this.api.actualizarProyecto(proId, proyecto).pipe(
            tap((res: {ts: number}) => {

                const proyectos = getState();
                const indice = proyectos.findIndex(pro => pro.proId === proId);

                proyectos[indice] = {...proyectos[indice], ...proyecto};
                proyectos[indice].actualizado = res.ts;

                dispatch(new OrdenarProyectos(proyectos));
                this.snackBar.open('Proyecto actualizado... :)', 'Ok', { duration: 2500 });
            })
        );
    }

    @Action(SincronizarProyecto)
    sincronizarProyecto({ dispatch, getState }: StateContext<Proyecto[]>, { proId, token }: SincronizarProyecto) {
        return this.api.proyectoSincronizar(proId).pipe(tap((res: {proyecto: string}) => {
            let proyectos = getState();
            const proyecto: Proyecto = this.api.decrypt(res.proyecto, token);
            const indice = proyectos.findIndex(pro => pro.proId === proyecto.proId);
            if (proyecto.completar.length === 0 ) {
                dispatch(new GenerarCompletar(proId));
            }
            if (indice !== -1) {
                proyectos[indice] = proyecto;
            } else {
                proyectos = [...proyectos, proyecto];
            }
            dispatch(new OrdenarProyectos(proyectos));
            this.snackBar.open('Proyecto sincronizado... :)', 'Ok', { duration: 2500 });
        }));
    }

    @Action(GenerarCompletar)
    generarCompletar({ dispatch, getState }: StateContext<Proyecto[]>, { proId }: GenerarCompletar) {
        return this.api.proyectoCompletar(proId).pipe(tap((res: {lista: Completar[], ts: number}) => {
            const proyectos = getState();
            const indice = proyectos.findIndex(pro => pro.proId === proId);
            proyectos[indice].completar = res.lista;
            proyectos[indice].actualizado = res.ts;
            dispatch(new OrdenarProyectos(proyectos));
        }));
    }

    @Action(PuntoCompletado)
    puntoCompletado({ dispatch, getState }: StateContext<Proyecto[]>, { completar }: PuntoCompletado) {
        return this.api.puntoCompletado(completar).pipe(tap((res: { ts: number}) => {
            const proyectos = getState();
            const indice = proyectos.findIndex(pro => pro.proId === completar.proId);
            const indiceCompletar = proyectos[indice].completar.findIndex(punto => punto.completarId === completar.completarId);
            proyectos[indice].completar[indiceCompletar].check = true;
            proyectos[indice].actualizado = res.ts;
            dispatch(new OrdenarProyectos(proyectos));
        }));
    }

    @Action(AgregarCompletar)
    agregarCompletar({ dispatch, getState }: StateContext<Proyecto[]>, { completar }: AgregarCompletar) {
        return this.api.puntoNuevo(completar).pipe(tap((res: { ts: number, completarId: number }) => {
            const proyectos = getState();
            const indice = proyectos.findIndex(pro => pro.proId === completar.proId);
            completar.completarId = res.completarId;
            proyectos[indice].completar = [...proyectos[indice].completar, completar];
            proyectos[indice].actualizado = res.ts;
            dispatch(new OrdenarProyectos(proyectos));
        }));
    }

    @Action(SincronizarPortafolio)
    sincronizarPortafolio({ dispatch, getState }: StateContext<Proyecto[]>, { portafolio, token }: SincronizarPortafolio) {
        return this.api.portafolioSincronizar(portafolio).pipe(tap((res: {proyectos: string}) => {
            const _proyectos = getState();
            const proyectosNuevos: Proyecto[] = this.api.decrypt(res.proyectos, token);

            const merge = proyectosNuevos.reduce((proyectos, proyecto) => {
                const index = proyectos.findIndex(pro => pro.proId === proyecto.proId);
                if (index !== -1) {
                    proyectos[index] = { ...proyectos[index], ...proyecto };
                } else {
                    proyectos = [...proyectos, proyecto];
                }
                return proyectos;
            }, _proyectos);

            dispatch(new OrdenarProyectos(merge));
            this.snackBar.open(`${portafolio} listo... :)`, 'Ok', { duration: 2500 });
        }));
    }

    @Action(NuevoAvance)
    NuevoAvance({ getState, dispatch }: StateContext<Proyecto[]>, { avance, proId }: NuevoAvance) {
        return this.api.proyectoAvance(proId, avance)
            .pipe(
                tap((ts: number) => {
                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === proId);

                    proyectos[indice].actualizado = ts;
                    proyectos[indice].avance = avance;

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(NuevaBitacora)
    nuevaBitacora({ getState, dispatch }: StateContext<Proyecto[]>, { bitacora, proId }: NuevaBitacora) {
        return this.api.proyectBitacora(proId, bitacora)
            .pipe(
                tap((ts: number) => {

                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === proId);

                    proyectos[indice].actualizado = ts;
                    proyectos[indice].bitacora = [...proyectos[indice].bitacora, bitacora];

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(NuevoPendiente)
    proyectoPendiente({ getState, dispatch }: StateContext<Proyecto[]>, { pendiente, proId }: NuevoPendiente) {
        return this.api.proyectoPendiente(proId, pendiente).pipe(
            tap((x: { pendiente: Pendiente, ts: number }) => {

                const proyectos = getState();
                const indice = proyectos.findIndex(pro => pro.proId === proId);

                proyectos[indice].actualizado = x.ts;
                proyectos[indice].pendiente = [...proyectos[indice].pendiente, x.pendiente];

                dispatch(new OrdenarProyectos(proyectos));
            })
        );
    }

    @Action(FinalizarPendiente)
    finalizarPendiente({ getState, dispatch }: StateContext<Proyecto[]>, { resuelto, pendienteId, proId }: FinalizarPendiente) {
        return this.api.finalizarPendiente(resuelto, pendienteId, proId).pipe(
            tap((ts: number) => {

                const proyectos = getState();
                const indice = proyectos.findIndex(pro => pro.proId === proId);

                proyectos[indice].actualizado = ts;
                const indicePendiente = proyectos[indice].pendiente.findIndex(_pendiente => _pendiente.pendienteId === pendienteId);
                proyectos[indice].pendiente[indicePendiente].resuelto = resuelto;
                dispatch(new OrdenarProyectos(proyectos));
            })
        );
    }

    @Action(NuevoSeguimiento)
    nuevoSeguimiento({ getState, dispatch }: StateContext<Proyecto[]>, { seguimiento, proId }: NuevoSeguimiento) {
        return this.api.proyectoSeguimiento(proId, seguimiento)
            .pipe(
                tap((ts: number) => {
                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === proId);
                    proyectos[indice].actualizado = ts;
                    proyectos[indice].seguimiento = seguimiento;

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(InspeccionNueva)
    inspeccionNueva({ getState, dispatch }: StateContext<Proyecto[]>, { inspeccion, proId }: InspeccionNueva) {
        return this.api.proyectoInspeccion(proId, inspeccion)
            .pipe(
                tap((res: { ts: number, inspeccion: Inspeccion }) => {
                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === proId);
                    proyectos[indice].actualizado = res.ts;
                    proyectos[indice].inspecciones = [...proyectos[indice].inspecciones, res.inspeccion];

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(Finalizada)
    finalizada({ getState, dispatch }: StateContext<Proyecto[]>, { inspeccionFinalizada }: Finalizada) {
        return this.api.proyectoFinalizarInspeccion(inspeccionFinalizada)
            .pipe(
                tap((res: { ts: number }) => {
                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === inspeccionFinalizada.proId);
                    const indiceInspeccion = proyectos[indice].inspecciones
                        .findIndex(_inspeccion => _inspeccion.inspeccionId === inspeccionFinalizada.inspeccionId);

                    proyectos[indice].actualizado = res.ts;
                    proyectos[indice].inspecciones[indiceInspeccion] = inspeccionFinalizada;

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(NuevosCostos)
    nuevosCostos({ getState, dispatch }: StateContext<Proyecto[]>, { costos, proId }: NuevosCostos) {
        return this.api.proyectoCostos(proId, costos)
            .pipe(
                tap((res: { ts: number }) => {
                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === proId);

                    proyectos[indice].costos = costos;
                    proyectos[indice].actualizado = res.ts;

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(NuevaCalidad)
    nuevaCalidad({ getState, dispatch }: StateContext<Proyecto[]>, { calidad, proId }: NuevaCalidad) {
        return this.api.proyectoCalidad(proId, calidad)
            .pipe(
                tap((res: { ts: number }) => {
                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === proId);

                    proyectos[indice].calidad = calidad;
                    proyectos[indice].actualizado = res.ts;

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(NuevaInformacion)
    nuevaInformacion({ getState, dispatch }: StateContext<Proyecto[]>, { informacion, proId }: NuevaInformacion) {
        return this.api.proyectoInformacion(proId, informacion)
            .pipe(
                tap((res: { ts: number }) => {
                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === proId);

                    proyectos[indice].informacion = informacion;
                    proyectos[indice].actualizado = res.ts;

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(NuevaAdquisicion)
    nuevaAdquisicion({ getState, dispatch }: StateContext<Proyecto[]>, { adquisicion, proId }: NuevaAdquisicion) {
        return this.api.proyectoAdquisicion(proId, adquisicion)
            .pipe(
                tap((res: { ts: number }) => {
                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === proId);

                    proyectos[indice].adquisicion = adquisicion;
                    proyectos[indice].actualizado = res.ts;

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(CompartirArchivo)
    compartirArchivo({ }: StateContext<Proyecto[]>, { archivo }: CompartirArchivo) {
        return this.api.compartirArchivo(archivo)
            .pipe(
                tap((res: { successCount: number }) => {
                    this.snackBar.open(`Dispostivos alcanzados: ${res.successCount} `, 'Ok', { duration: 2500 });
                })
            );
    }

    @Action(NuevoArchivo)
    nuevoArchivo({ getState, dispatch }: StateContext<Proyecto[]>, { archivo }: NuevoArchivo) {
        return this.api.proyectoArchivo(archivo)
            .pipe(
                tap((res: { ts: number, update: boolean, fileId: number }) => {
                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === archivo.proId);

                    if (res.update) {
                        const indiceArchivo = proyectos[indice].archivos.findIndex(_archivo => _archivo.fileId === archivo.fileId);
                        proyectos[indice].archivos[indiceArchivo] = archivo;
                    } else {
                        archivo = { ...archivo, fileId: res.fileId };
                        proyectos[indice].archivos = [...proyectos[indice].archivos, archivo];
                    }

                    proyectos[indice].actualizado = res.ts;

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(NuevoMensaje)
    nuevoMensaje({ getState, dispatch }: StateContext<Proyecto[]>, { mensaje }: NuevoMensaje) {
        return this.api.proyectoMensaje(mensaje)
            .pipe(
                tap((res: { ts: number, success: { successCount: number } }) => {

                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === mensaje.proId);

                    this.snackBar.open(`Dispostivos alcanzados: ${res.success.successCount} `, 'Ok', { duration: 2500 });

                    proyectos[indice].mensajes = [...proyectos[indice].mensajes, mensaje];
                    proyectos[indice].actualizado = res.ts;

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(AgregarRiesgo)
    nuevoRiesgo({ getState, dispatch }: StateContext<Proyecto[]>, { proId, riesgo }: AgregarRiesgo) {
        return this.api.agregarRiesgo(proId, riesgo)
            .pipe(
                tap((res: { ts: number, riesgoId: number }) => {

                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === proId);
                    riesgo.proId = proId;
                    riesgo.idRiesgo = riesgo.riesgoId;
                    riesgo.riesgoId = res.riesgoId;
                    proyectos[indice].riesgos = [...proyectos[indice].riesgos, riesgo];

                    proyectos[indice].actualizado = res.ts;

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(EliminarRiesgo)
    eliminarRiesgo({ getState, dispatch }: StateContext<Proyecto[]>, { riesgoEliminar }: EliminarRiesgo) {

        return this.api.eliminarRiesgo(riesgoEliminar)
            .pipe(
                tap((res: { ts: number, riesgoId: number }) => {

                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === riesgoEliminar.proId);

                    proyectos[indice].riesgos = proyectos[indice].riesgos.filter(_riesgo => _riesgo.riesgoId !== riesgoEliminar.riesgoId);
                    proyectos[indice].actualizado = res.ts;

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(ActualizarRiesgo)
    actualizarRiesgo({ getState, dispatch }: StateContext<Proyecto[]>, { riesgoActualizar }: ActualizarRiesgo) {
        return this.api.actualizarRiesgo(riesgoActualizar)
            .pipe(
                tap((res: { ts: number, riesgoId: number }) => {

                    const proyectos = getState();
                    const indice = proyectos.findIndex(pro => pro.proId === riesgoActualizar.proId);
                    const indiceRiesgo = proyectos[indice].riesgos.findIndex(_riesgo => _riesgo.riesgoId === riesgoActualizar.riesgoId);

                    proyectos[indice].riesgos[indiceRiesgo] = riesgoActualizar;
                    proyectos[indice].actualizado = res.ts;

                    dispatch(new OrdenarProyectos(proyectos));
                })
            );
    }

    @Action(OrdenarProyectos)
    ordenarProyectos({ setState }: StateContext<Proyecto[]>, { proyectos }: OrdenarProyectos) {
        proyectos.sort((a, b) => b.actualizado - a.actualizado);
        setState(proyectos);
    }

    @Action(BorrarProyectos)
    BorrarProyectos({ setState }: StateContext<Proyecto[]>) {
        setState([]);
    }

}


