import { State, Action, StateContext, Store, Selector } from '@ngxs/store';
import { Admin, Portafolio } from '../interfaces/Admin.interface';
import { ApiService } from '../services/api.service';
import { tap } from 'rxjs/operators';


export class GetCostosPortafolio {
    static readonly type = '[Costos] Refrescar costos portafolios';
    constructor(public readonly token: string) { }
}

export class GetCostosPrograma {
    static readonly type = '[Costos] Refrescar costos programas';
    constructor(public readonly token: string) { }
}

export class GetPendientes {
    static readonly type = '[Pendientes] Refrescar pendientes de proyectos';
    constructor(public readonly token: string) { }
}

export class GetModalidad {
    static readonly type = '[Modalidad] Refrescar modalidad de proyectos';
    constructor(public readonly token: string) { }
}

export class BorrarAdmin {
    static readonly type = '[Admin] Borrar admin';
}

export class GetFecha {
    static readonly type = '[Fecha] Refrescar proyectos por fecha';
    constructor(public readonly token: string, public readonly filtros: any) { }
}

export class GetPorPro {
    static readonly type = '[PorPro] Refrescar proyectos por portafolio y programa';
    constructor(public readonly token: string, public readonly filtros: any) { }
}

@State<Admin>({
    name: 'admin',
    defaults: {
        portafolios: [],
        programas: [],
        pendientes: [],
        modalidades: [],
        fecha: null,
        porpro: null
    }
})

export class AdminState {
    constructor(private api: ApiService) {
    }

    @Selector()
    static fecha({ fecha }: Admin) {
        return fecha;
    }

    @Selector()
    static porpro({ porpro }: Admin) {
        return porpro;
    }

    @Action(GetCostosPortafolio)
    getCostosPortafolio({ patchState }: StateContext<Admin>, { token }: GetCostosPortafolio) {
        return this.api.reportePortafolio()
            .pipe(tap((res: { portafolios: string }) => {
                const portafolios: Portafolio[] = this.api.decrypt(res.portafolios, token);
                patchState({ portafolios: portafolios });
            }));
    }

    @Action(GetCostosPrograma)
    getCostosPrograma({ patchState }: StateContext<Admin>, { token }: GetCostosPrograma) {
        return this.api.reportePrograma()
            .pipe(tap((res: { programas: string }) => {
                const programas = this.api.decrypt(res.programas, token);
                patchState({ programas: programas });
            }));
    }

    @Action(GetPendientes)
    getPendientes({ patchState }: StateContext<Admin>, { token }: GetPendientes) {
        return this.api.reportePendientes().pipe(
            tap((res: { pendientes: string }) => {
                const pendientes = this.api.decrypt(res.pendientes, token);
                patchState({
                    pendientes: pendientes
                });
            })
        );
    }

    @Action(GetModalidad)
    getModalidad({ patchState }: StateContext<Admin>, { token }: GetModalidad) {
        return this.api.reporteModalidad().pipe(
            tap((res: { modalidades: string }) => {
                const modalidades = this.api.decrypt(res.modalidades, token);

                patchState({
                    modalidades: modalidades
                });
            })
        );
    }

    @Action(BorrarAdmin)
    borrarAdmin({ setState }: StateContext<Admin>) {
        setState({
            portafolios: [],
            programas: [],
            pendientes: [],
            modalidades: [],
            fecha: null,
            porpro: null
        });
    }

    @Action(GetFecha)
    getFecha({ patchState }: StateContext<Admin>, { token, filtros }: GetFecha) {
        return this.api.reporteFecha(filtros).pipe(
            tap((res: any) => {
                const fecha = this.api.decrypt(res, token);
                patchState({ fecha });
            })
        );
    }

    @Action(GetPorPro)
    getPropGetPorPro({ patchState }: StateContext<Admin>, { token, filtros }: GetPorPro) {
        return this.api.reportePortafolioPrograma(filtros).pipe(
            tap((res: any) => {
                const porpro = this.api.decrypt(res, token);
                patchState({
                    porpro
                });
            })
        );
    }
}
