import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { Interno } from '../interfaces/Interno.interface';
import { ApiService } from '../services/api.service';
import { tap } from 'rxjs/operators';
import { Riesgo } from '../interfaces/Riesgo.interface';
import { InspeccionVial } from '../interfaces/Inspeccion';

export class InsertarRiesgo {
  static readonly type = '[Riesgos] Inserter riesgo';
  constructor(public readonly nuevoRiesgo: Riesgo) { }
}

export class ObtenerRiesgos {
  static readonly type = '[Riesgos] Obtener riesgos';
  constructor(public readonly token: string) {}
}

export class BorrarRiesgos {
  static readonly type = '[Proyectos] Borrar información de riesgos';
}

export class InspeccionCamino {
  static readonly type = '[Inspeccion Vial]';
  constructor(public readonly inspeccion: InspeccionVial) {}
}


@State<Interno>({
  name: 'interno',
  defaults: {
    riesgos: []
  }
})
export class InternoState {
  constructor(private api: ApiService) { }

  @Selector()
  static riesgos(state: Interno) {
    return state.riesgos;
  }

  @Action(InsertarRiesgo)
  insertarRiesgo(
    { getState, patchState }: StateContext<Interno>,
    { nuevoRiesgo }: InsertarRiesgo
  ) {
    return this.api.insertarRiesgo(nuevoRiesgo).pipe(
      tap((success: { id: number}) => {
        let riesgos = getState().riesgos;
        nuevoRiesgo.riesgoId = success.id;
        riesgos = [...riesgos, nuevoRiesgo];
        patchState({ riesgos: riesgos });
      })
    );
  }

  @Action(ObtenerRiesgos)
  obtenerRiesgos({ patchState }: StateContext<Interno>, { token }: ObtenerRiesgos) {
    return this.api.obtenerRiesgos().pipe(
      tap((res: { riesgos: string }) => {
        const riesgos: Riesgo[] = this.api.decrypt(res.riesgos, token);
        patchState({ riesgos: riesgos });
      })
    );
  }

  @Action(InspeccionCamino)
  inspeccionCamino({}: StateContext<Interno>, { inspeccion }: InspeccionCamino) {
    return this.api.inspeccionVial(inspeccion)
      .pipe(
        tap(res => {
          console.log(res);
        })
      )
  }

  @Action(BorrarRiesgos)
  borrarRiesgos({ setState }: StateContext<Interno>) {
    setState({ riesgos: [] });
  }

}
