import { State, Action, StateContext, Selector } from '@ngxs/store';
import { ApiService } from '../services/api.service';
import { tap } from 'rxjs/operators';
import { Parametros, Distrito, Ciudad, Comunidad, Barrio, DbObject } from '../interfaces/Parametros.interface';

export class CheckParamaters {
    static readonly type = '[Parametros] CheckParamaters';
}

export class CheckTeam {
    static readonly type = '[Parametros] CheckTeam';
}

export class Distritos {
    static readonly type = '[Parametros] Distritos';
}

export class Ciudades {
    static readonly type = '[Parametros] Ciudades';
}

export class Comunidades {
    static readonly type = '[Parametros] Comunidades';
}

export class Barrios {
    static readonly type = '[Parametros] Barrios';
}

export class Portafolios {
    static readonly type = '[Parametros] Portafolios';
}

export class Programas {
    static readonly type = '[Parametros] Programas';
}

export class Roles {
    static readonly type = '[Parametros] Roles';
}

export class BorrarParametros {
    static readonly type = '[Parametros] Borrar información de parametros';
}

export class AgregarCiudad {
    static readonly type = '[Parametros] Agregar ciudad';
    constructor(public readonly ciudad: Ciudad) { }
}

export class AgregarComunidad {
    static readonly type = '[Parametros] Agregar comunidad';
    constructor(public readonly comunidad: Comunidad) { }
}

export class AgregarBarrio {
    static readonly type = '[Parametros] Agregar barrio';
    constructor(public readonly barrio: Barrio) { }
}

export class ActualizarCiudad {
    static readonly type = '[Parametros] Actualizar ciudad';
    constructor(public readonly ciudad: Ciudad, public readonly id: number) { }
}

export class ActualizarComunidad {
    static readonly type = '[Parametros] Actualizar comunidad';
    constructor(public readonly comunidad: Comunidad, public readonly id: number) { }
}

export class ActualizarBarrio {
    static readonly type = '[Parametros] Actualizar barrio';
    constructor(public readonly barrio: Barrio, public readonly id: number) { }
}

export class AgregarCumplir {
    static readonly type = '[Parametros] Agregar cumplir';
    constructor(public readonly cumplir: string) { }
}

export class AgregarPortafolio {
    static readonly type = '[Parametros] Agregar portafolio';
    constructor(public readonly portafolio: string) { }
}

export class AgregarPrograma {
    static readonly type = '[Parametros] Agregar programa';
    constructor(public readonly programa: string) { }
}

export class AgregarProfesional {
    static readonly type = '[Parametros] Agregar profesional';
    constructor(public readonly profesional: string) { }
}

@State<Parametros>({
    name: 'parametros',
    defaults: {
        portafolios: [],
        roles: [],
        profesionales: [],
        programas: [],
        prioridades: [],
        fases: [],
        acciones: [],
        cumplir: [],
        rolesGwpvi: [],
        equipo: [],
        distritos: [],
        ciudades: [],
        comunidades: [],
        barrios: [],
        updates: {
            distritos: 0,
            ciudades: 0,
            comunidades: 0,
            barrios: 0,
            portafolios: 0,
            programas: 0,
            roles: 0,
            fases: 0,
            profesionales: 0,
            acciones: 0,
            cumplir: 0,
            prioridades: 0,
            rolesGwpvi: 0
        }
    }
})

export class ParametrosState {

    constructor(
        private api: ApiService
    ) { }

    ngxsOnInit({ }: StateContext<Parametros>) {
    }

    @Action(BorrarParametros)
    borrarParametros({ setState }: StateContext<Parametros>) {
        setState({
            portafolios: [],
            roles: [],
            profesionales: [],
            programas: [],
            prioridades: [],
            fases: [],
            acciones: [],
            equipo: [],
            rolesGwpvi: [],
            cumplir: [],
            distritos: [],
            ciudades: [],
            comunidades: [],
            barrios: [],
            updates: {
                distritos: 0,
                ciudades: 0,
                comunidades: 0,
                barrios: 0,
                portafolios: 0,
                programas: 0,
                roles: 0,
                fases: 0,
                profesionales: 0,
                acciones: 0,
                cumplir: 0,
                prioridades: 0,
                rolesGwpvi: 0
            }
        });
    }

    @Selector()
    static equipo(state: Parametros) {
        return state.equipo;
    }

    @Action(AgregarCiudad)
    addCity({ patchState, getState }: StateContext<Parametros>, { ciudad }: AgregarCiudad) {
        return this.api.newCity(ciudad).pipe(
            tap((ciudad: Ciudad) => {   
                const ciudades = getState().ciudades;
                patchState({
                    ciudades: [...ciudades, ciudad]
                })
            })
        );
    }

    @Action(AgregarComunidad)
    agregarComunidad({ patchState, getState }: StateContext<Parametros>, { comunidad }: AgregarComunidad) {
        return this.api.newCommunity(comunidad).pipe(
            tap((comunidad: Comunidad) => {   
                const comunidades = getState().comunidades;
                patchState({
                    comunidades: [...comunidades, comunidad]
                })
            })
        );
    }

    @Action(AgregarBarrio)
    agregarBarrio({ patchState, getState }: StateContext<Parametros>, { barrio }: AgregarBarrio) {
        return this.api.newNeighborhood(barrio).pipe(
            tap((_barrio: Barrio) => {   
                const barrios = getState().barrios;
                patchState({
                    barrios: [...barrios, _barrio]
                })
            })
        );
    }
    
    @Action(ActualizarCiudad)
    actualizarCiudad({ patchState, getState }: StateContext<Parametros>, { id, ciudad }: ActualizarCiudad) {
        return this.api.updateCity(id, ciudad).pipe(
            tap((_ciudad: Ciudad) => {   
                const ciudades = getState().ciudades;
                let ciudadIndex = ciudades.findIndex(ciudad_ => ciudad_.id == id);
                ciudades[ciudadIndex] = {...ciudades[ciudadIndex],..._ciudad};
                patchState({
                    ciudades: [...ciudades]
                })
            })
        );
    }

    @Action(ActualizarComunidad)
    actualizarComunidad({ patchState, getState }: StateContext<Parametros>, { id, comunidad }: ActualizarComunidad) {
        return this.api.updateCommunity(id, comunidad).pipe(
            tap((_comunidad: Comunidad) => {   
                const comunidades = getState().comunidades;
                let comunidadIndex = comunidades.findIndex(comunidad_ => comunidad_.id == id);
                comunidades[comunidadIndex] = {...comunidades[comunidadIndex],..._comunidad};
                patchState({
                    comunidades: [...comunidades]
                })
            })
        );
    }

    @Action(ActualizarBarrio)
    actualizarBarrio({ patchState, getState }: StateContext<Parametros>, { id, barrio }: ActualizarBarrio) {
        return this.api.updateNeighborhood(id, barrio).pipe(
            tap((_barrio: Barrio) => {   
                const barrios = getState().barrios;
                let barrioIndex = barrios.findIndex(barrio_ => barrio_.id == id);
                barrios[barrioIndex] = {...barrios[barrioIndex],..._barrio};
                patchState({
                    barrios: [...barrios]
                })
            })
        );
    }

    @Action(AgregarCumplir)
    agregarCumplir({ patchState, getState }: StateContext<Parametros>, { cumplir }: AgregarCumplir) {
        return this.api.newComply(cumplir).pipe(
            tap((newCumplir: DbObject) => {   
                const cumplir = getState().cumplir;
                patchState({
                    cumplir: [...cumplir, newCumplir]
                })
            })
        );
    }

    @Action(AgregarPortafolio)
    agregarPortafolio({ patchState, getState }: StateContext<Parametros>, { portafolio }: AgregarPortafolio) {       
        return this.api.newPortfolio(portafolio).pipe(
            tap((newPortafolio: DbObject) => {   
                const portafolios = getState().portafolios;
                patchState({
                    portafolios: [...portafolios, newPortafolio]
                })
            })
        );
    }

    @Action(AgregarPrograma)
    agregarPrograma({ patchState, getState }: StateContext<Parametros>, { programa }: AgregarPrograma) {       
        return this.api.newProgram(programa).pipe(
            tap((newPrograma: DbObject) => {   
                const programas = getState().programas;
                patchState({
                    programas: [...programas, newPrograma]
                })
            })
        );
    }

    @Action(AgregarProfesional)
    agregarProfesional({ patchState, getState }: StateContext<Parametros>, { profesional }: AgregarProfesional) {       
        return this.api.newProfesional(profesional).pipe(
            tap((newProfesional: DbObject) => {   
                const profesionales = getState().profesionales;
                patchState({
                    profesionales: [...profesionales, newProfesional]
                })
            })
        );
    }

    @Action(CheckParamaters)
    portafolios({ patchState, getState }: StateContext<Parametros>) {
        let updates = getState().updates;
        return this.api.portafolios(updates.portafolios).pipe(
            tap((portafolios: DbObject[]) => {
                if (portafolios) {
                    updates = getState().updates;
                    patchState({
                        portafolios: portafolios,
                        updates: {
                            ...updates,
                            portafolios: Date.now()
                        }
                    })
                }
            })
        );
    }

    @Action(CheckParamaters)
    programas({ patchState, getState }: StateContext<Parametros>) {
        let updates = getState().updates;
        return this.api.programas(updates.programas).pipe(
            tap((programas: DbObject[]) => {
                if (programas) {
                    updates = getState().updates;
                    patchState({
                        programas: programas,
                        updates: {
                            ...updates,
                            programas: Date.now()
                        }
                    })
                }
            })
        );
    }

    @Action(CheckParamaters)
    roles({ patchState, getState }: StateContext<Parametros>) {
        let updates = getState().updates;
        return this.api.roles(updates.roles).pipe(
            tap((roles: string[]) => {
                if (roles) {
                    updates = getState().updates;
                    patchState({
                        roles: roles,
                        updates: {
                            ...updates,
                            roles: Date.now()
                        }
                    })
                }
            })
        );
    }

    @Action(CheckParamaters)
    fases({ patchState, getState }: StateContext<Parametros>) {
        let updates = getState().updates;
        return this.api.fases(updates.fases).pipe(
            tap((fases: string[]) => {
                if (fases) {
                    updates = getState().updates;
                    patchState({
                        fases: fases,
                        updates: {
                            ...updates,
                            fases: Date.now()
                        }
                    })
                }
            })
        );
    }

    @Action(CheckParamaters)
    profesionales({ patchState, getState }: StateContext<Parametros>) {
        let updates = getState().updates;
        return this.api.profesionales(updates.profesionales).pipe(
            tap((profesionales: DbObject[]) => {
                if (profesionales) {
                    updates = getState().updates;
                    patchState({
                        profesionales: profesionales,
                        updates: {
                            ...updates,
                            profesionales: Date.now()
                        }
                    })
                }
            })
        );
    }

    @Action(CheckParamaters)
    acciones({ patchState, getState }: StateContext<Parametros>) {
        let updates = getState().updates;
        return this.api.acciones(updates.acciones).pipe(
            tap((acciones: string[]) => {
                if (acciones) {
                    updates = getState().updates;
                    patchState({
                        acciones: acciones,
                        updates: {
                            ...updates,
                            acciones: Date.now()
                        }
                    })
                }
            })
        );
    }

    @Action(CheckParamaters)
    cumplir({ patchState, getState }: StateContext<Parametros>) {
        let updates = getState().updates;
        return this.api.cumplir(updates.cumplir).pipe(
            tap((cumplir: DbObject[]) => {
                if (cumplir) {
                    updates = getState().updates;
                    patchState({
                        cumplir: cumplir,
                        updates: {
                            ...updates,
                            cumplir: Date.now()
                        }
                    })
                }
            })
        );
    }

    @Action(CheckParamaters)
    prioridades({ patchState, getState }: StateContext<Parametros>) {
        let updates = getState().updates;
        return this.api.prioridades(updates.prioridades).pipe(
            tap((prioridades: number[]) => {
                if (prioridades) {
                    updates = getState().updates;
                    patchState({
                        prioridades: prioridades,
                        updates: {
                            ...updates,
                            prioridades: Date.now()
                        }
                    })
                }
            })
        );
    }

    @Action(CheckParamaters)
    rolesGwpvi({ patchState, getState }: StateContext<Parametros>) {
        let updates = getState().updates;
        return this.api.rolesGwpvi(updates.rolesGwpvi).pipe(
            tap((rolesGwpvi: string[]) => {
                if (rolesGwpvi) {
                    updates = getState().updates;
                    patchState({
                        rolesGwpvi: rolesGwpvi,
                        updates: {
                            ...updates,
                            rolesGwpvi: Date.now()
                        }
                    })
                }
            })
        );
    }

    @Action(CheckParamaters)
    distritos({ patchState, getState }: StateContext<Parametros>) {
        let updates = getState().updates;
        return this.api.distritos(updates.distritos).pipe(
            tap((distritos: Distrito[]) => {
                if (distritos) {
                    updates = getState().updates;
                    patchState({
                        distritos: distritos,
                        updates: {
                            ...updates,
                            distritos: Date.now()
                        }
                    })
                }
            })
        );
    }

    @Action(CheckParamaters)
    ciudades({ patchState, getState }: StateContext<Parametros>) {
        let updates = getState().updates;
        return this.api.ciudades(updates.ciudades).pipe(
            tap((ciudades: Ciudad[]) => {
                if (ciudades) {
                    updates = getState().updates;
                    patchState({
                        ciudades: ciudades,
                        updates: {
                            ...updates,
                            ciudades: Date.now()
                        }
                    })
                }
            })
        );
    }

    @Action(CheckParamaters)
    comunidades({ patchState, getState }: StateContext<Parametros>) {
        let updates = getState().updates;
        return this.api.comunidades(updates.comunidades).pipe(
            tap((comunidades: Comunidad[]) => {
                if (comunidades) {
                    updates = getState().updates;
                    patchState({
                        comunidades: comunidades,
                        updates: {
                            ...updates,
                            comunidades: Date.now()
                        }
                    })
                }
            })
        );
    }

    @Action(CheckParamaters)
    barrios({ patchState, getState }: StateContext<Parametros>) {
        let updates = getState().updates;
        return this.api.barrios(updates.barrios).pipe(
            tap((barrios: Barrio[]) => {
                if (barrios) {
                    updates = getState().updates;
                    patchState({
                        barrios: barrios,
                        updates: {
                            ...updates,
                            barrios: Date.now()
                        }
                    })
                }
            })
        );
    }

    @Action(CheckParamaters)
    checkTeam({ patchState }: StateContext<Parametros>) {
        return this.api.team().pipe(
            tap((equipo: string[]) => {
                if (equipo) {
                    patchState({
                        equipo: equipo
                    });
                }
            })
        );
    }

}
