import { State, Selector, Action, StateContext } from '@ngxs/store';
import { ApiService } from '../services/api.service';
import { tap, catchError } from 'rxjs/operators';
import { Navigate } from '@ngxs/router-plugin';
import { BorrarParametros } from './parametros.state';
import { BorrarRiesgos } from './interno.state';
import { BorrarAdmin } from './admin.state';
import { of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BorrarProyectos } from './proyectos';

export class AuthStateModel {
  token: string;
  nombre: string;
  user: string;
  FCM?: string;
  permisos: string[];
}

export class Login {
  static readonly type = '[Auth] Login';
  constructor(public loginCredentials: { user: string; password: string }) { }
}

export class Logout {
  static readonly type = '[Auth] Logout';
}

export class GetToken {
  static readonly type = '[Auth] Token';
}

export class RegistrarFCM {
  static readonly type = '[Auth FCM] Registrar FCM Token';
  constructor(public token: string) { }
}

export class ReemplazarFCM {
  static readonly type = '[Auth FCM] Reemplazar FCM Token';
  constructor(public oldToken: string, public newToken: string) { }
}

export class TokenInvalido {
  static readonly type = '[Auth] Token invalido]';
}

@State<AuthStateModel>({
  name: 'auth',
  defaults: null
})

export class AuthState {
  @Selector()
  static token(state: AuthStateModel) {
    return state.token;
  }

  @Selector()
  static user(state: AuthStateModel) {
    return state.user;
  }

  @Selector()
  static fcm(state: AuthStateModel) {
    return state.FCM;
  }

  @Selector()
  static permisos(state: AuthStateModel) {
    return state.permisos;
  }

  constructor(
    private authService: ApiService,
    private snackBar: MatSnackBar
  ) { }

  // ngxsOnInit({ patchState, dispatch }: StateContext<AuthStateModel>) {
  //   console.log('State initialized, now getting animals');
  //   const auth: AuthStateModel = Object.assign({"user":"jrivera","token":"3j21pb9Z35JKJVkquR7yuddAXHGKJsf86YjtXmP436E39vfidrmZg8Zg6RmJCNWx3EeZj3AxZAXv21dy6xtetEBJ","permisos":"[\"mapa\",\"dev\",\"admin\",\"edit\"]","FCM":"dFCHN0dt95o:APA91bGZfbext9XOfnFZkyeBicgQuZQvQ6CaYTJmwU2vNegR8204yERSiOzCj7EpzqMy9knRzkgz7ew3CiPK1-cJgNuOyx83SvYQSvbB3Q7qegMEJeyTfjQ1gCudsqci1iNwV_DMq0YC"});
  //   patchState(auth);
  //   this.snackBar.open('Bienvenido a GWPVI', 'Ok', {
  //     duration: 3000
  //   });
  //   dispatch(new Navigate(['/gestion/portafolios']));
  // }

  @Action(Login)
  login({ patchState, dispatch }: StateContext<AuthStateModel>, { loginCredentials }: Login) {
    return this.authService.authApi(loginCredentials).pipe(
      tap(
        (credenciales: {
          token: string;
          permisos: string[];
          nombre: string;
        }) => {
          const user = loginCredentials.user.toLowerCase();
          const auth: AuthStateModel = Object.assign({ user }, credenciales);
          patchState(auth);
          this.snackBar.open('Bienvenido a GWPVI', 'Ok', {
            duration: 3000
          });
          dispatch(new Navigate(['/gestion/portafolios']));
        }
      ), catchError(err => {
        if (err.error.mpz) {
          this.snackBar.open(err.error.mpz, 'Ok');
        } else {
          this.snackBar.open('Error de comunicación con MPZ...', 'Ok');
        }
        return of(null);
      }));
  }

  @Action(RegistrarFCM)
  registrarFCM(
    { patchState, dispatch }: StateContext<AuthStateModel>,
    { token }: RegistrarFCM
  ) {
    return this.authService.fcmToken(token).pipe(
      tap((permisos: string[]) => {
        patchState({ FCM: token, permisos: permisos });
      }),
      catchError(err => {
        if (err.status === 401) {
          dispatch(new TokenInvalido());
        }
        if (err.error.mpz) {
          this.snackBar.open(err.error.mpz, 'Ok');
        } else {
          this.snackBar.open('Error de comunicación con MPZ...', 'Ok');
        }
        return of(null);
      })
    );
  }

  @Action(ReemplazarFCM)
  reemplazarFCM(
    { patchState }: StateContext<AuthStateModel>,
    { oldToken, newToken }: ReemplazarFCM
  ) {
    return this.authService
      .replaceFcmToken(oldToken, newToken)
      .pipe(
        tap((token: string) => {
          patchState({ FCM: token });
        })
      );
  }

  @Action(TokenInvalido)
  tokenInvalido({ dispatch, setState }: StateContext<AuthStateModel>) {
    dispatch(new Navigate(['/login']));
    dispatch(new BorrarProyectos());
    dispatch(new BorrarRiesgos());
    dispatch(new BorrarParametros());
    dispatch(new BorrarAdmin());
    setState(null);
  }

  @Action(Logout)
  logout({ getState, dispatch, }: StateContext<AuthStateModel>) {
    const { token, FCM, user } = getState();
    return this.authService.logout(token, FCM, user).pipe(tap(res => {
      dispatch(new TokenInvalido());
    }));
  }
}
